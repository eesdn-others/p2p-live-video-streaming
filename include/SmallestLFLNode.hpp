#ifndef SMALLESTLFLNODE_H
#define SMALLESTLFLNODE_H

#include <list>
#include <map>
#include <vector>
#include "Tree.hpp"
#include "Node.hpp"


class SmallestLFLNode : public Node {
    public:
        SmallestLFLNode(int _degree, Tree<SmallestLFLNode>* _tree, int _parent = -1);
        ~SmallestLFLNode()  = default;

        /**Move*/
        SmallestLFLNode(SmallestLFLNode&&) = default;
        SmallestLFLNode& operator=(SmallestLFLNode&&) = default;
        /**Copy*/
        SmallestLFLNode(const SmallestLFLNode&) = default;
        SmallestLFLNode& operator=(const SmallestLFLNode&) = default;

        void addChild(NodeId _node);
        void removeFromChildren(NodeId _node);
        void die();
        bool repair();

        bool isReachable() const;
		void setTree(Tree<SmallestLFLNode>* _tree){m_tree = _tree;}
		int getBalance() const;
		void changeSubtreeSize(int _delta);
        unsigned getLastFullLevel();
        int getDepth() const;
        void update();
        unsigned getAvailableSpace(unsigned level);
        std::list<NodeId>::iterator find_first_smaller(int,int);
        void updateReceivers();

	private:		
		Tree<SmallestLFLNode>* m_tree;
		void replace(NodeId, NodeId);
		void adopt(NodeId _oldFatherInd, std::list<NodeId>::iterator _iteChildInd);
        std::map<int, std::pair<int,int>> lastFullLevel;
};

#endif // SMALLESTLFLNODE_H
