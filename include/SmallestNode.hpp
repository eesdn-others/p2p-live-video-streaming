#ifndef SmallestNodeImpl_H
#define SmallestNodeImpl_H

#include "Node.hpp"
class SmallestNode : public Node
{
    public:
		SmallestNode(int  _degree, Tree<SmallestNode>* _tree, int _parent = -1);
        ~SmallestNode() = default;
		void addChild(NodeId _node);
        void removeFromChildren(NodeId _node);
        void die();
        bool repair();
		
		
		
		/**Move*/
        SmallestNode(SmallestNode&&) = default;
        SmallestNode& operator=(SmallestNode&&) = default;
        /**Copy*/
        SmallestNode(const SmallestNode&) = default;
        SmallestNode& operator=(const SmallestNode&) = default;
		
		
		int getSubtreeSize() const {return m_subtreeSize;}
		uint64_t getHash() const;
		bool isReachable() const;
		
		void setTree(Tree<SmallestNode>* _tree){m_tree = _tree;}
		int getBalance() const;
		void changeSubtreeSize(int _delta);
		int getDepth() const;
        #ifdef UPDATE
        void update();
        #endif

    protected:
    private:
		void adopt(NodeId _oldFatherInd, std::list<NodeId>::iterator _iteChildInd);
		std::list<NodeId>::iterator find_first_smaller(int);
		void resort(NodeId _sonId, int _delta);
		
		Tree<SmallestNode>* m_tree;
};

#endif // SMALLESTNODE_H

