#ifndef FULLINFO_H
#define FULLINFO_H

#include <list>
#include <map>
#include "Tree.hpp"
#include "Node.hpp"


class FullInfoNode : public Node {
    public:
        FullInfoNode(int _degree, Tree<FullInfoNode>* _tree, int _parent = -1);
        ~FullInfoNode() = default;

        /**Move*/
        FullInfoNode(FullInfoNode&&) = default;
        FullInfoNode& operator=(FullInfoNode&&) = default;
        /**Copy*/
        FullInfoNode(const FullInfoNode&) = default;
        FullInfoNode& operator=(const FullInfoNode&) = default;

        void addChild(NodeId _node);
        void removeFromChildren(NodeId _node);
        void die();
        bool repair();

        bool isReachable() const;
        void setTree(Tree<FullInfoNode>* _tree){m_tree = _tree;}
        int getBalance() const;
        void changeSubtreeSize(int _delta);
        void orderChildren();
        int getDepth() const;
        void getSubtreeLevel(std::map<unsigned, unsigned> &);
        unsigned getLevel();
        void getSubtree(std::vector<unsigned> &);
        std::vector<unsigned> getNodeFromLevel(unsigned);
        std::vector<unsigned> getNodeLevel(unsigned);
        unsigned getHeight();
        std::vector<unsigned> getSubtreeOrderedByLevel();
        bool pullNodes();
        bool swapNodes();
        void swap(NodeId);
        void pull(unsigned);
        bool isUnderLoaded();
        bool isOverLoaded();
        void takeMaxNextLevel(unsigned, unsigned);
        #ifdef UPDATE
        void update();
        #endif

    private:        
        Tree<FullInfoNode>* m_tree;
        void replace(NodeId, NodeId);
        void adopt(NodeId _oldFatherInd, std::list<NodeId>::iterator _iteChildInd);


};

#endif // FULLINFO_H
