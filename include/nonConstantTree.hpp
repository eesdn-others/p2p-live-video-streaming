#ifndef NONCONSTANTTREE_H
#define NONCONSTANTTREE_H

#include <iosfwd>
#include <cstdint>
#include <string>
#include <vector>
#include <list>

#include "Tree.hpp"
/**
 * A tree structure with a non mutable number of nodes
 */ 
template<typename Node>
class NonConstantTree : public Tree<Node>
{
    template<typename N> friend bool operator==(const NonConstantTree<N>& tree1, const NonConstantTree<N>& tree2);
    public:
        NonConstantTree(int _degree = 20, int _nbNodes = 0);
        NonConstantTree(int _degree, std::vector<Node>&& _nodes);
        NonConstantTree(int _degree, const std::vector<Node>& _nodes);
        virtual ~NonConstantTree() {};
        static NonConstantTree balancedTree(int _degree, int _nbNodes);

        static NonConstantTree path(int _degree, int _nbNodes);
        static NonConstantTree star(int _degree, int _nbNodes);

        static NonConstantTree star(std::vector<int> _degrees, int _nbNodes);
        static NonConstantTree balancedTree(std::vector<int> _degrees, int _nbNodes);
        
        /**Copy*/
        NonConstantTree(const NonConstantTree&);
        NonConstantTree& operator=(const NonConstantTree&);
        
        // /**Getter*/
        // const Node& getRoot() const {return m_nodes[0];}
        // Node& getRoot() {return m_nodes[0];}
        
        
        
        // const std::vector<Node>& getNodes() const {return m_nodes;}
        // std::vector<Node>& getNodes() {return m_nodes;}
        
         void createDotFile(const std::string& _filename, const std::string& _type = "pdf") const ;
        // Node& getRandomNode(int _level);
        // void getListNodes(int _level, int iterator, Node node, std::vector<unsigned int>* vector);
        // Node* killNode();
        // void killANode(int _index);
        // Node* repairNode();
        // void repairAll();
        // void repairRoot();
        // void repairANode(int _index);
        
        // double getBalance() const;
        // double getAbsBalance() const;
        //int getMaxHeight() const;
        inline unsigned getNumberPeers() {return this->m_nodes.size()-m_available_positions.size()-1;}
         int getNbReachable() const;
         int getNbUnreachable() const;
         bool isValid(unsigned) const;
        int getNbUnreachableNotNew() const;
        int addNode(int _degree);
        void removeNode(int _id);
        double getAvgDepthBandwidth(int) const;
        double getRatioReceivingBandwidth(int) const;
        double getAvgDepthReachable() const;
        double getAvgDepth() const ;
        int getMaxDegree() const;
        std::vector<unsigned> getValidNodes();

        //inline Node& getNode(unsigned _ind) { return m_nodes[_ind]; }
        
        // std::list<NonConstantTree> getRepairTransitionTrees() const;
        // std::list<NonConstantTree> getChurnTransitionTrees() const;
        
        // uint64_t getHash() const;
        
        protected:
           //std::vector<Node> m_nodes;
            // int m_degree;
            std::list<int> m_available_positions;
            /**Hash*/
            // mutable uint64_t m_hash;
            // mutable bool m_isHashValid;
};
#include "nonConstantTree.cpp"
#endif // TREE_H

