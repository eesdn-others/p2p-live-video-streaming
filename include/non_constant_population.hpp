#ifndef NON_CONSTANT_POPULATION_H
#define NON_CONSTANT_POPULATION_H

#include <cstddef>
#include "nonConstantTree.hpp"

#ifdef UPDATE
template<class Node> void overTime(Tree<Node>& node, int _maxTime, double _churnRate, unsigned phase,int (*f)(void),double);
template<class Node> void interrupt(Tree<Node>& node, int _maxTime, double _churnRate, unsigned phase,int (*f)(void),double);
#else
template<class Node> void overTime(Tree<Node>& node, int _maxTime, double _churnRate, unsigned phase,int (*f)(void));
template<class Node> void interrupt(Tree<Node>& node, int _maxTime, double _churnRate, unsigned phase,int (*f)(void));
#endif

#include "non_constant_population.cpp"

#endif  