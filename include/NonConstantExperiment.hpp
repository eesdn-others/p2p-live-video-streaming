#ifndef NONCONSTANTEXPERIMENT_H
#define NONCONSTANTEXPERIMENT_H

#include <vector>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <cmath>
#include <map>
#include <random>

#include "nonConstantTree.hpp"
#include "FakeQueue.hpp"

template<typename Node>
class NonConstantExperiment
{
    public:
        #ifdef UPDATE
        NonConstantExperiment(NonConstantTree<Node>* _tree, double _joinRate, double _indivLeaveRate, double _fixRate, int (*f)(void), double _updateTime);
        #else
        NonConstantExperiment(NonConstantTree<Node>* _tree, double _joinRate, double _indivLeaveRate, double _fixRate, int (*f)(void));
        #endif
        ~NonConstantExperiment();

        NonConstantExperiment(const NonConstantExperiment&) = delete;
        NonConstantExperiment& operator=(const NonConstantExperiment&) = delete;

        void run(int _nbSteps);
        void runUntil(int _nbEvents);
        void runUntil(int _nbEvents, int _maxTime);
        
        bool nextEvent();
        inline double getTime() const {return m_fakeQueue.getTime();}
        void cleanEvents();

        void addNode(int (*f)(void));
        void removeNode(int);
        //void insertNodes(std::vector<unsigned> nodes);

        inline const FakeQueue& getFakeQueue() const {return m_fakeQueue;}
        inline FakeQueue& getFakeQueue() {return m_fakeQueue;}
        
        inline const NonConstantTree<Node>* getTree() const {return m_tree;}
        inline NonConstantTree<Node>* getTree() {return m_tree;}

        int m_nbLeave;
        int m_nbFix;
        int m_nbJoin;
    protected:
    private:
        double m_joinRate;
        double m_indivLeaveRate;
        double m_fixRate;

        #ifdef UPDATE
        double m_updateTime;
        double m_last_update;
        #endif
        
        NonConstantTree<Node>* m_tree;
        FakeQueue m_fakeQueue;
        std::list<std::list<Event>::iterator> m_eventsToRemove;
        // std::map<int, std::list<std::list<Event>::iterator>> m_events;
};

#include "NonConstantExperiment.cpp"

#endif // NONCONSTANTEXPERIMENT_H