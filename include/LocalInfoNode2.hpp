#ifndef LOCALINFO2_H
#define LOCALINFO2_H

#include <list>
#include <map>
#include "Tree.hpp"
#include "Node.hpp"


class LocalInfoNode2 : public Node {
    public:
        LocalInfoNode2(int _degree, Tree<LocalInfoNode2>* _tree, int _parent = -1);
        ~LocalInfoNode2() = default;

        /**Move*/
        LocalInfoNode2(LocalInfoNode2&&) = default;
        LocalInfoNode2& operator=(LocalInfoNode2&&) = default;
        /**Copy*/
        LocalInfoNode2(const LocalInfoNode2&) = default;
        LocalInfoNode2& operator=(const LocalInfoNode2&) = default;

        void addChild(NodeId _node);
        void removeFromChildren(NodeId _node);
        void die();
        bool repair();

        bool isReachable() const;
        void setTree(Tree<LocalInfoNode2>* _tree){m_tree = _tree;}
        int getBalance() const;
        void changeSubtreeSize(int _delta);
        int getDepth() const;
        std::pair<unsigned, unsigned> estimatedHeight();
        void getSubtree(std::vector<unsigned> &);
        void getDegreeSubtree(std::vector<unsigned> &);
        void addDegree(unsigned);
        void update();
        std::list<NodeId>::iterator find_first_smaller(int,int);
        void updateReceivers();

    private:        
        Tree<LocalInfoNode2>* m_tree;
        void replace(NodeId, NodeId);
        void adopt(NodeId _oldFatherInd, std::list<NodeId>::iterator _iteChildInd);
        std::vector<unsigned> degreesSubtree;


};

#endif // LOCALINFO2_H
