#ifndef TREE_H
#define TREE_H

#include <iosfwd>
#include <cstdint>
#include <string>
#include <vector>
#include <list>
#include "update.hpp"
/**
 * A tree structure with a non mutable number of nodes
 */ 
template<typename Node>
class Tree
{
    template<typename N> friend bool operator==(const Tree<N>& tree1, const Tree<N>& tree2);
    public:
        Tree(int _degree, int _nbNodes);
        Tree(int _degree, std::vector<Node>&& _nodes);
        Tree(int _degree, const std::vector<Node>& _nodes);
        ~Tree() = default;
        static Tree balancedTree(int _degree, int _nbNodes);

        static Tree path(int _degree, int _nbNodes);
        static Tree star(int _degree, int _nbNodes);

        static Tree star(std::vector<int> _degrees, int _nbNodes);
        static Tree balancedTree(std::vector<int> _degrees, int _nbNodes);
        
        /**Copy*/
        Tree(const Tree&);
        Tree& operator=(const Tree&);
        
        /**Getter*/
        const Node& getRoot() const {return m_nodes[0];}
        Node& getRoot() {return m_nodes[0];}
        
        
        
        const std::vector<Node>& getNodes() const {return m_nodes;}
        std::vector<Node>& getNodes() {return m_nodes;}
        int getNbNodes() const { return m_nbNodes; }
        
        void createDotFile(const std::string& _filename, const std::string& _type = "pdf") const ;
        Node& getRandomNode(int _level);
        void getListNodes(int _level, int iterator, Node node, std::vector<int>* vector);
        Node* killNode();
        void killANode(int _index);
        Node* repairNode();
        void repairAll();
        void repairRoot();
        void repairANode(int _index);
        
        double getBalance() const;
        double getAbsBalance() const;
        int getMaxHeight() const;
        int getNbReachable() const;
        int getNbUnreachable() const;
        int getNbUnreachableNotNew() const;
        
        double getAvgDepthReachable() const;
        double getAvgDepth() const ;

        int getMaxDegree() const;

        double getAvgDepthBandwidth(int) const;
        double getRatioReceivingBandwidth(int) const;

        // std::vector<int> v;

        inline Node& getNode(int _ind) {return m_nodes[_ind];}
        inline void setDegree(int deg) {m_degree=deg;}
        std::list<Tree> getRepairTransitionTrees() const;
        std::list<Tree> getChurnTransitionTrees() const;
        
        uint64_t getHash() const;
        /*
        double repair_time=0;
        double swap_time=0;
        double pull_time=0;
        double die_time=0;
        */
        #ifdef UPDATE
        void update();
        #endif
        
        protected:

            std::vector<Node> m_nodes;
            int m_degree;
            int m_nbNodes;
 
            
            /**Hash*/
            mutable uint64_t m_hash;
            mutable bool m_isHashValid;
};
#include "Tree.cpp"
#endif // TREE_H

