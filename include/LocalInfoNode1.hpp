#ifndef LOCALINFO1_H
#define LOCALINFO1_H

#include <list>
#include <map>
#include "Tree.hpp"
#include "Node.hpp"


class LocalInfoNode1 : public Node {
    public:
        LocalInfoNode1(int _degree, Tree<LocalInfoNode1>* _tree, int _parent = -1);
        ~LocalInfoNode1() = default;

        /**Move*/
        LocalInfoNode1(LocalInfoNode1&&) = default;
        LocalInfoNode1& operator=(LocalInfoNode1&&) = default;
        /**Copy*/
        LocalInfoNode1(const LocalInfoNode1&) = default;
        LocalInfoNode1& operator=(const LocalInfoNode1&) = default;

        void addChild(NodeId _node);
        void removeFromChildren(NodeId _node);
        void die();
        bool repair();

        bool isReachable() const;
        void setTree(Tree<LocalInfoNode1>* _tree){m_tree = _tree;}
        int getBalance() const;
        void changeSubtreeSize(int _delta);
        int getDepth() const;
        std::list<NodeId>::iterator find_first_smaller(int,int);
        void updateReceivers();
        #ifdef UPDATE
        void update();
        #endif
    private:        
        Tree<LocalInfoNode1>* m_tree;
        void replace(NodeId, NodeId);
        void adopt(NodeId _oldFatherInd, std::list<NodeId>::iterator _iteChildInd);
        //map (a,b) where a is the id of the node and b is the number of push on that node
        std::map<unsigned,unsigned> push_done;


};

#endif // LOCALINFO1_H
