#ifndef NAMES_H
#define NAMES_H

#include <string>

template<class Node> std::string getName();

#include "names.cpp"

#endif