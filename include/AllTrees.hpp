#ifndef ALLTREES_H
#define ALLTREES_H

#include <tuple>

typedef std::tuple<int, int, int> TriplInt;

struct CompareTriplInt {
	bool operator()(TriplInt& _t1, TriplInt& _t2) {
		return std::get<1>(_t1) > std::get<1>(_t2) || 
			(std::get<1>(_t1) == std::get<1>(_t2) && std::get<0>(_t1) > std::get<0>(_t2));
	}
	typedef CompareTriplInt value_type;
	typedef CompareTriplInt& reference; 
	typedef const CompareTriplInt& const_reference;
	typedef int size_type;
};

void allTrees(int _nbNodes);

#endif // ALLTREES_H
