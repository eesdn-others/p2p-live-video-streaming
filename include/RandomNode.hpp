#ifndef RANDOMNODE_H
#define RANDOMNODE_H

#include <list>

#include "Tree.hpp"
#include "Node.hpp"


class RandomNode : public Node {
    public:
        RandomNode(int _degree, Tree<RandomNode>* _tree, int _parent = -1);
        ~RandomNode() = default;

        /**Move*/
        RandomNode(RandomNode&&) = default;
        RandomNode& operator=(RandomNode&&) = default;
        /**Copy*/
        RandomNode(const RandomNode&) = default;
        RandomNode& operator=(const RandomNode&) = default;

        void addChild(NodeId _node);
        void removeFromChildren(NodeId _node);
        void die();
        bool repair();

        bool isReachable() const;
		void setTree(Tree<RandomNode>* _tree){m_tree = _tree;}
		int getBalance() const;
		void changeSubtreeSize(int _delta);
        int getDepth() const;
        #ifdef UPDATE
        void update();
        #endif


	private:		
		Tree<RandomNode>* m_tree;
		
		void replace(NodeId, NodeId);
		void adopt(NodeId _oldFatherInd, std::list<NodeId>::iterator _iteChildInd);
};

#endif // RANDOMNODE_H
