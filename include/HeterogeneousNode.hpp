#ifndef HETEROGENEOUS_H
#define HETEROGENEOUS_H

#include <list>
#include <map>
#include <vector>
#include "Tree.hpp"
#include "Node.hpp"


class HeterogeneousNode : public Node {
    public:
        HeterogeneousNode(int _degree, Tree<HeterogeneousNode>* _tree, int _parent = -1);
        ~HeterogeneousNode() = default;

        /**Move*/
        HeterogeneousNode(HeterogeneousNode&&) = default;
        HeterogeneousNode& operator=(HeterogeneousNode&&) = default;
        /**Copy*/
        HeterogeneousNode(const HeterogeneousNode&) = default;
        HeterogeneousNode& operator=(const HeterogeneousNode&) = default;

        void addChild(NodeId _node);
        void removeFromChildren(NodeId _node);
        void die();
        bool repair();

        bool isReachable() const;
		void setTree(Tree<HeterogeneousNode>* _tree){m_tree = _tree;}
		int getBalance() const;
		void changeSubtreeSize(int _delta);
        unsigned getLastFullLevel();
        void orderChildren();

        int getDepth() const;
    
        void update();
        void getSubtree(std::vector<unsigned> &subtree);
        unsigned getLevel(unsigned root_subtree);
        unsigned getAvailableSpace(unsigned level);
	private:		
		Tree<HeterogeneousNode>* m_tree;
		void replace(NodeId, NodeId);
		void adopt(NodeId _oldFatherInd, std::list<NodeId>::iterator _iteChildInd);
        std::map<int, std::pair<int,int>> lastFullLevel;
};

#endif // HETEROGENEOUS_H
