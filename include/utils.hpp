#ifndef UTILS_H_INCLUDED
#define UTILS_H_INCLUDED

#include <algorithm>
#include <iostream>
#include <vector>

///Represents the exception for taking the median of an empty list
class median_of_empty_list_exception:public std::exception{
  virtual const char* what() const throw() {
    return "Attempt to take the median of an empty list of numbers.  "
      "The median of an empty list is undefined.";
  }
};

///Return the median of a sequence of numbers defined by the random
///access iterators begin and end.  The sequence must not be empty
///(median is undefined for an empty set).
/// 
///The numbers must be convertible to double.
template<class RandAccessIter> 
double median(RandAccessIter begin, RandAccessIter end)
  throw(median_of_empty_list_exception);

/**For printing tuple */

template<int...> struct seq{virtual ~seq(){}};

template<int N, int... Is>
struct gen_seq : gen_seq<N-1, N-1, Is...>{virtual ~gen_seq(){}};

template<int... Is>
struct gen_seq<0, Is...> : seq<Is...>{virtual ~gen_seq(){}};

/**----------------------------------------*/

template<class Tuple, int... Is>
void print(std::ostream& _out, Tuple& t, seq<Is...>);

template<typename T, typename... Args>
void toCSV(const std::string& _filename, const T& _tab, Args... args);


template<typename InputIterator>
double mean(InputIterator first, InputIterator last, InputIterator wFirst, InputIterator wLast);

template<typename InputIterator>
double mean(InputIterator first, InputIterator last);

std::vector<double> frange(double _start, double _stop, int _nbSteps);

#include "utils.cpp"

#endif // UTILS_H_INCLUDED