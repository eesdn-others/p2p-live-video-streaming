#ifndef PARTIALINFONODE_H
#define PARTIALINFONODE_H

#include "Tree.hpp"
#include "Node.hpp"


class PartialInfoNode : public Node {
	public:
		enum Type {NORMAL, NEW_NODE, BIG_SUBTREE};
		PartialInfoNode(int _degree, Tree<PartialInfoNode>* _tree, PartialInfoNode::Type _type = NORMAL, int _parent = -1);
		~PartialInfoNode() = default;
		
		/** Copy */
        PartialInfoNode(const PartialInfoNode&) = default;
        PartialInfoNode& operator=(const PartialInfoNode&) = default;
        /** Move */
        PartialInfoNode(PartialInfoNode&&) = default;
        PartialInfoNode& operator=(PartialInfoNode&&) = default;
		
		
		void die();
		bool repair();
		void addChild(NodeId _id);
		void removeFromChildren(NodeId _id);
		
		inline Type getType() const {return m_type;}
		inline void setType(const Type& _type) {m_type = _type;}
		bool isReachable() const; 
		
		Tree<PartialInfoNode>* getTree() const {return m_tree;}
		void setTree(Tree<PartialInfoNode>* _tree) {m_tree=_tree;}
		int getSubtreeSize() const;
		std::string getDotLabel() const;
		void replace(unsigned _old, unsigned _new);
		int getBalance() const;
		void changeSubtreeSize(int _delta);
		int getDepth() const;
        #ifdef UPDATE
        void update();
        #endif

	private:
		Type m_type;
		Tree<PartialInfoNode>* m_tree;
		
		void adopt(NodeId _oldFatherInd, std::list<NodeId>::iterator _iteChildInd);
		void resort(NodeId _sonId);
		std::list<NodeId>::iterator find_first_new_node();
};

#endif