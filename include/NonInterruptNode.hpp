#ifndef NONINTERRUPTNODE_H
#define NONINTERRUPTNODE_H

#include <list>
#include <vector>
#include <iostream>

#include "Tree.hpp"
#include "Node.hpp"

class NonInterruptNode : public Node
{
    public:
        NonInterruptNode(int _degree, Tree<NonInterruptNode>* _tree, int _parent = -1);
        ~NonInterruptNode() = default;

        /** Copy */
        NonInterruptNode(const NonInterruptNode&) = default;
        NonInterruptNode& operator=(const NonInterruptNode&) = default;
        /** Move */
        NonInterruptNode(NonInterruptNode&&) = default;
        NonInterruptNode& operator=(NonInterruptNode&&) = default;

		int getSubtreeSize() const;
		
        void addChild(unsigned _node);
        
        void removeFromChildren(unsigned _node);
        void die();
        bool repair();

        bool isReachable() const;
		void setTree(Tree<NonInterruptNode>* _tree){m_tree = _tree;}
		int getBalance() const;
		void changeSubtreeSize(int _delta);
        int getDepth() const;
        #ifdef UPDATE
        void update();
        #endif

    protected:
    private:
		Tree<NonInterruptNode>* m_tree;
		
		void replace(unsigned, unsigned);
		void adopt(unsigned _oldFatherInd, std::list<unsigned>::iterator _iteChildInd);
};

#endif // NONINTERRUPTNODE_H
