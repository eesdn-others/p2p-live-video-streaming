#ifndef LOCALINFO3_H
#define LOCALINFO3_H

#include <list>
#include <map>
#include "Tree.hpp"
#include "Node.hpp"


class LocalInfoNode3 : public Node {
    public:
        LocalInfoNode3(int _degree, Tree<LocalInfoNode3>* _tree, int _parent = -1);
        ~LocalInfoNode3() = default;

        /**Move*/
        LocalInfoNode3(LocalInfoNode3&&) = default;
        LocalInfoNode3& operator=(LocalInfoNode3&&) = default;
        /**Copy*/
        LocalInfoNode3(const LocalInfoNode3&) = default;
        LocalInfoNode3& operator=(const LocalInfoNode3&) = default;

        void addChild(NodeId _node);
        void removeFromChildren(NodeId _node);
        void die();
        bool repair();

        bool isReachable() const;
        void setTree(Tree<LocalInfoNode3>* _tree){m_tree = _tree;}
        int getBalance() const;
        void changeSubtreeSize(int _delta);
        void orderChildren();
        int getDepth() const;
        void getSubtreeLevel(std::map<unsigned, unsigned> &);
        unsigned getLevel();
        void getSubtree(std::vector<unsigned> &);
        std::vector<unsigned> getNodeFromLevel(unsigned);
        std::vector<unsigned> getNodeLevel(unsigned);
        unsigned getHeight();
        std::vector<unsigned> getSubtreeOrderedByLevel();
        bool pullNodes();
        bool swapNodes();
        void swap(NodeId);
        void pull(unsigned);
        bool isUnderLoaded();
        bool isOverLoaded();
        void takeMaxNextLevel(unsigned, unsigned);
        #ifdef UPDATE
        void update();
        #endif

    private:        
        Tree<LocalInfoNode3>* m_tree;
        void replace(NodeId, NodeId);
        void adopt(NodeId _oldFatherInd, std::list<NodeId>::iterator _iteChildInd);


};

#endif // LOCALINFO3_H
