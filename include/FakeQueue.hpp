#ifndef FAKEQUEUE_H
#define FAKEQUEUE_H

#include <list>
#include <iostream>
#include <functional>

#include "MyRandom.hpp"

struct Event {
	friend std::ostream& operator<<(std::ostream&, const Event&);

    Event(double _rate, std::function<bool()> _func = nullptr) : 
    	m_rate(_rate), 
    	m_func(_func) 
    {}
	double m_rate;
	std::function<bool()> m_func;
};

// bool operator==(const Event& _ev1, const Event& _ev2);

class FakeQueue {
	friend std::ostream& operator<<(std::ostream&, const FakeQueue&);
	public:
		FakeQueue();
		~FakeQueue() = default;
		void addEvent(Event& _event);
		void addEvent(Event&& _event);

		std::list<Event>::iterator addEvent(double _rate, std::function<bool()> _func = nullptr);
		// void removeEvent(const Event& _event);
		void removeEvent(std::list<Event>::const_iterator);
		const Event& getNextEvent();

		inline double getTime() const {return m_time;};
	
	protected:
		double nextTime();
	
	public:
		double m_time;
		std::list<Event> m_events;
		double m_totalRate;
};

std::ostream& operator<<(std::ostream&, const FakeQueue&);
std::ostream& operator<<(std::ostream&, const Event&);

#endif