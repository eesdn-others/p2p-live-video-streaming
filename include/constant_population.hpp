#ifndef CONSTANT_POPULATION_H
#define CONSTANT_POPULATION_H

#include <cstddef>
#include "Tree.hpp"

#include "update.hpp"
#ifdef UPDATE
template<class Node> void overTime(Tree<Node>& node, int _maxTime, double _churnRate, double _updateTime);
template<class Node> void interrupt(Tree<Node>& node, int _maxTime, double _churnRate, double _updateTime);
template<typename Node> void distribution(Tree<Node>& node, int _maxTime, double _churnRate, double _updateTime);
template<class Node> void interruptDistribution(Tree<Node>& node, int _maxTime, double _churnRate, double _updateTime);
#else
template<class Node> void overTime(Tree<Node>& node, int _maxTime, double _churnRate);
template<class Node> void interrupt(Tree<Node>& node, int _maxTime, double _churnRate);
template<typename Node> void distribution(Tree<Node>& node, int _maxTime, double _churnRate);
template<class Node> void interruptDistribution(Tree<Node>& node, int _maxTime, double _churnRate);
#endif  



#include "constant_population.cpp"

#endif  