#ifndef EXPERIMENT_H
#define EXPERIMENT_H

#include <vector>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <cmath>
#include <random>

#include "Tree.hpp"
#include "FakeQueue.hpp"
#include "update.hpp"

template<typename Node>
class Experiment
{
    public:

        #ifdef UPDATE
        Experiment(double _churnRate, double _fixRate, Tree<Node>* _tree, double _updateTime);
        #else
        Experiment(double _churnRate, double _fixRate, Tree<Node>* _tree);
        #endif  

        ~Experiment() = default;

        Experiment(const Experiment&) = delete;
        Experiment& operator=(const Experiment&) = delete;

        void run(int _nbSteps);
        void runUntil(int _nbEvents);
        void runUntil(int _nbEvents, int _maxTime);
        
        bool nextEvent();
        inline double getTime() const {return m_fakeQueue.getTime();}
        
        inline const FakeQueue& getFakeQueue() const {return m_fakeQueue;}
        inline FakeQueue& getFakeQueue() {return m_fakeQueue;}
        
        inline const Tree<Node>* getTree() const {return m_tree;}
        inline Tree<Node>* getTree() {return m_tree;}
        
        int m_nbChurn;
        int m_nbFix;
    protected:
    private:
        double m_churnRate;
        double m_fixRate;

        #ifdef UPDATE
        double m_updateTime;
        double m_last_update;
        #endif

        Tree<Node>* m_tree;
        FakeQueue m_fakeQueue;

};

#include "Experiment.cpp"

#endif // EXPERIMENT_H