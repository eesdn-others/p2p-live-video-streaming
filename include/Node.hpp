#ifndef NODE_H
#define NODE_H

#include <list>

#include "Tree.hpp"

#include "update.hpp"

using NodeId = unsigned;

class Node {
    friend std::ostream& operator<<(std::ostream& _out, Node& _n) ;
public:
    Node(int, int, unsigned);
    virtual ~Node() = 0;
    inline const std::list<NodeId>& getChildren() const {return m_children;}
    inline std::list<NodeId>& getChildren() {return m_children;}
    
    inline NodeId getOwnInd() const {return m_ownInd;}
    inline void setOwnInd(int _ownInd) {m_ownInd = _ownInd;}
    
    inline void setParent(int _parent) {
        m_parent = _parent;
    }
    
    inline int getParent() const { return m_parent;}
    inline bool hasParent() const {return m_parent != -1;}
    
    inline int getDegree() const {return m_degree;}
    inline void setDegree(int _degree) {m_degree = _degree;}

    int getSubtreeSize() const {return m_subtreeSize;}
    void changeSubtreeSize(int _delta);
    
    bool isOverloaded() const {return (int) m_children.size() > m_degree;}
    std::string getDotLabel() const;
    
    bool isNew() const {return m_new;}
    void setNew(bool _new){m_new = _new;}
    
    int getBalance() const;
    int getDepth() const;
    void setReachable(bool state){reachable = state;}
protected:
    int m_degree;
    bool reachable = false;
    std::list<NodeId> m_children;
    int m_subtreeSize;
    
    int m_parent;
    NodeId m_ownInd;
    mutable bool m_new = false;
};

std::ostream& operator<<(std::ostream& _out, Node& _n) ;

#endif
