#include <iostream>

#include <ctime>
#include <iostream>
#include <functional>
#include <algorithm>
#include <string>
#include <tclap/CmdLine.h>
#include <thread>
#include <utility>
#include <map>
#include <bitset>

#include "Tree.hpp"

#include "RandomNode.hpp"
#include "SmallestNode.hpp"
#include "Experiment.hpp"
#include "NonInterruptNode.hpp"
#include "PartialInfoNode.hpp"
#include "HeterogeneousNode.hpp"
#include "LocalInfoNode1.hpp"
#include "LocalInfoNode2.hpp"
#include "LocalInfoNode3.hpp"
#include "FullInfoNode.hpp"
#include "SmallestLFLNode.hpp"


#include "AllTrees.hpp"
#include "constant_population.hpp"
#include "names.hpp"
#include "utils.hpp"

#include "NonConstantExperiment.hpp"
#include "update.hpp"

template <typename T>
std::string to_string_with_precision(const T a_value, const int n = 6)
{
    std::ostringstream out;
    out << std::fixed << std::setprecision(n) << a_value;
    return out.str();
}

std::vector<int> bandwidthDistribution_fullUse(int _nbNodes);
std::vector<int> bandwidthDistribution_semiUse(int _nbNodes);

template<class Node>
std::vector<double> asFunctionOfChurnRate(const std::vector<double> _range, Tree<Node>& _tree, std::function<int(Tree<Node>&)>& _f,
                           float _fixRate, int _expNbSteps, int _runSteps);



#ifdef UPDATE
template<class Node> void simulate(std::string _typeExp, std::string _subType, int _nbNodes, int _maxEvents, double _churnRate, double _delay, double _updateTime);
#else
template<class Node> void simulate(std::string _typeExp, std::string _subType, int _nbNodes, int _maxEvents, double _churnRate, double _delay);
#endif
 
template<class Node> void distanceOverChurn(double _minValue, double _maxValue, int _maxEvents, int _nbSteps);


std::vector<int> bandwidthDistribution2(int _nbNodes) {
    int firstGroup = .11 * _nbNodes;
    int secondGroup = .03 * _nbNodes;
    int thirdGroup = .09 * _nbNodes;

    std::vector<int> distr(_nbNodes);
    int i = 0;
    for(; i < firstGroup; ++i) {
        distr[i] = 20;
    }
    for(; i < firstGroup + secondGroup; ++i) {
        distr[i] = 8;
    }
    for(; i < firstGroup + secondGroup + thirdGroup; ++i) {
        distr[i] = 3;   
    }
    for(; i < _nbNodes; ++i) {
        distr[i] = 1;
    }
    return distr;
}


std::vector<int> bandwidthDistribution3(int _nbNodes) {
    int firstGroup = .15 * _nbNodes;
    int secondGroup = .25 * _nbNodes;
    int thirdGroup = .40 * _nbNodes;

    std::vector<int> distr(_nbNodes);
    int i = 0;
    for(; i < firstGroup; ++i) {
        distr[i] = 16;
    }
    for(; i < firstGroup + secondGroup; ++i) {
        distr[i] = 4;
    }
    for(; i < firstGroup + secondGroup + thirdGroup; ++i) {
        distr[i] = 1;   
    }
    for(; i < _nbNodes; ++i) {
        distr[i] = 0;
    }
    return distr;
}

std::vector<int> bandwidthDistribution4(int _nbNodes) {
    int firstGroup = .163 * _nbNodes;
    int secondGroup = .281 * _nbNodes;
    int thirdGroup = .08 * _nbNodes;
    int fourthGroup = .376 * _nbNodes;
    std::vector<int> distr(_nbNodes);

    int i = 0;
    for(; i < firstGroup; ++i) {
        distr[i] = 4;
    }
    for(; i < firstGroup + secondGroup; ++i) {
        distr[i] = 3;
    }
    for(; i < firstGroup + secondGroup + thirdGroup; ++i) {
        distr[i] = 2;   
    }
    for(; i < firstGroup + secondGroup + thirdGroup + fourthGroup; ++i) {
        distr[i] = 1;   
    }
    for(; i < _nbNodes; ++i) {
        distr[i] = 0;
    }
    return distr;
}


std::vector<int> bandwidthDistribution_fullUse(int _nbNodes) {

    int firstGroup = .12 * _nbNodes;
    int secondGroup = .42 * _nbNodes;
    int thirdGroup = .21 * _nbNodes;
    int fourthGroup = .20 * _nbNodes;
    std::cout << firstGroup << ", " << secondGroup << ", " << thirdGroup << ", " << fourthGroup << '\n';

    std::vector<int> distr(_nbNodes);

    int i = 0;
    for(; i < firstGroup; ++i) {
        distr[i] = 4;
    }
    for(; i < firstGroup + secondGroup; ++i) {
        distr[i] = 2;
    }
    for(; i < firstGroup + secondGroup + thirdGroup; ++i) {
        distr[i] = 1;   
    }
    for(; i < _nbNodes; ++i) {
        distr[i] = 1;
    }
    return distr;
}

std::vector<int> bandwidthDistributionNew(int param, int _nbNodes) {
	int firstGroup ;
    int secondGroup;
    int thirdGroup ;
    int fourthGroup;
    int fifthGroup;

	if (param == 1){
		//opt
		firstGroup =  0.493 * _nbNodes;
    	secondGroup = 0.187 * _nbNodes;
    	thirdGroup =  0.084 * _nbNodes;
    	fourthGroup = 0.052 * _nbNodes;
    	fifthGroup =  0.184 * _nbNodes;
	}
	else if(param == 2){
		//pess
		firstGroup =  0.609 * _nbNodes;
    	secondGroup = 0.187 * _nbNodes;
    	thirdGroup =  0.084 * _nbNodes;
    	fourthGroup = 0.052 * _nbNodes;
    	fifthGroup =  0.068 * _nbNodes;
	}
	else if(param == 3){
		//distr
		firstGroup =  0.5577 * _nbNodes;
    	secondGroup = 0.2116 * _nbNodes;
    	thirdGroup =  0.095  * _nbNodes;
    	fourthGroup = 0.0588 * _nbNodes;
    	fifthGroup =  0.0769 * _nbNodes;
	}
	else{
		std::cout << "Invalid parameter\n";
		exit(1);
	}

    std::cout << firstGroup << ", " << secondGroup << ", " << thirdGroup << ", " << fourthGroup<<", "<< fifthGroup << '\n';

    std::vector<int> distr(_nbNodes);

    int i = 0;
    for(; i < fifthGroup; ++i) {
        distr[i] = 20;
    }
    int j=0;
    for(; i < fifthGroup + fourthGroup; ++i) {
    	//random number between 3 and 19
        //distr[i] = 3+rand()%17;
        // in this way the distribution is the same in the comparation of the protocols
    	//distr[i] = 3 + j++ %17;
    	distr[i] = 3 + (14 - j++%15);
    }
    for(; i < fifthGroup + fourthGroup + thirdGroup; ++i) {
        distr[i] = 2;   
    }
     for(; i < fifthGroup + fourthGroup + thirdGroup + secondGroup; ++i) {
         distr[i] = 1;
    }
    for(; i < _nbNodes; ++i) {
        distr[i] = 0;
    }
    std::sort(distr.begin(), distr.end(),std::greater<int>());

    return distr;
}


std::vector<int> bandwidthDistribution_semiUse(int _nbNodes) {
    int firstGroup = .12 * _nbNodes;
    int secondGroup = .21 * _nbNodes;
    int thirdGroup = .21 * _nbNodes;
    // int fourthGroup = .12 * _nbNodes;

    std::vector<int> distr(_nbNodes);
    int i = 0;
    for(; i < firstGroup; ++i) {
        distr[i] = 4;
    }
    for(; i < firstGroup + secondGroup; ++i) {
        distr[i] = 1;
    }
    for(; i < firstGroup + secondGroup + thirdGroup; ++i) {
        distr[i] = 1;   
    }
    for(; i < _nbNodes; ++i) {
        distr[i] = 0;
    }
    return distr;
}

/**
Parse the args and launch the simulation accordingly
*/
void parseArgs(int argc, char** argv) {
    TCLAP::CmdLine cmd("");

    TCLAP::ValueArg<std::string> arg_typeExp ("t", "typeExp", "The type of experience to launch", true, "", "string");
    cmd.add(arg_typeExp);

    TCLAP::ValueArg<std::string> arg_subTypeExp ("s", "subTypeExp", "", false, "", "string");
    cmd.add(arg_subTypeExp);

    TCLAP::ValueArg<std::string> arg_nodeType ("n", "nodeType", "The type of nodes", false, "small", "string");
    cmd.add(arg_nodeType);
    TCLAP::ValueArg<double> arg_nbNodes ("m", "nbNodes", "The number of nodes", false, 126, "int");
    cmd.add(arg_nbNodes);
    TCLAP::ValueArg<int> args_maxEvent ("e", "maxEvent", "", false, 100000, "int");
    cmd.add(args_maxEvent);
	TCLAP::ValueArg<double> args_churnRate ("c", "churnRate", "The system churn rate", false, 0.5, "double");
    cmd.add(args_churnRate);
	TCLAP::ValueArg<double> args_delay ("d", "delay", "The delay for the delay simulation", false, 0.1, "double");
    cmd.add(args_delay);

	#ifdef UPDATE
    TCLAP::ValueArg<double> args_update ("u", "update", "The update time for the node", false, 100, "double");
    cmd.add(args_update);
	#endif

    cmd.parse(argc, argv);

    std::string typeExp = arg_typeExp.getValue();
    std::string subTypeExp = arg_subTypeExp.getValue();
    std::string nodeType = arg_nodeType.getValue();

    double nbNodes = arg_nbNodes.getValue();
	double churnRate = args_churnRate.getValue();
	double delay = args_delay.getValue();


	#ifdef UPDATE
	double updateTime = args_update.getValue();
	#endif

    int maxEvent = args_maxEvent.getValue();

    if(nodeType == "rand") {
    	#ifdef UPDATE
		simulate<RandomNode>(typeExp, subTypeExp, nbNodes, maxEvent, churnRate, delay, updateTime);
		#else
		simulate<RandomNode>(typeExp, subTypeExp, nbNodes, maxEvent, churnRate, delay);
		#endif  
    } 
	else if (nodeType == "small") {
    	#ifdef UPDATE
		simulate<SmallestNode>(typeExp, subTypeExp, nbNodes, maxEvent, churnRate, delay, updateTime);
		#else
		simulate<SmallestNode>(typeExp, subTypeExp, nbNodes, maxEvent, churnRate, delay);
		#endif  
    } 
	else if (nodeType == "nointerr") {
    	#ifdef UPDATE
		simulate<NonInterruptNode>(typeExp, subTypeExp, nbNodes, maxEvent, churnRate, delay, updateTime);
		#else
		simulate<NonInterruptNode>(typeExp, subTypeExp, nbNodes, maxEvent, churnRate, delay);
		#endif  
    } 
	else if (nodeType == "partial") {
    	#ifdef UPDATE
		simulate<PartialInfoNode>(typeExp, subTypeExp, nbNodes, maxEvent, churnRate, delay, updateTime);
		#else
		simulate<PartialInfoNode>(typeExp, subTypeExp, nbNodes, maxEvent, churnRate, delay);
		#endif  
    }
    else if (nodeType == "heterogeneous") {
    	#ifdef UPDATE
		simulate<HeterogeneousNode>(typeExp, subTypeExp, nbNodes, maxEvent, churnRate, delay, updateTime);
		#else
		simulate<HeterogeneousNode>(typeExp, subTypeExp, nbNodes, maxEvent, churnRate, delay);
		#endif  
    }
    else if (nodeType == "local1") {
    	#ifdef UPDATE
		simulate<LocalInfoNode1>(typeExp, subTypeExp, nbNodes, maxEvent, churnRate, delay, updateTime);
		#else
		simulate<LocalInfoNode1>(typeExp, subTypeExp, nbNodes, maxEvent, churnRate, delay);
		#endif  
    }
    else if (nodeType == "local2") {
    	#ifdef UPDATE
		simulate<LocalInfoNode2>(typeExp, subTypeExp, nbNodes, maxEvent, churnRate, delay, updateTime);
		#else
		simulate<LocalInfoNode2>(typeExp, subTypeExp, nbNodes, maxEvent, churnRate, delay);
		#endif  
    }
    else if (nodeType == "local3") {
    	#ifdef UPDATE
		simulate<LocalInfoNode3>(typeExp, subTypeExp, nbNodes, maxEvent, churnRate, delay, updateTime);
		#else
		simulate<LocalInfoNode3>(typeExp, subTypeExp, nbNodes, maxEvent, churnRate, delay);
		#endif  
    }
    else if (nodeType == "full") {
    	#ifdef UPDATE
		simulate<FullInfoNode>(typeExp, subTypeExp, nbNodes, maxEvent, churnRate, delay, updateTime);
		#else
		simulate<FullInfoNode>(typeExp, subTypeExp, nbNodes, maxEvent, churnRate, delay);
		#endif  
    }	
    else if (nodeType == "smallestLFL") {
    	#ifdef UPDATE
		simulate<SmallestLFLNode>(typeExp, subTypeExp, nbNodes, maxEvent, churnRate, delay, updateTime);
		#else
		simulate<SmallestLFLNode>(typeExp, subTypeExp, nbNodes, maxEvent, churnRate, delay);
		#endif  
    }	
}

/**
Main function for the launching of the simulation
std::string _typeExp: The type of the experiment
std::string _subType: Subtype of the experiment To ignore
int _nbNodes: Number of nodes in the tree
int _maxEvents: Number maximum of events
double _churnRate: Explicit
double _delay: To ignore
*/
template<class Node>
#ifdef UPDATE
void simulate(std::string _typeExp, std::string _subType, int _nbNodes, int _maxEvents, double _churnRate, double _delay, double _updateTime) {
#else
void simulate(std::string _typeExp, std::string _subType, int _nbNodes, int _maxEvents, double _churnRate, double _delay) {
#endif  
    
    // All trees simulation
	if (_typeExp == "allTrees") {
        for(int i = 1; i < 31; ++i) {
			std::cout << "# of nodes : " << i << '\n';
			allTrees(i);
		}
    }
    // Main simulation. For churn between 0 (not included) and 10, 31 steps
	else if (_typeExp == "overTime") {
		int start = 0; 
		int stop = 10;
		std::cout << "Start : " << start << ", stop : " << stop << '\n';
		for(double churnRate : frange(start, stop, 31)) {	
			if(churnRate != 0) {
				std::cout << "Churn Rate : " << churnRate << ", # People : " << _nbNodes << '\n';
				Tree<Node> tree = Tree<Node>::balancedTree(2, _nbNodes);
				#ifdef UPDATE
				overTime<Node>(tree, _maxEvents, _churnRate, _updateTime);
				#else
				overTime<Node>(tree, _maxEvents, _churnRate);
				#endif  	
				
			}
		}
	}
    // Only simulate for churn rate given	
	else if (_typeExp == "one_overTime") {
		std::cout  << "overTime<" << getName<Node>() << ">(" << _nbNodes << ", " << _maxEvents << ", " << _churnRate << ")\n";
		Tree<Node> tree = Tree<Node>::balancedTree(2, _nbNodes);
		#ifdef UPDATE
		overTime<Node>(tree, _maxEvents, _churnRate, _updateTime);
		#else
		overTime<Node>(tree, _maxEvents, _churnRate);
		#endif  		
	}
    // main simulation for the interruptions metrics
	else if (_typeExp == "interrupt") {
		double start = 0; 
		double stop = 2;
		std::cout << "churnRate : (" << start << ", " << stop << ")\n";
		for(double churnRate : frange(start, stop, 31)) {
			if(churnRate != 0) {
				std::cout << "Churn Rate : " << churnRate << ", # People : " << _nbNodes << '\n';
				Tree<Node> tree = Tree<Node>::balancedTree(2, _nbNodes);
				#ifdef UPDATE
				interrupt<Node>(tree, _maxEvents, _churnRate, _updateTime);
				#else
				interrupt<Node>(tree, _maxEvents, _churnRate);
				#endif  
			}
		}
	}
    // Simulation for the main metrics distribution
	else if (_typeExp == "distribution") {
		Tree<Node> tree = Tree<Node>::balancedTree(2, _nbNodes);
		#ifdef UPDATE
		distribution<Node>(tree, _maxEvents, _churnRate, _updateTime);
		#else
		distribution<Node>(tree, _maxEvents, _churnRate);
		#endif  	
	}
    // Simulation for the distribution of interruption metrics distribution
	else if (_typeExp == "distributionInterr") {
		Tree<Node> tree = Tree<Node>::balancedTree(2, _nbNodes);
		#ifdef UPDATE
		interruptDistribution<Node>(tree, _maxEvents, _churnRate, _updateTime);
		#else
		interruptDistribution<Node>(tree, _maxEvents, _churnRate);
		#endif  		
	}

	/******************** start ***********************
	************** NEW DISTRIBUTIONS******************
	*************************************************/

	else if (_typeExp == "new_distr_1") {           
		auto bandwidthDistrib = bandwidthDistributionNew(1, _nbNodes);
		for(auto& v : bandwidthDistrib) {
			std::cout << v << '\t';
		}
		std::cout << '\n';
		Tree<Node> tree = Tree<Node>::balancedTree(bandwidthDistrib, _nbNodes);
		if ( _subType == "overTime" ) {
			#ifdef UPDATE
			overTime<Node>(tree, _maxEvents, _churnRate, _updateTime);
			#else
			overTime<Node>(tree, _maxEvents, _churnRate);
			#endif  
			tree.createDotFile("tree");
			
		}
		else if( _subType == "interrupt" ) {
			#ifdef UPDATE
			interrupt<Node>(tree, _maxEvents, _churnRate, _updateTime);
			#else
			interrupt<Node>(tree, _maxEvents, _churnRate);
			#endif 
			
		}
	}

	else if (_typeExp == "new_distr_2") {
		auto bandwidthDistrib = bandwidthDistribution2(_nbNodes);
		for(auto& v : bandwidthDistrib) {
			std::cout << v << '\t';
		}
		std::cout << '\n';
		Tree<Node> tree = Tree<Node>::balancedTree(bandwidthDistrib, _nbNodes);
		if ( _subType == "overTime" ) {
			#ifdef UPDATE
			overTime<Node>(tree, _maxEvents, _churnRate, _updateTime);
			#else
			overTime<Node>(tree, _maxEvents, _churnRate);
			#endif  
			tree.createDotFile("tree");
			
		}
		else if( _subType == "interrupt" ) {
			#ifdef UPDATE
			interrupt<Node>(tree, _maxEvents, _churnRate, _updateTime);
			#else
			interrupt<Node>(tree, _maxEvents, _churnRate);
			#endif  
			tree.createDotFile("tree");
			
		}	
	}
	else if (_typeExp == "new_distr_3") {
		auto bandwidthDistrib = bandwidthDistribution3(_nbNodes);
		for(auto& v : bandwidthDistrib) {
			std::cout << v << '\t';
		}
		std::cout << '\n';
		Tree<Node> tree = Tree<Node>::balancedTree(bandwidthDistrib, _nbNodes);
		if ( _subType == "overTime" ) {
			#ifdef UPDATE
			overTime<Node>(tree, _maxEvents, _churnRate, _updateTime);
			#else
			overTime<Node>(tree, _maxEvents, _churnRate);
			#endif  
			tree.createDotFile("tree");
			
		}
		else if( _subType == "interrupt" ) {
			#ifdef UPDATE
			interrupt<Node>(tree, _maxEvents, _churnRate, _updateTime);
			#else
			interrupt<Node>(tree, _maxEvents, _churnRate);
			#endif  
			tree.createDotFile("tree");
			
		}	
	}
	else if (_typeExp == "new_distr_4") {
		auto bandwidthDistrib = bandwidthDistribution4(_nbNodes);
		for(auto& v : bandwidthDistrib) {
			std::cout << v << '\t';
		}
		std::cout << '\n';
		Tree<Node> tree = Tree<Node>::balancedTree(bandwidthDistrib, _nbNodes);
		if ( _subType == "overTime" ) {
			#ifdef UPDATE
			overTime<Node>(tree, _maxEvents, _churnRate, _updateTime);
			#else
			overTime<Node>(tree, _maxEvents, _churnRate);
			#endif  
			tree.createDotFile("tree");
			
		}
		else if( _subType == "interrupt" ) {
			#ifdef UPDATE
			interrupt<Node>(tree, _maxEvents, _churnRate, _updateTime);
			#else
			interrupt<Node>(tree, _maxEvents, _churnRate);
			#endif  
			tree.createDotFile("tree");
			
		}	
	}

	else if (_typeExp == "new_distr_5") {
		auto bandwidthDistrib = bandwidthDistribution_fullUse(_nbNodes);
		for(auto& v : bandwidthDistrib) {
			std::cout << v << '\t';
		}
		Tree<Node> tree = Tree<Node>::balancedTree(bandwidthDistrib, _nbNodes);
		tree.createDotFile("tree");
		if ( _subType == "overTime" ) {
			#ifdef UPDATE
			overTime<Node>(tree, _maxEvents, _churnRate, _updateTime);
			#else
			overTime<Node>(tree, _maxEvents, _churnRate);
			#endif  
			tree.createDotFile("tree");
			
		}
		else if( _subType == "interrupt" ) {
			#ifdef UPDATE
			interrupt<Node>(tree, _maxEvents, _churnRate, _updateTime);
			#else
			interrupt<Node>(tree, _maxEvents, _churnRate);
			#endif  
			tree.createDotFile("tree");
			
		}
	}
	/******************** end ***********************
	************** NEW DISTRIBUTIONS******************
	*************************************************/

	else if (_typeExp == "hetero_semi") {
		Tree<Node> tree = Tree<Node>::balancedTree(bandwidthDistribution_semiUse(_nbNodes), _nbNodes);
		if ( _subType == "overTime" ) {
			#ifdef UPDATE
			overTime<Node>(tree, _maxEvents, _churnRate, _updateTime);
			#else
			overTime<Node>(tree, _maxEvents, _churnRate);
			#endif  
			tree.createDotFile("tree");
			
		}
		else if( _subType == "interrupt" ) {
			#ifdef UPDATE
			interrupt<Node>(tree, _maxEvents, _churnRate, _updateTime);
			#else
			interrupt<Node>(tree, _maxEvents, _churnRate);
			#endif  
			tree.createDotFile("tree");
			
		}
	}
	else {
		std::cerr << "Experiment type not valid: " << _typeExp << '\n';
		exit(-1);
	}
}

int main(int argc, char** argv)
{	
    srand(time(NULL));
    parseArgs(argc, argv);
    return 0;
}
 