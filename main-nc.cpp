#include <iostream>

#include <ctime>
#include <iostream>
#include <functional>
#include <algorithm>
#include <string>
#include <tclap/CmdLine.h>
#include <thread>
#include <utility>
#include <map>
#include <bitset>

#include "nonConstantTree.hpp"

#include "RandomNode.hpp"
#include "SmallestNode.hpp"
#include "Experiment.hpp"
#include "NonInterruptNode.hpp"
#include "PartialInfoNode.hpp"
#include "HeterogeneousNode.hpp"
#include "LocalInfoNode1.hpp"
#include "LocalInfoNode2.hpp"
#include "LocalInfoNode3.hpp"
#include "FullInfoNode.hpp"
#include "SmallestLFLNode.hpp"


#include "AllTrees.hpp"
#include "non_constant_population.hpp"
#include "names.hpp"
#include "utils.hpp"
#include "update.hpp"

#include "NonConstantExperiment.hpp"


std::vector<int> bandwidthDistribution_fullUse(int _nbNodes);
std::vector<int> bandwidthDistribution_semiUse(int _nbNodes);

template<class Node>
std::vector<double> asFunctionOfChurnRate(const std::vector<double> _range, Tree<Node>& _tree, std::function<int(Tree<Node>&)>& _f,
                           float _fixRate, int _expNbSteps, int _runSteps);

#ifdef UPDATE
template<class Node> void simulate(std::string _typeExp, std::string _subType, int _nbNodes, int _maxEvents1, int _maxEvents2, int _maxEvents3, double _churnRate1, double _churnRate2, double _churnRate3, double joinRate1,double joinRate2,double joinRate3, double _delay, double _updateTime);
#else
template<class Node> void simulate(std::string _typeExp, std::string _subType, int _nbNodes, int _maxEvents1, int _maxEvents2, int _maxEvents3, double _churnRate1, double _churnRate2, double _churnRate3, double joinRate1,double joinRate2,double joinRate3, double _delay);
#endif
template<class Node> void distanceOverChurn(double _minValue, double _maxValue, int _maxEvents, int _nbSteps);


/**
Parse the args and launch the simulation accordingly



example------>    ./NonConstant --j1 1000 --j2 1 --j3 1 --c1 0.003 --c2 1 --c3 1 --max1 10  --max2 5 --max3 5 -t hetero_opt -s interrupt -n local1



*/
void parseArgs(int argc, char** argv) {
    TCLAP::CmdLine cmd("");

    TCLAP::ValueArg<std::string> arg_typeExp ("t", "typeExp", "The type of experience to launch", true, "", "string");
    cmd.add(arg_typeExp);

    TCLAP::ValueArg<std::string> arg_subTypeExp ("s", "subTypeExp", "", false, "", "string");
    cmd.add(arg_subTypeExp);

    TCLAP::ValueArg<std::string> arg_nodeType ("n", "nodeType", "The type of nodes", false, "small", "string");
    cmd.add(arg_nodeType);
    TCLAP::ValueArg<double> arg_nbNodes ("m", "nbNodes", "The number of nodes", false, 126, "int");
    cmd.add(arg_nbNodes);

	TCLAP::ValueArg<double> args_delay ("d", "delay", "The delay for the delay simulation", false, 0.1, "double");
    cmd.add(args_delay);


    TCLAP::ValueArg<double> args_joinRate1 ("1", "j1", "The system join rate of phase 1", true, 0.5, "double");
    cmd.add(args_joinRate1);
    TCLAP::ValueArg<double> args_joinRate2 ("2", "j2", "The system join rate of phase 2", true, 0.5, "double");
    cmd.add(args_joinRate2);   
    TCLAP::ValueArg<double> args_joinRate3 ("3", "j3", "The system join rate of phase 3", true, 0.5, "double");
    cmd.add(args_joinRate3);

    TCLAP::ValueArg<double> args_churnRate1 ("4", "c1", "The system churn rate of phase 1", true, 0.5, "double");
    cmd.add(args_churnRate1);
    TCLAP::ValueArg<double> args_churnRate2 ("5", "c2", "The system churn rate of phase 2", true, 0.5, "double");
    cmd.add(args_churnRate2);   
    TCLAP::ValueArg<double> args_churnRate3 ("6", "c3", "The system churn rate of phase 3", true, 0.5, "double");
    cmd.add(args_churnRate3);

    TCLAP::ValueArg<int> args_maxEvent1 ("7", "max1", "", true, 100000, "int");
    cmd.add(args_maxEvent1);
    TCLAP::ValueArg<int> args_maxEvent2 ("8", "max2", "", true, 100000, "int");
    cmd.add(args_maxEvent2);
    TCLAP::ValueArg<int> args_maxEvent3 ("9", "max3", "", true, 100000, "int");
    cmd.add(args_maxEvent3);
    
    #ifdef UPDATE
    TCLAP::ValueArg<double> args_update ("u", "update", "The update time for the node", true, 100, "double");
    cmd.add(args_update);
    #endif

    cmd.parse(argc, argv);

    std::string typeExp = arg_typeExp.getValue();
    std::string subTypeExp = arg_subTypeExp.getValue();
    std::string nodeType = arg_nodeType.getValue();

    double nbNodes = arg_nbNodes.getValue();
	double delay = args_delay.getValue();

	double churnRate1 = args_churnRate1.getValue();
	double churnRate2 = args_churnRate2.getValue();
	double churnRate3 = args_churnRate3.getValue();

	double joinRate1 = args_joinRate1.getValue();
	double joinRate2 = args_joinRate2.getValue();
	double joinRate3 = args_joinRate3.getValue();


    int maxEvent1 = args_maxEvent1.getValue();
    int maxEvent2 = args_maxEvent2.getValue();
    int maxEvent3 = args_maxEvent3.getValue();

    #ifdef UPDATE
    double updateTime = args_update.getValue();
    #endif


    if(nodeType == "rand") {
        #ifdef UPDATE
        simulate<RandomNode>(typeExp, subTypeExp, nbNodes, maxEvent1,maxEvent2,maxEvent3, churnRate1,churnRate2,churnRate3,joinRate1,joinRate2,joinRate3 , delay, updateTime);
        #else
        simulate<RandomNode>(typeExp, subTypeExp, nbNodes, maxEvent1,maxEvent2,maxEvent3, churnRate1,churnRate2,churnRate3,joinRate1,joinRate2,joinRate3 , delay);
        #endif  
    } 
	else if (nodeType == "small") {
        #ifdef UPDATE
        simulate<SmallestNode>(typeExp, subTypeExp, nbNodes, maxEvent1,maxEvent2,maxEvent3, churnRate1,churnRate2,churnRate3,joinRate1,joinRate2,joinRate3 , delay, updateTime);
        #else
        simulate<SmallestNode>(typeExp, subTypeExp, nbNodes, maxEvent1,maxEvent2,maxEvent3, churnRate1,churnRate2,churnRate3,joinRate1,joinRate2,joinRate3 , delay);
        #endif  
    } 
	else if (nodeType == "nointerr") {

        #ifdef UPDATE
        simulate<NonInterruptNode>(typeExp, subTypeExp, nbNodes, maxEvent1,maxEvent2,maxEvent3, churnRate1,churnRate2,churnRate3,joinRate1,joinRate2,joinRate3 , delay, updateTime);
        #else
        simulate<NonInterruptNode>(typeExp, subTypeExp, nbNodes, maxEvent1,maxEvent2,maxEvent3, churnRate1,churnRate2,churnRate3,joinRate1,joinRate2,joinRate3 , delay);
        #endif  
    } 
	else if (nodeType == "partial") {
        #ifdef UPDATE
        simulate<PartialInfoNode>(typeExp, subTypeExp, nbNodes, maxEvent1,maxEvent2,maxEvent3, churnRate1,churnRate2,churnRate3,joinRate1,joinRate2,joinRate3 , delay, updateTime);
        #else
        simulate<PartialInfoNode>(typeExp, subTypeExp, nbNodes, maxEvent1,maxEvent2,maxEvent3, churnRate1,churnRate2,churnRate3,joinRate1,joinRate2,joinRate3 , delay);
        #endif  
    }
    else if (nodeType == "heterogeneous") {
        #ifdef UPDATE
        simulate<HeterogeneousNode>(typeExp, subTypeExp, nbNodes, maxEvent1,maxEvent2,maxEvent3, churnRate1,churnRate2,churnRate3,joinRate1,joinRate2,joinRate3 , delay, updateTime);
        #else
        simulate<HeterogeneousNode>(typeExp, subTypeExp, nbNodes, maxEvent1,maxEvent2,maxEvent3, churnRate1,churnRate2,churnRate3,joinRate1,joinRate2,joinRate3 , delay);
        #endif  
    }
    else if (nodeType == "local1") {
        #ifdef UPDATE
        simulate<LocalInfoNode1>(typeExp, subTypeExp, nbNodes, maxEvent1,maxEvent2,maxEvent3, churnRate1,churnRate2,churnRate3,joinRate1,joinRate2,joinRate3 , delay, updateTime);
        #else
        simulate<LocalInfoNode1>(typeExp, subTypeExp, nbNodes, maxEvent1,maxEvent2,maxEvent3, churnRate1,churnRate2,churnRate3,joinRate1,joinRate2,joinRate3 , delay);
        #endif  
    }
    else if (nodeType == "local2") {
        #ifdef UPDATE
        simulate<LocalInfoNode2>(typeExp, subTypeExp, nbNodes, maxEvent1,maxEvent2,maxEvent3, churnRate1,churnRate2,churnRate3,joinRate1,joinRate2,joinRate3 , delay, updateTime);
        #else
        simulate<LocalInfoNode2>(typeExp, subTypeExp, nbNodes, maxEvent1,maxEvent2,maxEvent3, churnRate1,churnRate2,churnRate3,joinRate1,joinRate2,joinRate3 , delay);
        #endif  
    }
    else if (nodeType == "local3") {
        #ifdef UPDATE
        simulate<LocalInfoNode3>(typeExp, subTypeExp, nbNodes, maxEvent1,maxEvent2,maxEvent3, churnRate1,churnRate2,churnRate3,joinRate1,joinRate2,joinRate3 , delay, updateTime);
        #else
        simulate<LocalInfoNode3>(typeExp, subTypeExp, nbNodes, maxEvent1,maxEvent2,maxEvent3, churnRate1,churnRate2,churnRate3,joinRate1,joinRate2,joinRate3 , delay);
        #endif  
    }
    else if (nodeType == "full") {
        #ifdef UPDATE
        simulate<FullInfoNode>(typeExp, subTypeExp, nbNodes, maxEvent1,maxEvent2,maxEvent3, churnRate1,churnRate2,churnRate3,joinRate1,joinRate2,joinRate3 , delay, updateTime);
        #else
        simulate<FullInfoNode>(typeExp, subTypeExp, nbNodes, maxEvent1,maxEvent2,maxEvent3, churnRate1,churnRate2,churnRate3,joinRate1,joinRate2,joinRate3 , delay);
        #endif  
    }	
    else if (nodeType == "smallestLFL") {
        #ifdef UPDATE
        simulate<SmallestLFLNode>(typeExp, subTypeExp, nbNodes, maxEvent1,maxEvent2,maxEvent3, churnRate1,churnRate2,churnRate3,joinRate1,joinRate2,joinRate3 , delay, updateTime);
        #else
        simulate<SmallestLFLNode>(typeExp, subTypeExp, nbNodes, maxEvent1,maxEvent2,maxEvent3, churnRate1,churnRate2,churnRate3,joinRate1,joinRate2,joinRate3 , delay);
        #endif  
    }   
}

/*
Main function for the launching of the simulation
std::string _typeExp: The type of the experiment
std::string _subType: Subtype of the experiment To ignore
int _nbNodes: Number of nodes in the tree
int _maxEvents: Number maximum of events
double _churnRate: Explicit
double _delay: To ignore
*/
template<class Node>
#ifdef UPDATE
void simulate(std::string _typeExp, std::string _subType, int _nbNodes, int _maxEvents1, int _maxEvents2, int _maxEvents3, double _churnRate1, double _churnRate2, double _churnRate3, double _joinRate1,double _joinRate2,double _joinRate3, double _delay, double _updateTime) {
#else
void simulate(std::string _typeExp, std::string _subType, int _nbNodes, int _maxEvents1, int _maxEvents2, int _maxEvents3, double _churnRate1, double _churnRate2, double _churnRate3, double _joinRate1,double _joinRate2,double _joinRate3, double _delay) {
#endif

    int (*f)(void);
    
    // optimistic estimate

 	if (_typeExp == "new_distr_1") {  
		f = [](void){
				std::discrete_distribution<> dist({0.493, 0.187, 0.084, 0.00306, 0.00306,0.00306,0.00306,0.00306,0.00306,0.00306,0.00306,0.00306,0.00306,0.00306,0.00306,0.00306,0.00306,0.00306, 0.00306,0.00306,0.184});
    			std::random_device rd; 
	 			std::mt19937 gen(rd());
	 			return dist(gen);
				};
	}

    else if (_typeExp == "new_distr_2") {
        f = [](void){
                std::discrete_distribution<> dist({0,0.77,0,0.09,0,0,0,0,0.03,0,0,0,0,0,0,0,0,0,0,0,0.11});
                std::random_device rd; 
                std::mt19937 gen(rd());
                return dist(gen);
                };
    }
    else if (_typeExp == "new_distr_3") {
        f = [](void){
                std::discrete_distribution<> dist({0.2,0.4,0,0,0.25,0,0,0,0,0,0,0,0,0,0,0,0.15});
                std::random_device rd; 
                std::mt19937 gen(rd());
                return dist(gen);
                };
    }
    else if (_typeExp == "new_distr_4") {
        f = [](void){
                std::discrete_distribution<> dist({0.1,0.376,0.08,0.281,0.163});
                std::random_device rd; 
                std::mt19937 gen(rd());
                return dist(gen);
                };       
    }

	// pfe distribution

	else if (_typeExp == "new_distr_5") {
		f = [](void){
				std::discrete_distribution<> dist({0, 0.41, 0.42, 0, 0.12});
    			std::random_device rd; 
	 			std::mt19937 gen(rd());
	 			return dist(gen);
				};
	}
	 else {
		std::cerr << "Experiment type not valid: " << _typeExp << std::endl;
		exit(-1);
	}

	/*
	subtype
	*/
	if ( _subType == "overTime" ) {
		NonConstantTree<Node> tree = NonConstantTree<Node>();
        #ifdef UPDATE
        overTime<Node>(tree, _maxEvents1, _joinRate1, _churnRate1, 1, f, _updateTime);
        overTime<Node>(tree, _maxEvents2, _joinRate2, _churnRate2, 2, f, _updateTime);
        overTime<Node>(tree, _maxEvents3, _joinRate3, _churnRate3, 3, f, _updateTime);
        #else
        overTime<Node>(tree, _maxEvents1, _joinRate1, _churnRate1, 1, f);
        overTime<Node>(tree, _maxEvents2, _joinRate2, _churnRate2, 2, f);
        overTime<Node>(tree, _maxEvents3, _joinRate3, _churnRate3, 3, f);   
        #endif

	}
	else if( _subType == "interrupt" ) {
		NonConstantTree<Node> tree = NonConstantTree<Node>();
        #ifdef UPDATE
        interrupt<Node>(tree, _maxEvents1, _joinRate1, _churnRate1, 1, f, _updateTime);
        interrupt<Node>(tree, _maxEvents2, _joinRate2, _churnRate2, 2, f, _updateTime);
        interrupt<Node>(tree, _maxEvents3, _joinRate3, _churnRate3, 3, f, _updateTime);
        #else
        interrupt<Node>(tree, _maxEvents1, _joinRate1, _churnRate1, 1, f);
        interrupt<Node>(tree, _maxEvents2, _joinRate2, _churnRate2, 2, f);
        interrupt<Node>(tree, _maxEvents3, _joinRate3, _churnRate3, 3, f);
        #endif

	}
}

int main(int argc, char** argv)
{	
    srand(time(NULL));
    parseArgs(argc, argv);
    return 0;
}
 