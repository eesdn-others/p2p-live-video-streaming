#include "SmallestNode.hpp"

#include <iostream>
//#include <bitset>
#include <algorithm>



SmallestNode::SmallestNode(int _degree, Tree<SmallestNode>* _tree, int _parent) :
    Node(_degree, _parent, 0),
	m_tree(_tree)
{
}

#ifdef UPDATE
void SmallestNode::update(){
	;
}
#endif


void SmallestNode::addChild(NodeId _id) {
    auto& child = m_tree->getNode(_id);
    
	this->m_children.insert(find_first_smaller(child.getSubtreeSize()), _id);
	child.setParent(getOwnInd()); // I am your father
	#ifdef UPDATE
	this->changeSubtreeSize(child.getSubtreeSize());
	#endif
}

void SmallestNode::removeFromChildren(NodeId _id) {
	auto& child = m_tree->getNode(_id);
    this->m_children.remove(_id);
    #ifdef UPDATE
	this->changeSubtreeSize(-child.getSubtreeSize());
	#endif
	child.setParent(-1);
}

void SmallestNode::die() {
	auto& parent = m_tree->getNode(getParent());

	parent.removeFromChildren(getOwnInd());
	setParent(-1);
	auto newFatherIdIte = this->m_children.begin();
	//If some children
	if(newFatherIdIte != this->m_children.end()) {
		auto& newFather = m_tree->getNode(*newFatherIdIte);
		parent.adopt(getOwnInd(), newFatherIdIte);
		while(!this->m_children.empty()) {
			newFather.adopt(getOwnInd(), this->m_children.begin());
		}
	}
}

bool SmallestNode::repair() {
    if((int) this->m_children.size() > m_degree && m_degree) {
        std::list<NodeId>::iterator ite = this->m_children.begin();
        std::advance(ite, m_degree-1);
        auto newFatherIte = ite;
        std::advance(ite, 1);
        auto newSonIte = ite;
		m_tree->getNode(*newFatherIte).adopt(getOwnInd(), newSonIte);
		return true;
    }
	return false;
}

void SmallestNode::adopt(NodeId _oldFatherId, std::list<NodeId>::iterator _iteChildInd) {
	auto& oldFather = m_tree->getNode(_oldFatherId);
	auto& child = m_tree->getNode(*_iteChildInd);
	
	child.setParent(getOwnInd());
	

	this->m_children.splice(find_first_smaller(child.getSubtreeSize()), 
		oldFather.m_children, _iteChildInd);
	#ifdef UPDATE
	oldFather.changeSubtreeSize(-child.getSubtreeSize());
	this->changeSubtreeSize(child.getSubtreeSize());
	#endif
	// exit(0);
}

std::list<NodeId>::iterator SmallestNode::find_first_smaller(int _subtreeSize) {
	auto ite = this->m_children.begin();
    while(ite != this->m_children.end() && m_tree->getNode(*ite).m_subtreeSize >= _subtreeSize) {
		++ite;
	}
	return ite;
}

void SmallestNode::changeSubtreeSize(int _delta) {
	// std::cout << getOwnInd() << ".changeSubtreeSize(" << _delta <<")" << std::endl;
	m_subtreeSize += _delta;
	if(this->hasParent()) {
		m_tree->getNode(this->getParent()).resort(getOwnInd(), _delta);
	}
}

/**
* Re sort the children list, knowing that *_sonId* changed its subtree size by _delta 
*/

void SmallestNode::resort(NodeId _sonId, int _delta) {
	auto iteSon = std::find(this->m_children.begin(), this->m_children.end(), _sonId);
	if(_delta > 0 && iteSon != this->m_children.begin()) {
		auto ite = this->m_children.begin();
		while(ite != iteSon && 
			m_tree->getNode(*ite).m_subtreeSize >= m_tree->getNode(*iteSon).m_subtreeSize) {
			++ite;
		}
		if(ite != iteSon) {
			this->m_children.splice(ite, this->m_children, iteSon);
		}
	}
	else if (_delta < 0 && iteSon != this->m_children.end()) { // Less nodes
		auto ite = std::next(iteSon), iteStart = ite;
		while(ite != this->m_children.end() && 
			m_tree->getNode(*ite).m_subtreeSize > m_tree->getNode(*iteSon).m_subtreeSize) {
			++ite;
		}
		if(ite != iteStart) {
			this->m_children.splice(ite, this->m_children, iteSon); 
		}
	}
	this->changeSubtreeSize(_delta);
}


uint64_t SmallestNode::getHash() const {
    uint64_t result = 0;
    for(auto sonInd : this->m_children) {
		auto& son = m_tree->getNode(sonInd);
        result = result << 1;
        ++result;
        size_t shift = (2*son.getSubtreeSize()-2);
        if(shift > 0) {
            result = result << shift;
            uint64_t hash = son.getHash();
            result += hash;
        }
        result = result << 1;
    }
    return result;
}

bool SmallestNode::isReachable() const {
    if(getOwnInd() == 0) {
        return true;
    }
	auto& parent = m_tree->getNode(getParent());
    if(parent.isReachable()) {
        int i = 0;
        auto ite = parent.m_children.begin();
        while(i < parent.m_degree) {
            if(*ite == getOwnInd()) {
                if(m_new == true) m_new = false;
                return true;
            }
            ++ite;
            ++i;
        }
    }
    return false;
}

int SmallestNode::getBalance() const {
	int balance = 0;
	if(!m_children.empty()) {
		auto iteFirstChild = m_children.begin();
		auto iteSecondChild = ++m_children.begin();
		//If not only child and is a leaf
		if(iteSecondChild != m_children.end() && !m_tree->getNode(*iteFirstChild).getChildren().empty()) {
			balance += m_tree->getNode(*iteFirstChild).getSubtreeSize();
			for(; iteSecondChild != m_children.end(); ++iteSecondChild) {
				balance -= m_tree->getNode(*iteSecondChild).getSubtreeSize();
			}
		}
	}
	return balance;
}


int SmallestNode::getDepth() const {
    if(getOwnInd() == 0) {
        return 0;
    }
    auto& parent = m_tree->getNode(getParent());

    return 1+parent.getDepth();
}