#include "SmallestLFLNode.hpp"

#include <algorithm>
#include <iostream>
#include <cassert>
#include "MyRandom.hpp"
#include <cstdlib> 
#include <cmath>

SmallestLFLNode::SmallestLFLNode(int _degree, Tree<SmallestLFLNode>* _tree, int _parent) :
Node(_degree, _parent, 0),
m_tree(_tree)
{
}


//update last full level map
void SmallestLFLNode::update(){
    this->lastFullLevel.clear();
    for(auto it = m_children.begin();it!=std::next(m_children.begin(),m_degree) && m_tree->getNode(*it).getDegree() ;it++){
        unsigned childrenLastFullLevel = m_tree->getNode(*it).getLastFullLevel();
        unsigned availableBandwidth = m_tree->getNode(*it).getAvailableSpace(childrenLastFullLevel);
        this->lastFullLevel.insert(std::make_pair(*it, std::make_pair(childrenLastFullLevel, availableBandwidth)));
    }
}

void SmallestLFLNode::updateReceivers() {
    int i = 0;
    for(auto it : m_children){
        if(i++ < m_degree){
            m_tree->getNode(it).setReachable(true);
        }
        else{
            m_tree->getNode(it).setReachable(false);
        }
    }
}

unsigned SmallestLFLNode::getLastFullLevel(){
    
    if ((int) m_children.size() < m_degree) return 0;
    if(!m_degree) return m_tree->getNodes().size();
    int LFL = m_tree->getNodes().size();
    
    for(auto son : m_children){
        if(m_tree->getNode(son).getDegree()){
             int SFL = m_tree->getNode(son).getLastFullLevel();
             if(SFL < LFL){
                LFL=SFL;
            }
        }
    }
    return LFL+1;
}

unsigned SmallestLFLNode::getAvailableSpace(unsigned level){

    if(level == 0) return std::max((int)(getDegree() - m_children.size()), 0);
    int sum = 0;
    for(auto it : m_children){
        sum+= m_tree->getNode(it).getAvailableSpace(level - 1);
    }
    return sum;
}


void SmallestLFLNode::addChild(unsigned _id) {
    assert(m_degree > 0);
    //std::cout << "m_tree : " << m_tree << std::endl;
    //std::cout << getOwnInd() << ".addChild("<<_id<<')'<<std::endl;
    
    // get the node reference
    SmallestLFLNode& node = m_tree->getNode(_id);

    // add the id of the node in the list of sons
    m_children.insert(find_first_smaller(node.getDegree(),node.getSubtreeSize()), _id);

    // I set me as node parent
    node.setParent(getOwnInd());
    // change the subtree size
    this->changeSubtreeSize(node.m_subtreeSize);

    this->updateReceivers();
}

void SmallestLFLNode::die() {

    auto& parent = m_tree->getNode(getParent());

    if(m_children.size() == 0){
        parent.removeFromChildren(getOwnInd());
        return;
    }

    while (!m_children.empty()){
        parent.adopt(getOwnInd(), m_children.begin());
    }
    parent.removeFromChildren(getOwnInd());

    parent.updateReceivers();

    return;
}

std::list<NodeId>::iterator SmallestLFLNode::find_first_smaller(int degree, int subtreeSize) {
    auto ite = this->m_children.begin();
    while(ite != this->m_children.end()){
        if(m_tree->getNode(*ite).getDegree() < degree || (m_tree->getNode(*ite).getDegree() == degree && m_tree->getNode(*ite).getSubtreeSize() < subtreeSize)){
            break;
        }
        ++ite;
    } 
        
    
    return ite;
}


void SmallestLFLNode::adopt(NodeId _oldFatherId, std::list<NodeId>::iterator _iteChildInd) {
    assert(m_degree > 0);
    auto& oldFather = m_tree->getNode(_oldFatherId);
    auto& child = m_tree->getNode(*_iteChildInd);
    
    child.setParent(getOwnInd());
    
    this->m_children.splice(find_first_smaller(child.getDegree(),child.getSubtreeSize()), oldFather.m_children, _iteChildInd);
    oldFather.changeSubtreeSize(-child.getSubtreeSize());
    this->changeSubtreeSize(child.getSubtreeSize());

    oldFather.updateReceivers();
    this->updateReceivers();
}

void SmallestLFLNode::replace(NodeId _old, NodeId _new) {
    // std::cout << _new << " replaces " << _old << " at " << getOwnInd() << std::endl;
    SmallestLFLNode& oldNode = m_tree->getNode(_old);
    SmallestLFLNode& newNode = m_tree->getNode(_new);
    
    oldNode.removeFromChildren(_new);
    auto ite = std::find(m_children.begin(), m_children.end(), _old);
    
    newNode.setParent(getOwnInd());
    oldNode.setParent(-1);
    *ite = _new;
    int newSS = newNode.m_subtreeSize;
    int oldSS = oldNode.m_subtreeSize;
    
    this->changeSubtreeSize(newSS-oldSS);
}

void SmallestLFLNode::removeFromChildren(NodeId _child) {
    // std::cout << _child << " leaves " << getOwnInd() << std::endl;
    SmallestLFLNode& child = m_tree->getNode(_child);
    m_children.remove(_child);
    this->changeSubtreeSize(-child.m_subtreeSize);
    child.setParent(-1);
}


bool SmallestLFLNode::repair() {
    
    int size = m_children.size();

        //if all the children have degree 0

    if( getOwnInd() && size > m_degree && !m_tree->getNode(m_children.front()).getDegree()){

        auto it = std::next(m_children.begin(), m_degree);

        while(it != m_children.end()){
                        
            //node is killed and rejoin the system

            m_tree->getNode(0).adopt(getOwnInd(), it);
                        
            it = std::next(m_children.begin(), m_degree);

        }
    }

    
    //if overloaded && the node has degree > 0 && there exists a son with degree > 0 ... 
    if( (int) m_children.size() > m_degree && m_degree && m_tree->getNode(m_children.front()).getDegree()) {

#ifdef UPDATE
        //remove in lastFullLevel the node that are no more my children or that do not receive the video
        auto it = lastFullLevel.begin();
        while (it != lastFullLevel.end()) {
            if (std::find(m_children.begin(), m_children.end(), it->first) ==  m_children.end()  ||  !m_tree->getNode(it->first).isReachable()){
                it = lastFullLevel.erase(it);
            }
            else {
                ++it;
            }   
        }   
#else
        // normal case, I update during each repair
        this->update();
#endif

        while((int) m_children.size() > m_degree){

            auto childrenToAdopt = std::next(m_children.begin(),m_degree);

            //in the normal case is always the used branch, the else is used just in the UPDATE CASE 
            if(lastFullLevel.size()){  

                int nodeMinLFL = -1, minLFL = std::numeric_limits<int>::max(),available = 0;

                for(auto it: m_children){
                    if (lastFullLevel.find(it) != lastFullLevel.end()) {

                        //found
                        if(lastFullLevel[it].first < minLFL || (lastFullLevel[it].first == minLFL && lastFullLevel[it].second > available)){
                            minLFL = lastFullLevel[it].first;
                            available = lastFullLevel[it].second;
                            nodeMinLFL = it;
                        }
                    } 
                }
                // when at level L there are no more available slots, I update L to L+1 
                if(!--lastFullLevel[nodeMinLFL].second){

                    lastFullLevel[nodeMinLFL].first+=1;
                    lastFullLevel[nodeMinLFL].second=std::pow(m_tree->getNode(nodeMinLFL).getDegree(),lastFullLevel[nodeMinLFL].first);
                }
                m_tree->getNode(nodeMinLFL).adopt(getOwnInd(),std::find(m_children.begin(),m_children.end(), *childrenToAdopt));
            }        

            //if empty I push randomly on my children (with degree > 0)
            else{      
              
                int nodesWithBandwidthGrZero = 0;
                for(auto it = m_children.begin();it!=std::next(m_children.begin(),m_degree) && m_tree->getNode(*it).getDegree() ;it++){
                    ++nodesWithBandwidthGrZero;
                }
                std::mt19937 rng;
                rng.seed(std::random_device()());
                std::uniform_int_distribution<std::mt19937::result_type> dist6(0,nodesWithBandwidthGrZero-1);
                auto nodeToPush = std::next(m_children.begin(), dist6(rng));
                m_tree->getNode(*nodeToPush).adopt(getOwnInd(),std::find(m_children.begin(),m_children.end(), *childrenToAdopt));
            }
        }

        return true;
    }
    return false;
}



bool SmallestLFLNode::isReachable() const {
    return reachable;
}




int SmallestLFLNode::getBalance() const {
    int balance = 0;
    if(!m_children.empty()) {
        auto iteFirstChild = m_children.begin();
        auto iteSecondChild = ++m_children.begin();
        //If not only child and is a leaf
        if(iteSecondChild != m_children.end() && !m_tree->getNode(*iteFirstChild).m_children.empty()) {
            balance += m_tree->getNode(*iteFirstChild).m_subtreeSize;
            for(; iteSecondChild != m_children.end(); ++iteSecondChild) {
                balance -= m_tree->getNode(*iteSecondChild).m_subtreeSize;
            }
        }
    }
    return balance;
}

void SmallestLFLNode::changeSubtreeSize(int _delta) {
    // std::cout << "("<<getOwnInd()<<", "<<getParent()<<")" << ".changeSubtreeSize(" << _delta <<") : " << m_subtreeSize;
    m_subtreeSize += _delta;
    // std::cout << " => " << m_subtreeSize << "-->" << this << std::endl;
    if(this->hasParent()) {
        // std::cout << getOwnInd() << "^" << getParent() << std::endl;
        m_tree->getNode(this->getParent()).changeSubtreeSize(_delta);
    }
}


int SmallestLFLNode::getDepth() const {
    if(getOwnInd() == 0) {
        return 0;
    }
    auto& parent = m_tree->getNode(getParent());

    return 1+parent.getDepth();
}



