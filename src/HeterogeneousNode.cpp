#include "HeterogeneousNode.hpp"

#include <algorithm>
#include <iostream>
#include <cassert>
#include "MyRandom.hpp"
#include <cstdlib> 
HeterogeneousNode::HeterogeneousNode(int _degree, Tree<HeterogeneousNode>* _tree, int _parent) :
Node(_degree, _parent, 0),
m_tree(_tree)
{
}


//update last full level map
void HeterogeneousNode::update(){
    this->lastFullLevel.clear();
    for(auto it = m_children.begin();it!=m_children.end();it++){
        if(m_tree->getNode(*it).getDegree()){
            unsigned childrenLastFullLevel = m_tree->getNode(*it).getLastFullLevel();
            unsigned availableBandwidth = m_tree->getNode(*it).getAvailableSpace(childrenLastFullLevel);
           // std::cout<<*it<<"   "<<childrenLastFullLevel<<"   "<<availableBandwidth<<"\n";
            this->lastFullLevel.insert(std::make_pair(*it, std::make_pair(childrenLastFullLevel, availableBandwidth)));
        }
    }
}


/*
return the subtree rooted in the node
*/
void HeterogeneousNode::getSubtree(std::vector<unsigned> &subtree){
    
    subtree.push_back(getOwnInd());

    for(auto it=m_children.begin();it!=m_children.end();it++){

        m_tree->getNode(*it).getSubtree(subtree);
    }
}

/*
return the level of the node in the subtree
*/
unsigned HeterogeneousNode::getLevel(unsigned root_subtree){
    if(getOwnInd() == root_subtree) return 0;
    return 1 + m_tree->getNode(getParent()).getLevel(root_subtree);
}


unsigned HeterogeneousNode::getAvailableSpace(unsigned level){
    std::vector<unsigned> subtree;
    this->getSubtree(subtree);
    unsigned sum = 0;
    for(auto it = subtree.begin();it!=subtree.end();it++){
        if(m_tree->getNode(*it).getLevel(getOwnInd()) == level){
            sum += m_tree->getNode(*it).getDegree() - m_tree->getNode(*it).getChildren().size();
        }
    }
    return sum;
}


void HeterogeneousNode::addChild(unsigned _id) {
    //std::cout << "m_tree : " << m_tree << std::endl;
    //std::cout << getOwnInd() << ".addChild("<<_id<<')'<<std::endl;
    
    // get the node reference
    HeterogeneousNode& node = m_tree->getNode(_id);
    // add the id of the node in the list of sons
    m_children.push_back(_id);
    // I set me as node parent
    node.setParent(getOwnInd());
    // change the subtree size
    this->changeSubtreeSize(node.m_subtreeSize);
}

void HeterogeneousNode::die() {
  
            HeterogeneousNode& parent = m_tree->getNode(getParent());
    if(m_children.size() == 0){
        parent.removeFromChildren(getOwnInd());
           //      for(auto i=0;i<1;i++) std::cout<<m_tree->getNode(i)<<std::endl;

//     std::cout << "Press Enter to Continue";
//std::cin.ignore();
        return;
    }
    while (!m_children.empty()){
        m_tree->getRoot().adopt(getOwnInd(), m_children.begin());
    }
    parent.removeFromChildren(getOwnInd());
    return;

    /*
    auto a=getParent();
  std::cout<<"******************************************b4*******************************************************************"<<std::endl;
    std::cout<<"called DIE on  "<< getOwnInd()<<" that has "<<m_children.size()<<" children and bandwidth "<< m_degree<<std::endl;
    std::cout<<"NODE---->"<<m_tree->getNode(getOwnInd())<<std::endl;
    std::cout<<"PARENT---->"<<m_tree->getNode(a)<<std::endl;
    std::cout<<"*************************************************************************************************************"<<std::endl;

    LocalInfoNode2& parent = m_tree->getNode(getParent());
    */
    if(m_children.size() == 0){
        //std::cout<<parent<<std::endl;
        m_tree->getNode(getParent()).removeFromChildren(getOwnInd());
      /*  
        std::cout<<"******************************************after NC******************************************************************"<<std::endl;
        std::cout<<"NODE---->"<<m_tree->getNode(getOwnInd())<<std::endl;
    std::cout<<"PARENT---->"<<m_tree->getNode(a)<<std::endl;
        std::cout<<"*************************************************************************************************************"<<std::endl;
        */
        return;
    }
    auto son = m_children.begin();
    // if the node has at least one children i get the node with highest bandwidth between them
    auto sonMaxBW = son;
    while (son != m_children.end()){
        if (m_tree->getNode(*son).getDegree() > m_tree->getNode(*sonMaxBW).getDegree()){
            sonMaxBW = son;
        }
        son++;
    }
    HeterogeneousNode& newFather = m_tree->getNode(*sonMaxBW);
    m_tree->getNode(getParent()).replace(getOwnInd(), *sonMaxBW);
    
    if(m_children.size() > 0){
        while(!m_children.empty()) {
            newFather.adopt(getOwnInd(), m_children.begin());
        }
    }
    /*
    std::cout<<"******************************************after YC*******************************************************************"<<std::endl;
    std::cout<<"NODE---->"<<m_tree->getNode(getOwnInd())<<std::endl;
    std::cout<<"PARENT---->"<<m_tree->getNode(a)<<std::endl;
    std::cout<<"*************************************************************************************************************"<<std::endl;
    */
}



void HeterogeneousNode::adopt(NodeId _oldFatherInd, std::list<NodeId>::iterator _iteChildInd) {
    // std::cout << getOwnInd() << " adopt " << *_iteChildInd << " from " << _oldFatherInd << ", new SS :"
    // << m_tree->getNode(*_iteChildInd).m_subtreeSize << std::endl;
    
    m_children.splice(m_children.end(), m_tree->getNode(_oldFatherInd).m_children, _iteChildInd);
    m_tree->getNode(*_iteChildInd).setParent(getOwnInd());
    this->changeSubtreeSize(m_tree->getNode(*_iteChildInd).m_subtreeSize);
    m_tree->getNode(_oldFatherInd).changeSubtreeSize(-m_tree->getNode(*_iteChildInd).m_subtreeSize);
}

void HeterogeneousNode::replace(NodeId _old, NodeId _new) {
    // std::cout << _new << " replaces " << _old << " at " << getOwnInd() << std::endl;
    HeterogeneousNode& oldNode = m_tree->getNode(_old);
    HeterogeneousNode& newNode = m_tree->getNode(_new);
    
    oldNode.removeFromChildren(_new);
    auto ite = std::find(m_children.begin(), m_children.end(), _old);
    
    newNode.setParent(getOwnInd());
    oldNode.setParent(-1);
    *ite = _new;
    int newSS = newNode.m_subtreeSize;
    int oldSS = oldNode.m_subtreeSize;
    
    this->changeSubtreeSize(newSS-oldSS);
}

void HeterogeneousNode::removeFromChildren(NodeId _child) {
    // std::cout << _child << " leaves " << getOwnInd() << std::endl;
    HeterogeneousNode& child = m_tree->getNode(_child);
    m_children.remove(_child);
    this->changeSubtreeSize(-child.m_subtreeSize);
    child.setParent(-1);
}



unsigned HeterogeneousNode::getLastFullLevel(){
    
    if ((int)m_children.size() < m_degree) return 0;
    if (!m_degree) return std::numeric_limits<int>::max()/2;

    int LFL = std::numeric_limits<int>::max()/2;
    
    std::list<NodeId>::iterator son;
    auto children = m_children;
    for(son=children.begin();son!=children.end();son++){
        if(m_tree->getNode(*son).getDegree()){
             int SFL = m_tree->getNode(*son).getLastFullLevel();
             if(SFL < LFL){
                LFL=SFL;
            }
        }
    }
    return LFL+1;
}

void HeterogeneousNode::orderChildren(){
    m_children.sort([&](NodeId i, NodeId j){
        if (m_tree->getNode(i).getDegree() > m_tree->getNode(j).getDegree()){
            return true;
        }
        else if (m_tree->getNode(i).getDegree() < m_tree->getNode(j).getDegree()){
            return false;
        }
        else return m_tree->getNode(i).getSubtreeSize() > m_tree->getNode(j).getSubtreeSize();
    });
}

bool HeterogeneousNode::repair() {
   /*
    if(getOwnInd() != 0){
        m_tree->getNode(0).repair();
    }
    std::vector<unsigned> subtree;
    for(unsigned i=0;i<100;i++){
        auto children=m_tree->getNode(i).m_children;
        for(auto s = children.begin(); s!= children.end();s++){
            m_tree->getNode(i).removeFromChildren(*s);
        }
    }

    m_tree->getNode(0).m_degree=3;
    m_tree->getNode(1).m_degree=1;
    m_tree->getNode(2).m_degree=1;
    m_tree->getNode(3).m_degree=1;
    m_tree->getNode(4).m_degree=1;
    m_tree->getNode(5).m_degree=1;
    m_tree->getNode(6).m_degree=1;
    m_tree->getNode(7).m_degree=0;
    m_tree->getNode(8).m_degree=1;

    m_tree->getNode(0).addChild(1);
    m_tree->getNode(0).addChild(2);

    m_tree->getNode(0).addChild(3);
    m_tree->getNode(0).addChild(4);
    m_tree->getNode(0).addChild(5);


    m_tree->getNode(0).addChild(6);
    m_tree->getNode(0).addChild(7);
    m_tree->getNode(0).addChild(8);


*/

  //   std::cout<<"####################\nrepairing----->"<<getOwnInd()<<"\n####################\n";
 //    std::cout<<"############"<<"height--->"<<m_tree->getMaxHeight()-1<<"\n";
   //          std::cout<<"before\n";
      //   for(auto i=0;i<1;i++) std::cout<<m_tree->getNode(i)<<std::endl;

    int size = m_children.size();

    this->orderChildren();   

    //this list contains the node that receive the video with degree > 0
    std::vector<NodeId> node_receive_video, node_dont_receive_video;
    
    //if overloaded...
    if(size > m_degree && m_degree) {

        // vector of children that receive the video and have degree > 0
        for(auto it=m_children.begin();it!=std::next(m_children.begin(), m_degree);it++){
            if(m_tree->getNode(*it).getDegree()){
                 node_receive_video.push_back(*it);
            }
        }

        // vector of children that do not receive the video
        for(auto it=std::next(m_children.begin(), m_degree);it!=m_children.end();it++){
            node_dont_receive_video.push_back(*it);
        }

        // if there is no children with degree > 0 stop

        if(!node_receive_video.size()) return false;
        
        /***************************************
            CASE 1: push-replace
        ****************************************/
       
        auto iter1 = node_dont_receive_video.begin();
        while(iter1 != node_dont_receive_video.end()){

            auto iter2 = node_receive_video.begin();
            bool found = 0;

            while(iter2 != node_receive_video.end() && ! found){

            if((int)m_tree->getNode(*iter2).getDegree() - (int)m_tree->getNode(*iter2).getChildren().size() > (int)m_tree->getNode(*iter1).getChildren().size() && m_tree->getNode(*iter1).getChildren().size() > 0  ){

                found = 1;

                m_tree->getNode(*iter2).adopt(getOwnInd(),  std::find(m_children.begin(),m_children.end(), *iter1));

                while(!m_tree->getNode(*iter1).getChildren().empty()) {

                    m_tree->getNode(*iter2).adopt(*iter1, m_tree->getNode(*iter1).getChildren().begin());
                }
            }
            iter2++;
        }
            if(found){
                iter1 = node_dont_receive_video.erase(iter1);
             }
             else{
                iter1++;
            }
        }
        if ((int) m_children.size() == m_degree) return true;



        /***************************************
           CASE 2: push and pull
        ****************************************/

        iter1 = node_dont_receive_video.begin();
        while(iter1 != node_dont_receive_video.end()){

            auto iter2 = node_receive_video.begin();
            bool found = 0;
            std::random_shuffle ( node_receive_video.begin(), node_receive_video.end() );

            while(iter2 != node_receive_video.end() && ! found){
                //std::cout<<*iter1<<"----"<<*iter2<<std::endl;
                if((int)m_tree->getNode(*iter2).getDegree() - (int)m_tree->getNode(*iter2).getChildren().size() > (int)m_tree->getNode(*iter1).getChildren().size()){

                    found = 1;

                    m_tree->getNode(*iter2).adopt(getOwnInd(),  std::find(m_children.begin(),m_children.end(), *iter1));

                    int available,sons,childrenToPull;

                    available = (int)m_tree->getNode(*iter2).getDegree() - (int)m_tree->getNode(*iter2).getChildren().size();
                    sons=(int)m_tree->getNode(*iter1).getChildren().size();
                    childrenToPull=(available < sons) ? available : sons;
                    if (childrenToPull > 0){

                        m_tree->getNode(*iter1).orderChildren();
                        while(childrenToPull>0) {
                            m_tree->getNode(*iter2).adopt(*iter1, m_tree->getNode(*iter1).getChildren().begin());
                            --childrenToPull;
                        }
                    }   
                }
                iter2++;
            }
            if(found){
                iter1 = node_dont_receive_video.erase(iter1);
            }
             else{
                iter1++;
            }
        }   
        if ((int) m_children.size() == m_degree) return true;

        /***************************************
           CASE 3: node with min last full level
        ****************************************/

#ifdef UPDATE
        //remove in lastFullLevel the node that are no more my children or that do not receive the video
        auto it = lastFullLevel.begin();
        while (it != lastFullLevel.end()) {
            if (std::find(node_receive_video.begin(),node_receive_video.end(), it->first) ==  node_receive_video.end()){
                it = lastFullLevel.erase(it);
            }
            else {
                ++it;
            }   
        }   
#else
        // normal case, I update during each repair
        this->update();
#endif
        // if not empty (possible if all my children are different form the nodes that I had in my last updates)
        if(lastFullLevel.size()){
            auto childrenToAdopt = node_dont_receive_video.begin();
            while(childrenToAdopt != node_dont_receive_video.end()){       
                int nodeMinLFL = -1, minLFL=std::numeric_limits<int>::max();
                for(auto it=node_receive_video.begin(); it != node_receive_video.end(); it++){
                    if (lastFullLevel.find(*it) != lastFullLevel.end()) {
                        //  found
                        if(lastFullLevel[*it].first < minLFL && lastFullLevel[*it].second){
                            minLFL = lastFullLevel[*it].first;
                            nodeMinLFL = *it;
                        }
                    } 
                }
                if(nodeMinLFL > 0){
                    lastFullLevel[nodeMinLFL].second-=1;
                    m_tree->getNode(nodeMinLFL).adopt(getOwnInd(),std::find(m_children.begin(),m_children.end(), *childrenToAdopt));
                    childrenToAdopt = node_dont_receive_video.erase(childrenToAdopt);
                }
                else{
                    // i've finished all the available slots
                    for(auto it=node_receive_video.begin(); it != node_receive_video.end(); it++){
                        lastFullLevel[*it].first+=1;
                        lastFullLevel[*it].second =  std::pow(m_tree->getNode(*it).getDegree(), lastFullLevel[*it].first + 1 ); 
                    }
                }
            }         
        }
        //if empty I push randomly on my children
        else{
            while((int)m_children.size() > (int)m_degree){   
                std::mt19937 rng;
                rng.seed(std::random_device()());
                std::uniform_int_distribution<std::mt19937::result_type> dist6(0,node_receive_video.size()-1);
                int nodeToPush = node_receive_video[dist6(rng)];
                m_tree->getNode(nodeToPush).adopt(getOwnInd(),std::next(m_children.begin(), m_degree));
            }
        }
        
        /*
         
        }
        */
/*
#else
        unsigned nodeMinLFL;
        while((int)m_children.size() > (int)m_degree){
            
            int LFL = std::numeric_limits<int>::max();
            for(auto it=node_receive_video.begin(); it != node_receive_video.end(); it++){
                
                int SFL=m_tree->getNode(*it).getLastFullLevel();
                if(SFL < LFL){
                    LFL=SFL;
                    nodeMinLFL=*it;
                }
            }

            std::vector<unsigned> available_children;
            for(auto it=node_receive_video.begin(); it != node_receive_video.end(); it++){

                if((int)m_tree->getNode(*it).getLastFullLevel() == LFL ){

                        available_children.push_back(*it);

                }
             }
            auto a = available_children.begin();
            std::advance(a, std::rand() % available_children.size());
            nodeMinLFL = *a;
            m_tree->getNode(nodeMinLFL).adopt(getOwnInd(),std::next(m_children.begin(), m_degree));
            
        }
*/



        return true;
    }
    return false;
}



bool HeterogeneousNode::isReachable() const {
    if(getOwnInd() == 0) {
        return true;
    }
    auto& parent = m_tree->getNode(getParent());
    if(parent.isReachable()) {
        parent.orderChildren();
        int i = 0;
        auto ite = parent.m_children.begin();
        while(i < parent.m_degree) {
            if(*ite == getOwnInd()) {
                if(m_new == true) m_new = false;
                return true;
            }
            ++ite;
            ++i;
        }
    }
    return false;
}




int HeterogeneousNode::getBalance() const {
    int balance = 0;
    if(!m_children.empty()) {
        auto iteFirstChild = m_children.begin();
        auto iteSecondChild = ++m_children.begin();
        //If not only child and is a leaf
        if(iteSecondChild != m_children.end() && !m_tree->getNode(*iteFirstChild).m_children.empty()) {
            balance += m_tree->getNode(*iteFirstChild).m_subtreeSize;
            for(; iteSecondChild != m_children.end(); ++iteSecondChild) {
                balance -= m_tree->getNode(*iteSecondChild).m_subtreeSize;
            }
        }
    }
    return balance;
}

void HeterogeneousNode::changeSubtreeSize(int _delta) {
    // std::cout << "("<<getOwnInd()<<", "<<getParent()<<")" << ".changeSubtreeSize(" << _delta <<") : " << m_subtreeSize;
    m_subtreeSize += _delta;
    // std::cout << " => " << m_subtreeSize << "-->" << this << std::endl;
    if(this->hasParent()) {
        // std::cout << getOwnInd() << "^" << getParent() << std::endl;
        m_tree->getNode(this->getParent()).changeSubtreeSize(_delta);
    }
}


int HeterogeneousNode::getDepth() const {
    if(getOwnInd() == 0) {
        return 0;
    }
    auto& parent = m_tree->getNode(getParent());

    return 1+parent.getDepth();
}



