#ifndef CONSTANT_POPULATION_CPP
#define CONSTANT_POPULATION_CPP

#include "constant_population.hpp"

#include <vector>
#include <list>
#include <algorithm>
#include <map>
#include <stack>
#include <fstream>
#include <iostream>

#include "Tree.hpp"
#include "Experiment.hpp"
#include "utils.hpp"
#include "names.hpp"



/** 
Simulation for the interruption metrics
Save step by step the metrics and save them in a file with the name:
"nameOfTheProtocol"_"numberOfNodes"_"churnRate"_interrupt.plt
in the interrupt folder
in a csv format where the columns represent the following metrics:
1st: time t of the simulation
2nd: average interruption time over the nodes at time t
3rd: average number of interruption over the nodes at time t
4th: average interruption time over the non new nodes at time t
5th: average number of interruption over the non new nodes at time t
*/
template<class Node> 
#ifdef UPDATE
void interrupt(Tree<Node>& tree, int _maxTime, double _churnRate, double _updateTime) {
#else
void interrupt(Tree<Node>& tree, int _maxTime, double _churnRate) {
#endif  

	int nbNodes = tree.getNodes().size() - 1;
#ifdef UPDATE
	std::string filename = "interrupt/"+getName<Node>() + "_"+std::to_string(int(_updateTime))+ "_" + std::to_string(nbNodes) + '_' + std::to_string(_churnRate) +"_interrupt.plt";
#else
	std::string filename = "interrupt/"+getName<Node>() + "_" + std::to_string(nbNodes) + '_' + std::to_string(_churnRate) +"_interrupt.plt";
#endif
	std::cout << filename << std::endl;

	#ifdef UPDATE
	Experiment<Node> exp {_churnRate, 1.0,  &tree, _updateTime};
	#else
	Experiment<Node> exp {_churnRate, 1.0,  &tree};
	#endif  
	
	std::vector<bool> wasReachable(nbNodes);
    
	std::vector<int> nbInterruption(nbNodes);
	std::vector<double> timeWithoutVideo(nbNodes);
	std::list<double> interruptionsTime;
    
    
    std::vector<int> nbInterruptionNotNew(nbNodes);
    std::vector<double> timeWithoutVideoNotNew(nbNodes);
    std::list<double> interruptionsTimeNotNew;
	
    std::list<double> times;
    std::list<double> meanTime, meanNumber;
    std::list<double> meanTimeNotNew, meanNumberNotNew;
	
    double lastTime = 0;
	
	for(int i = 0; i < nbNodes; ++i) {
		wasReachable[i] = tree.getNode(i+1).isReachable();
	}
	
	while(exp.getTime() < _maxTime) {
		std::cout << exp.getTime() << std::endl;
		exp.runUntil(1, _maxTime);
		double now = exp.getTime();
        
		for(int j = 0; j < nbNodes; ++j) {
			bool isReachableNow = tree.getNode(j+1).isReachable();
			if(wasReachable[j] && !isReachableNow) {
                if(!tree.getNode(j+1).isNew()) {
                    ++nbInterruptionNotNew[j];
                }
				++nbInterruption[j];
			}
			else if (!wasReachable[j]) {
                if(!tree.getNode(j+1).isNew()) {
                    interruptionsTimeNotNew.push_back(now - lastTime);
                    timeWithoutVideoNotNew[j] += (now - lastTime);
                }
                interruptionsTime.push_back(now - lastTime);
				timeWithoutVideo[j] += (now - lastTime);
			}
			wasReachable[j] = isReachableNow;
		}
		lastTime = now;
		times.push_back(exp.getTime());
		
        meanTime.push_back(mean(timeWithoutVideo.begin(), timeWithoutVideo.end()));
		meanNumber.push_back(mean(nbInterruption.begin(), nbInterruption.end()));
        
        meanTimeNotNew.push_back(mean(timeWithoutVideoNotNew.begin(), timeWithoutVideoNotNew.end()));
        meanNumberNotNew.push_back(mean(nbInterruptionNotNew.begin(), nbInterruptionNotNew.end()));
	}


	toCSV(filename, times, meanTime, meanNumber, meanTimeNotNew, meanNumberNotNew);
	/*
	std::ofstream ofs("interrupt/"+getName<Node>() + "_" + std::to_string(nbNodes) + '_' + std::to_string(_churnRate) +"_interruptTimeSet.plt", std::ofstream::out);
	for(auto& time : interruptionsTime) {
		ofs << time << " ";
	}
	ofs.close();
    
    ofs.open("interrupt/"+getName<Node>() + "_" + std::to_string(nbNodes) + '_' + std::to_string(_churnRate) +"_interruptTimeSetNotNew.plt", std::ofstream::out);
    for(auto& time : interruptionsTimeNotNew) {
        ofs << time << " ";
    }
    ofs.close();
    */
}


/**
Simulation for the interruption metrics distribution
Save the distribution in 4 different files
The distribution of the number of interruption in getName<Node>() + "_nbInterrupt_" + std::to_string(nbNodes) + '_' + std::to_string(_churnRate) +".plt"
The distribution of the number of interruption for non new node in getName<Node>() + "_nbInterruptNotNew_" + std::to_string(nbNodes) + '_' + std::to_string(_churnRate) +".plt"
The distribution of the time of interruption in getName<Node>() + "_interruptTime_" + std::to_string(nbNodes) + '_' + std::to_string(_churnRate) +".plt
The distribution of the time of interruption for non new node in getName<Node>() + "_interruptTimeNotNew_" + std::to_string(nbNodes) + '_' + std::to_string(_churnRate) +".plt"
*/
template<class Node> 
#ifdef UPDATE
void interruptDistribution(Tree<Node>& tree, int _maxTime, double _churnRate, double _updateTime) {
#else
void interruptDistribution(Tree<Node>& tree, int _maxTime, double _churnRate) {
#endif  

	int nbNodes = tree.getNodes().size() - 1;
	std::cout << "interruptDistribution("<<nbNodes<<", "<<_maxTime<<", "<<_churnRate<<")"<<std::endl;

	std::cout << "# nodes:" << tree.getNodes().size() << std::endl;

	#ifdef UPDATE
	Experiment<Node> exp {_churnRate, 1.0,  &tree, _updateTime};
	#else
	Experiment<Node> exp {_churnRate, 1.0,  &tree};
	#endif  


	std::vector<bool> wasReachable(nbNodes);
    
	std::vector<unsigned> nbInterruption(nbNodes);
	std::map<double, int> interruptionsTime;
	std::vector<double> timeInterruption(nbNodes);
	
    std::vector<unsigned> nbInterruptionNotNew(nbNodes);
    std::map<double, int> interruptionsTimeNotNew;
    std::vector<double> timeInterruptionNotNew(nbNodes);   
	
	for(int i = 0; i < nbNodes; ++i) {
		wasReachable[i] = tree.getNode(i+1).isReachable();
		++i;
	}
	
	// double lastTime = 0;
	while(exp.getTime() < _maxTime) {
		exp.runUntil(1, _maxTime);
		
		double now = exp.getTime();
		
		for(int j = 0; j < nbNodes; ++j) {
			bool isReachableNow = tree.getNode(j+1).isReachable();
			
			if(wasReachable[j] && !isReachableNow) {
                if(tree.getNode(j+1).isNew()) {
                    ++nbInterruptionNotNew[j];
                    timeInterruptionNotNew[j] = now;
                }
				++nbInterruption[j];
				timeInterruption[j] = now;
			}
			else if (!wasReachable[j] && isReachableNow) {
                if(tree.getNode(j+1).isNew()) {
                    ++interruptionsTimeNotNew[now - timeInterruptionNotNew[j]];
                }
				++interruptionsTime[now - timeInterruption[j]];
			}
			wasReachable[j] = isReachableNow;
		}
		// lastTime = now;
	}
	
	std::map<int, int> interruptions;
	for(auto& nb : nbInterruption) {
		++interruptions[nb];
	}
	std::ofstream ofs("distribution/"+getName<Node>() + "_nbInterrupt_" + std::to_string(nbNodes) + '_' + std::to_string(_churnRate) +".plt", std::ofstream::out);
	for(auto& pair : interruptions) {
		ofs << pair.first << " " << pair.second << std::endl;
	}
	ofs.close();
    ///
    std::map<int, int> interruptionsNotNew;
    for(auto& nb : nbInterruptionNotNew) {
        ++interruptionsNotNew[nb];
    }
    
    ofs.open("distribution/"+getName<Node>() + "_nbInterruptNotNew_" + std::to_string(nbNodes) + '_' + std::to_string(_churnRate) +".plt", std::ofstream::out);
    for(auto& pair : interruptionsNotNew) {
        ofs << pair.first << " " << pair.second << std::endl;
    }
    ofs.close();
    
    /////////
	
	ofs.open("distribution/"+getName<Node>() + "_interruptTime_" + std::to_string(nbNodes) + '_' + std::to_string(_churnRate) +".plt", std::ofstream::out);
	for(auto& pair : interruptionsTime) {
		ofs << pair.first << " " << pair.second << std::endl;
	}
	ofs.close();
	std::cout << "distribution/"+getName<Node>() + "_interruptTime_" + std::to_string(nbNodes) + '_' + std::to_string(_churnRate) +".plt saved !" << std::endl;
	
    
    ofs.open("distribution/"+getName<Node>() + "_interruptTimeNotNew_" + std::to_string(nbNodes) + '_' + std::to_string(_churnRate) +".plt", std::ofstream::out);
    for(auto& pair : interruptionsTimeNotNew) {
        ofs << pair.first << " " << pair.second << std::endl;
    }
    ofs.close();
    std::cout << "distribution/"+getName<Node>() + "_interruptTimeNotNew_" + std::to_string(nbNodes) + '_' + std::to_string(_churnRate) +".plt saved !" << std::endl;
    
    
	std::cout << "nbChurn : " << exp.m_nbChurn << std::endl;
	std::cout << "nbFix: " << exp.m_nbFix << std::endl;
}


/** 
Simulation for the main metrics
Save step by step the metrics and save them in a file with the name:
"nameOfTheProtocol"_"numberOfNodes"_"churnRate".plt
in the overTime folder
in a csv format where the columns represent the following metrics:
1st: time t of the simulation
2nd: max height at time t
3rd: Number of nodes not receiving the video
4th: Number of son of the root
5th: Balance of the tree (ignore?)
6th: Absolute balance (ignore?)
7th: avg depth of the nodes
8th: avg depth of the nodes that receive the video

*/

template<class Node> 
#ifdef UPDATE
void overTime(Tree<Node>& tree,  int _maxTime, double _churnRate, double _updateTime) {
#else
void overTime(Tree<Node>& tree,  int _maxTime, double _churnRate) {
#endif  


	int nbNodes = tree.getNodes().size() - 1;
#ifdef UPDATE
	std::string filename = "overTime/" + getName<Node>() +"_"+std::to_string(int(_updateTime))+ "_distOverTime_balancedTree_" + std::to_string(nbNodes) + "_" + std::to_string(_churnRate) + ".plt";
#else
	std::string filename = "overTime/" + getName<Node>() + "_distOverTime_balancedTree_" + std::to_string(nbNodes) + "_" + std::to_string(_churnRate) + ".plt";
#endif
    std::cout << filename << std::endl;

	#ifdef UPDATE
	Experiment<Node> exp {_churnRate, 1.0,  &tree, _updateTime};
	#else
	Experiment<Node> exp {_churnRate, 1.0,  &tree};
	#endif  


    std::vector<int> results1, results2, results3, results6;
    std::vector<double> times, results4, results5,results7,results8;
    
    unsigned max_Degree = tree.getMaxDegree();

    // here for each bandwidth in each entry I store the avg depth of the nodes with that bandwidth
    std::vector<std::vector<double>> depth_in_function_bandwidth(max_Degree+1);

    // here for each bandwidth in each entry I store the ratio of the node with that bandwidth that receive the video
    std::vector<std::vector<double>> receivingvideo_in_function_bandwidth(max_Degree+1);



	while(exp.getTime() < _maxTime) {
		//std::cout << exp.getTime() << std::endl;
		exp.runUntil(1, _maxTime);
		// tree.createDotFile("tree_"+std::to_string(exp.getTime()));
        results1.push_back(tree.getMaxHeight());
        
		results2.push_back(tree.getNbUnreachable());
		/*
		results3.push_back(tree.getRoot().getChildren().size());
		results4.push_back(tree.getBalance());
		
		results5.push_back(tree.getAbsBalance());
        results6.push_back(tree.getNbUnreachableNotNew());
        results7.push_back(tree.getAvgDepth());
        results8.push_back(tree.getAvgDepthReachable());
        for(int i=0;i < (int) depth_in_function_bandwidth.size();i++){
        	depth_in_function_bandwidth[i].push_back(tree.getAvgDepthBandwidth(i));
        	receivingvideo_in_function_bandwidth[i].push_back(tree.getRatioReceivingBandwidth(i));
        }	

		*/
        times.push_back(exp.getTime());
    }
    toCSV(filename, times, results1, results2);//, results3, results4, results5, results6,results7,results8);
/*
#ifdef UPDATE
    std::string filename_1="overTime/AvgDepthBandwidth_" + getName<Node>() + "_"+ std::to_string(int(_updateTime)) +"_distOverTime_balancedTree_" + std::to_string(nbNodes) + "_" + std::to_string(_churnRate) + ".plt";
    std::string filename_2="overTime/RatioReceivingBandwidth_" + getName<Node>() +"_"+ std::to_string(int(_updateTime)) + "_distOverTime_balancedTree_" + std::to_string(nbNodes) + "_" + std::to_string(_churnRate) + ".plt";
#else
    std::string filename_1="overTime/AvgDepthBandwidth_" + getName<Node>() + "_distOverTime_balancedTree_" + std::to_string(nbNodes) + "_" + std::to_string(_churnRate) + ".plt";
    std::string filename_2="overTime/RatioReceivingBandwidth_" + getName<Node>() + "_distOverTime_balancedTree_" + std::to_string(nbNodes) + "_" + std::to_string(_churnRate) + ".plt";
#endif
    //print to file
    std::ofstream file1,file2;
    file1.open (filename_1);
    file2.open (filename_2);
    for(int i=0;i<(int) depth_in_function_bandwidth[0].size();i++){
    	file1 << times[i];
    	file2 << times[i];
    	for(int j=0;j<(int) depth_in_function_bandwidth.size();j++){
    		file1 << "\t" << depth_in_function_bandwidth[j][i];
    		file2 << "\t" << receivingvideo_in_function_bandwidth[j][i];
    	}
    	file1 << "\n";
    	file2 << "\n";
    }
  	file1.close();
  	file2.close();
  	*/
}


/**
Simulation for the interruption metrics distribution
Save the distribution in 4 different files
The distribution of the heights in getName<Node>() + "heights" + std::to_string(nbNodes) + '_' + std::to_string(_churnRate) +".plt"
The distribution of the balance in getName<Node>() + "balance" + std::to_string(nbNodes) + '_' + std::to_string(_churnRate) +".plt"
The distribution of the absBalance in getName<Node>() + "absBalance" + std::to_string(nbNodes) + '_' + std::to_string(_churnRate) +".plt
*/
template<typename Node>

#ifdef UPDATE
void distribution(Tree<Node>& tree,  int _maxEvents, double _churnRate, double _updateTime) {
#else
void distribution(Tree<Node>& tree,  int _maxEvents, double _churnRate) {
#endif  

	int nbNodes = tree.getNodes().size() - 1;


	#ifdef UPDATE
	Experiment<Node> exp {_churnRate, 1.0,  &tree, _updateTime};
	#else
	Experiment<Node> exp {_churnRate, 1.0,  &tree};
	#endif  


	std::string filenamePart1 = "distribution/" + getName<Node>() + "_";
	std::string filenamePart2 = "_" + std::to_string(nbNodes) + "_" + std::to_string(_churnRate) + ".plt";
	
    std::map<int, int> heights;
	std::map<int, int> balance;
	std::map<int, int> absBalance;
	
	std::stack<const Node*> stack1;
    std::stack<const Node*> stack2;
	
    
    for(int i = 0; i < _maxEvents; ++i) {
        exp.runUntil(1);
        
        // DFS on the tree
		int height = 1;
		stack1.push(&tree.getRoot());
        while(true) {
			if(!stack1.empty()) {
				const Node* node = stack1.top();
				stack1.pop();
				for(unsigned ind : node->getChildren()) {
					stack2.push(&tree.getNode(ind));
				}
				int d = node->getBalance();
				++balance[d];
				++absBalance[abs(d)];
				++heights[height];
			} else if(!stack2.empty()) {
				++height;
				std::swap(stack1, stack2);
			} else {
				break;
			}
		}
    }
 
	std::ofstream ofs (filenamePart1 + "heights" + filenamePart2, std::ofstream::out);
	for(auto& pair : heights) {
		ofs << pair.first << " " << pair.second << std::endl;
	}
	ofs.close();
	
	ofs.open(filenamePart1 + "balance" + filenamePart2, std::ofstream::out);
	for(auto& pair : balance) {
		ofs << pair.first << " " << pair.second << std::endl;
	}
	ofs.close();
	
	ofs.open(filenamePart1 + "absBalance" + filenamePart2, std::ofstream::out);
	for(auto& pair : absBalance) {
		ofs << pair.first << " " << pair.second << std::endl;
	}
	ofs.close();	
}

#endif