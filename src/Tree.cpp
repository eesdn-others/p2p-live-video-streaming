#ifndef TREE_CPP
#define TREE_CPP

#include "Tree.hpp"

#include <cmath>
#include <stack>
#include <fstream>
#include <iostream>

#include <cstdlib>
#include <string>

/**
 Create a tree of _nbNodes with max degree _degree
 The tree sets the id of the node for redirection purposes
 Note: Tree<Node>::m_degree may be redundant
 */
template<class Node>
Tree<Node>::Tree(int _degree, int _nbNodes) :
    m_nodes(_nbNodes+1, Node(_degree, this)), 
    m_degree(_degree),
    m_nbNodes(_nbNodes+1),
    m_hash(0), 
    m_isHashValid(false)
{
    for(int i = 0; i < (int) m_nodes.size(); ++i) {
        m_nodes[i].setOwnInd(i);
    }
}

/**
 Copy from move of array of Node
 */
template<class Node>
Tree<Node>::Tree(int _degree, std::vector<Node>&& _nodes) :
    m_nodes(std::move(_nodes)), 
    m_degree(_degree), 
    m_nbNodes(m_nodes.size()),
    m_hash(0),
    m_isHashValid(false)
{
    for(int i = 0; i < (int) m_nodes.size(); ++i) {
        m_nodes[i].setOwnInd(i);
    }
}

/**
 Copy from array of Node
 */
template<class Node>
Tree<Node>::Tree(int _degree, const std::vector<Node>& _nodes) :
m_nodes(std::move(_nodes)), 
    m_degree(_degree),
    m_nbNodes(m_nodes.size()),
    m_hash(0), 
    m_isHashValid(false)
{
    for(int i = 0; i < (int) m_nodes.size(); ++i) {
        m_nodes[i].setOwnInd(i);
    }
}

/**
 Copy constructor
 */
template<class Node>
Tree<Node>::Tree(const Tree& _tree) :
    m_nodes(_tree.m_nodes), 
    m_degree(_tree.m_degree),
    m_nbNodes(m_nodes.size()),
    m_hash(_tree.m_hash), 
    m_isHashValid(_tree.m_isHashValid)
{
    for(auto& node : m_nodes) {
        node.setTree(this);
    }
}

/**
 Assignement operator
*/
template<class Node>
Tree<Node>& Tree<Node>::operator=(const Tree& _tree) {
    if(&_tree != this) {
        m_nodes =_tree.m_nodes;
        m_degree = _tree.m_degree;
        m_nbNodes = m_nodes.size(),
        m_hash = _tree.m_hash; 
        m_isHashValid = _tree.m_isHashValid;
        
        for(auto& node : m_nodes) {
            node.setTree(this);
        }
    }
    return *this;
}

/**
 Create a balanced tree of _nbNode nodes
 */
template<class Node>
Tree<Node> Tree<Node>::balancedTree(int _degree, int _nbNodes) {
    Tree<Node> tree {_degree, _nbNodes};
    
    int root_ite = 0, node_ite = 1, i = 0;
    while(node_ite < (int) tree.getNodes().size()) {
        tree.m_nodes[root_ite].addChild(node_ite);
        ++node_ite;
        ++i;
        if(i % _degree == 0) {
            ++root_ite;
        }
    }
    return tree;
}

/**
Create a balanced tree of _nbNode nodes
*/
template<class Node>
Tree<Node> Tree<Node>::balancedTree(std::vector<int> _degrees, int _nbNodes) {
    Tree<Node> tree { 20, _nbNodes };
    
    for(auto i = 1; i < (int) tree.getNodes().size(); ++i) {
        tree.getNode(i).setDegree(_degrees[i-1]);
    }
    
    int root_ite = 0, node_ite = 1, i = 0;
    while(node_ite < (int) tree.getNodes().size()) {
        tree.m_nodes[root_ite].addChild(node_ite);
        ++node_ite;
        ++i;
        if( i % tree.getNode(root_ite).getDegree() == 0) {
            ++root_ite;
        }
    }
    return tree;
}

/**
 Create a balanced tree of _nbNode nodes
 */
template<class Node>
Tree<Node> star(std::vector<int> _degrees, int _nbNodes) {
    Tree<Node> tree(20, _nbNodes);
    
    tree.getRoot().setDegree(_degrees[0]);
    for(auto i = 1; i < (int) tree.getNodes().size(); ++i) {
        tree.getRoot().addChild(i);
        tree.getNode(i).setDegree(_degrees[i-1]);
    }
    return tree;
}

/**
 Create a star with _nbNodes nodes
 */

template<class Node>
Tree<Node> Tree<Node>::star(int _degree, int _nbNodes) {
    Tree<Node> tree(_degree, _nbNodes);
    
    for(auto i = 1; i < (int) tree.getNodes().size(); ++i) {
        tree.getRoot().addChild(i);
    }
    return tree;
}

/**
 Create a path of length _nbNodes
 */
template<class Node>
Tree<Node> Tree<Node>::path(int _degree, int _nbNodes) {
    Tree<Node> tree { _degree, _nbNodes };
    for(auto i = 1; i < (int) tree.getNodes().size()-1; ++i) {
        tree.m_nodes[i].addChild(i+1);
    }
    return tree;
}

/**
 Save the tree as a dot file for graphviz
 */
template<class Node>
void Tree<Node>::createDotFile(const std::string& _name, const std::string& _type) const {
    
    std::string dotFilename = _name + ".dot";
    std::ofstream ofs (dotFilename, std::ofstream::out);
    ofs << "digraph G {" << std::endl;
    
    for(const Node& node : m_nodes) {
        std::string fill;
        if(node.isReachable()) {
            fill = ", color=green, style=filled"; 
        }
        ofs << node.getOwnInd() << " [label=\"" << node.getDotLabel() << "\\n" << node.getParent() << "\"" << fill << "];" << std::endl;
        for(int ind : node.getChildren()) {
            ofs << node.getOwnInd() << "->" << ind << ";" << std::endl;
        }
    }
    ofs << "}";
    ofs.close();
    std::string sysString = "dot " + dotFilename + " -T"+_type+"  -o " + _name + "."+_type;
    system(sysString.c_str());
    
}

/**
 Get a random node from level 0 to level
 */
template<class Node>
Node& Tree<Node>::getRandomNode(int _level) {
    //use just the root (root is at level 1)
    if(_level<=1)
        return m_nodes[0];
    
    std::vector<int> vector;
    //push the root in the vector
    vector.push_back(m_nodes[0].getOwnInd());
    //push each node (that is inside the level) in the vector
    getListNodes(_level,1,m_nodes[0], &vector);
    int l;
    //use do while because otherwise the last number is generated few time
    do{
        l = rand() % (vector.size());
    }while(l>=vector.size()); 
    
    //std::cout <<"size: "<< vector.size() << " random: " << l << std::endl;
    return m_nodes[vector[l]];
    /*for(std::vector<int int>::const_iterator i = vector.begin(); i != vector.end(); ++i)
        std::cout << *i << ' ';*/
    
}

template<class Node>
void Tree<Node>::getListNodes(int _level, int iterator, Node node, std::vector<int>* vector) {
    if(_level == iterator)
        return;
    else{
        std::list<int> myList=node.getChildren();
        auto ite=myList.begin();
        int i=0;
        //get at maximum the first two children
        while(ite!=myList.end() && i < 2){
            vector->push_back(m_nodes[*ite].getOwnInd());
            getListNodes(_level,iterator+1,m_nodes[*ite],vector);
            ++ite;
            ++i;
        }
        
    }
}


/**
 Kill a node at random
 */
template<class Node>
Node* Tree<Node>::killNode() {
    int i = rand() % (m_nodes.size()-1);
    m_nodes[i+1].die();
    getRoot().addChild(i+1);
    m_isHashValid = false;
    return &m_nodes[i+1];
}


/**
 Kill the node whose id is _index
 */
template<class Node>
void Tree<Node>::killANode(int _index) {
    m_nodes[_index].die();
    getRoot().addChild(_index);
    m_isHashValid = false;
}

template<class Node>
Node* Tree<Node>::repairNode() {
    int i = rand() % m_nodes.size()+1;
    m_nodes[i-1].repair();
    m_isHashValid = false;
    return &m_nodes[i];
}

/**
 Repair the node whose id is _index
 */

template<class Node>
void Tree<Node>::repairANode(int _index) {
    m_nodes[_index].repair();
    m_isHashValid = false;
}

/**
 Repair the root
 */
template<class Node>
void Tree<Node>::repairRoot() {
    getRoot().repair();
    m_isHashValid = false;
}

/**
 Repair all the nodes, root included
 */
template<class Node>
void Tree<Node>::repairAll() {
    for(Node& node : m_nodes)
        node.repair();
    m_isHashValid = false;
}

/**
 Return the max height of the tree
 */
template<class Node>
int Tree<Node>::getMaxHeight() const {
    std::stack<const Node*> stack1;
    std::stack<const Node*> stack2;
    stack1.push(&getRoot());
    
    int height = 1; //<----
    while(true) {
        if(!stack1.empty()) {
            const Node* root = stack1.top();
            stack1.pop();
            for(int ind : root->getChildren()) {
                stack2.push(&m_nodes[ind]);
            }
        } else if(!stack2.empty()) {
            ++height;
            std::swap(stack1, stack2);
        } else {
            return height;
        }
    }
}

/**
 Return the balance of the tree
 */
template<class Node>
double Tree<Node>::getBalance() const {
    std::stack<const Node*> stack1;
    std::stack<const Node*> stack2;
    stack1.push(&getRoot());
    
    double balance = 0;
    int height = 1;
    while(true) {
        if(!stack1.empty()) {
            const Node* root = stack1.top();
            stack1.pop();
            for(int ind : root->getChildren()) {
                stack2.push(&m_nodes[ind]);
            }
            balance += root->getBalance() / double(height);
        } else if(!stack2.empty()) {
            ++height;
            std::swap(stack1, stack2);
        } else {
            return balance;
        }
    }
}

/**
 Return the absolute balance of the tree
 */
template<class Node>
double Tree<Node>::getAbsBalance() const {
    std::stack<const Node*> stack1;
    std::stack<const Node*> stack2;
    stack1.push(&getRoot());
    
    double balance = 0;
    int height = 1;
    while(true) {
        if(!stack1.empty()) {
            const Node* root = stack1.top();
            stack1.pop();
            for(int ind : root->getChildren()) {
                stack2.push(&m_nodes[ind]);
            }
            balance += std::fabs(root->getBalance() / double(height));//here
        } else if(!stack2.empty()) {
            ++height;
            std::swap(stack1, stack2);
        } else {
            return balance;
        }
    }
}


/**
 Return the number of nodes receiving the video
 */
template<class Node>
int Tree<Node>::getNbReachable() const {
    int nbReachable = 0;
    std::list<unsigned> stack{0};
    while(!stack.empty()){
        int nodeId = stack.back();
        auto node = m_nodes[nodeId];
        stack.pop_back();
        int i = 0;
        for(auto idSon : node.getChildren()){
            if(i >= node.getDegree()){
                break;
            }
        stack.push_back(idSon);
        ++i;
        }
        ++nbReachable;
    }
    return nbReachable;
}

/**
 Return the number of nodes not receiving the video.
 Uses Tree<Node>::getNbReachable()
 */
template<class Node>
int Tree<Node>::getNbUnreachable() const {
    return m_nodes.size() - getNbReachable();
}

/**
 Return the number of nodes not receiving the video and not new node
 */
template<class Node>
int Tree<Node>::getNbUnreachableNotNew() const {
    int nbUnreachable = 0;
    for(const Node& node : m_nodes) {
        if(!node.isReachable() && !node.isNew()) {
            ++nbUnreachable;
        }
    }
    return nbUnreachable;
}

/**
 Return the list of repaired tree from *this
 Only the overloaded nodes are repaired and generate a new tree.
 */
template<class Node>
std::list<Tree<Node>> Tree<Node>::getRepairTransitionTrees() const {
    std::list<Tree> trees;
    for(size_t i = 0; i < m_nodes.size(); ++i) {
        if(this->m_nodes[i].isOverloaded()) {
            trees.push_back(*this);
            // std::cout<<"trees.back().repairANode("<<i<<")\n";
            trees.back().repairANode(i);
        }
    }
    return trees;
}

/**
 Return the list of repaired tree from *this
 A churn on a leaf at the root does not count
 */
template<class Node>
std::list<Tree<Node>> Tree<Node>::getChurnTransitionTrees() const {
    std::list<Tree> trees;
    
    for(int i = 1; i < (int) m_nodes.size(); ++i) {
        if(!(this->m_nodes[i].getParent() == 0 && this->m_nodes[i].getChildren().empty())) {
            // Not a leaf at the root
            trees.push_back(*this);
            trees.back().killANode(i);
        }
    }
    return trees;
}


/**
 Return the hash of the tree.
 Used for the AllTrees simulation
 */
template<class Node>
uint64_t Tree<Node>::getHash() const {
    if(!m_isHashValid) {
        m_hash = getRoot().getHash();
        m_isHashValid = true;
    }
    return m_hash;
}

/**
 Return the avg depth of the nodes
 */
template<class Node>
double Tree<Node>::getAvgDepth() const {
    int n_nodes = 0;
    int sum_depth=0;
    for(const Node& node : m_nodes) {
        if(node.getOwnInd()){
             sum_depth+=node.getDepth();
             n_nodes++;
        }
    }
    return sum_depth/double(n_nodes);
}

/**
 Return the avg depth of the nodes receiving the video
 */
template<class Node>
double Tree<Node>::getAvgDepthReachable() const {
    int n_nodes = 0;
    int sum_depth=0;
    for(const Node& node : m_nodes) {
        if(node.isReachable() && node.getOwnInd()) {
            sum_depth+=node.getDepth();
            n_nodes++;
        }
    }
    if(n_nodes){
        return sum_depth / double(n_nodes);
    }
    return 0;
}

/**
 Return the max degree
 */
template<class Node>
int Tree<Node>::getMaxDegree() const {
    int maxDegree = 0;
    for(const Node& node : m_nodes) {
        if(node.getDegree() > maxDegree && node.getOwnInd()) {
            maxDegree=node.getDegree();
        }
    }
    return maxDegree;
}

/**
 Return the avg depth of the nodes with the bandwidth passed as argument
 */
template<class Node>
double Tree<Node>::getAvgDepthBandwidth(int deg) const {
    int count=0;
    int sum_depth=0;
    for(const Node& node : m_nodes) {
        if(node.getDegree() == deg && node.getOwnInd()) {
            count++;
            sum_depth+=node.getDepth();
        }
    }
    if (!count) return -1;
    return sum_depth/double(count);
}

/**
 Return the ratio of nodes receiving the video with the bandwidth passed as argument
 */
template<class Node>
double Tree<Node>::getRatioReceivingBandwidth(int deg) const {
    int total=0;
    int receiving=0;
    for(const Node& node : m_nodes) {
        if(node.getDegree() == deg && node.getOwnInd()) {
            total++;
            if(node.isReachable()){
                receiving++;
            }
        }
    }
    if (!total) return -1;
    return receiving/double(total);
}

#ifdef UPDATE
template<class Node>
void Tree<Node>::update(){
    for(Node& node : m_nodes) {
        node.update();
    }
}
#endif
        






#endif