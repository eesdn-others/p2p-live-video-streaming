#include "LocalInfoNode1.hpp"

#include <algorithm>
#include <iostream>
#include <cassert>
#include <limits>
#include <cstdlib>

LocalInfoNode1::LocalInfoNode1(int _degree, Tree<LocalInfoNode1>* _tree, int _parent) :
Node(_degree, _parent, 0),
m_tree(_tree)
{
}


#ifdef UPDATE
void LocalInfoNode1::update(){
    return;
}
#endif


void LocalInfoNode1::updateReceivers() {
    int i = 0;
    for(auto it : m_children){
        if(i++ < m_degree){
            m_tree->getNode(it).setReachable(true);
        }
        else{
            m_tree->getNode(it).setReachable(false);
        }
    }
}


void LocalInfoNode1::addChild(unsigned _id) {
    //std::cout << "m_tree : " << m_tree << std::endl;
    //std::cout << getOwnInd() << ".addChild("<<_id<<')'<<std::endl;
    
    this->push_done[_id]=0;

    // get the node reference
    LocalInfoNode1& node = m_tree->getNode(_id);

    // add the id of the node in the list of sons
    m_children.insert(find_first_smaller(node.getDegree(),node.getSubtreeSize()), _id);

    // I set me as node parent
    node.setParent(getOwnInd());
    // change the subtree size
    this->changeSubtreeSize(node.m_subtreeSize);

    this->updateReceivers();
}

std::list<NodeId>::iterator LocalInfoNode1::find_first_smaller(int degree, int subtreeSize) {
    auto ite = this->m_children.begin();
    while(ite != this->m_children.end()){
        if(m_tree->getNode(*ite).getDegree() < degree || (m_tree->getNode(*ite).getDegree() == degree && m_tree->getNode(*ite).getSubtreeSize() < subtreeSize)){
            break;
        }
        ++ite;
    } 
        
    
    return ite;
}

void LocalInfoNode1::die() {

    this->push_done.clear();

    auto& parent = m_tree->getNode(getParent());

    if(m_children.size() == 0){
        parent.removeFromChildren(getOwnInd());
        return;
    }

    while (!m_children.empty()){
        parent.adopt(getOwnInd(), m_children.begin());
    }
    parent.removeFromChildren(getOwnInd());
    parent.updateReceivers();

    return;
}



void LocalInfoNode1::adopt(NodeId _oldFatherId, std::list<NodeId>::iterator _iteChildInd) {
    auto& oldFather = m_tree->getNode(_oldFatherId);
    auto& child = m_tree->getNode(*_iteChildInd);
    
    child.setParent(getOwnInd());
    this->push_done[*_iteChildInd]=0;

    this->m_children.splice(find_first_smaller(child.getDegree(),child.getSubtreeSize()), oldFather.m_children, _iteChildInd);
    oldFather.changeSubtreeSize(-child.getSubtreeSize());
    this->changeSubtreeSize(child.getSubtreeSize());

    oldFather.updateReceivers();
    this->updateReceivers();
}
 
void LocalInfoNode1::replace(NodeId _old, NodeId _new) {
    // std::cout << _new << " replaces " << _old << " at " << getOwnInd() << std::endl;
    LocalInfoNode1& oldNode = m_tree->getNode(_old);
    LocalInfoNode1& newNode = m_tree->getNode(_new);
    
    oldNode.removeFromChildren(_new);
    
    auto ite = std::find(m_children.begin(), m_children.end(), _old);
    
    newNode.setParent(getOwnInd());
    
    this->push_done[_new]=0;
    
    oldNode.setParent(-1);
    *ite = _new;
    int newSS = newNode.m_subtreeSize;
    int oldSS = oldNode.m_subtreeSize;
    
    this->changeSubtreeSize(newSS-oldSS);
}

void LocalInfoNode1::removeFromChildren(NodeId _child) {
    //std::cout<<"called for node   "<<getOwnInd()<<"\n";
    this->push_done.erase(_child);
    LocalInfoNode1& child = m_tree->getNode(_child);
    m_children.remove(_child);
    this->changeSubtreeSize(-child.m_subtreeSize);
    child.setParent(-1);

}




bool LocalInfoNode1::repair() {

    int size = m_children.size();


        //if all the children have degree 0

    if( getOwnInd() && size > m_degree && !m_tree->getNode(m_children.front()).getDegree()){

        auto it = std::next(m_children.begin(), m_degree);

        while(it != m_children.end()){
                        
            //node is killed and rejoin the system

            m_tree->getNode(0).adopt(getOwnInd(), it);
                        
            it = std::next(m_children.begin(), m_degree);

        }
    }
    
    unsigned sum_push,sum_bandwidth;

    //this list contains the node that receive the video with degree > 0
    
    //if overloaded...
    if( size > m_degree && m_degree && m_tree->getNode(m_children.front()).getDegree()) {

       
        while((int)m_children.size() > (int)m_degree){
            //std::cout<<i++<< " repair of "<<getOwnInd()<<std::endl;
            //std::cout<< m_children.size() << m_degree<<std::endl;
            sum_push = sum_bandwidth = 0;
            
            //sum of the bandwidth of the nodes that receive the video
            //sum of the push done on the nodes that receive the vide
            for(auto iter = m_children.begin(); iter!=std::next(m_children.begin(),m_degree); iter++){   

                sum_bandwidth +=  m_tree->getNode(*iter).getDegree();
                
                sum_push += push_done[m_tree->getNode(*iter).getOwnInd()];
            }

            std::map<unsigned,float> diff;
            
            //difference push - expected push
            for(auto it = m_children.begin(); it!=std::next(m_children.begin(),m_degree); it++){   
                if(m_tree->getNode(*it).getDegree()){
                    diff[*it]=push_done[*it] - sum_push*(m_tree->getNode(*it).getDegree()/double(sum_bandwidth));
                }
            }       
                
            float min = std::numeric_limits<float>::max();

            std::vector<unsigned> id_min;
            
            // I find the minimum value
            for(auto it=diff.begin();it!=diff.end();it++){
                if(it->second < min){
                    min=it->second;
                }
            }
            // i put in the vector all the ids of the nodes the min value
            for(auto it=diff.begin();it!=diff.end();it++){
                if(it->second == min ){
                    id_min.push_back(it->first);
                }
            }
            
            
            // i take randomly a node with the min value (in order to avoid to push always on the same node)
            unsigned idNode = id_min[rand() % id_min.size()];
            
            m_tree->getNode(idNode).adopt(getOwnInd(),std::prev(m_children.end()));
            this->push_done[idNode]+=1;
          
        }

        return true;
    }
    return false;
}




bool LocalInfoNode1::isReachable() const {

    return reachable;
}




int LocalInfoNode1::getBalance() const {
    int balance = 0;
    if(!m_children.empty()) {
        auto iteFirstChild = m_children.begin();
        auto iteSecondChild = ++m_children.begin();
        //If not only child and is a leaf
        if(iteSecondChild != m_children.end() && !m_tree->getNode(*iteFirstChild).m_children.empty()) {
            balance += m_tree->getNode(*iteFirstChild).m_subtreeSize;
            for(; iteSecondChild != m_children.end(); ++iteSecondChild) {
                balance -= m_tree->getNode(*iteSecondChild).m_subtreeSize;
            }
        }
    }
    return balance;
}

void LocalInfoNode1::changeSubtreeSize(int _delta) {
    // std::cout << "("<<getOwnInd()<<", "<<getParent()<<")" << ".changeSubtreeSize(" << _delta <<") : " << m_subtreeSize;
    m_subtreeSize += _delta;
    // std::cout << " => " << m_subtreeSize << "-->" << this << std::endl;
    if(this->hasParent()) {
        // std::cout << getOwnInd() << "^" << getParent() << std::endl;
        m_tree->getNode(this->getParent()).changeSubtreeSize(_delta);
    }
}


int LocalInfoNode1::getDepth() const {
    if(getOwnInd() == 0) {
        return 0;
    }
    auto& parent = m_tree->getNode(getParent());
    
    return 1+parent.getDepth();
}
