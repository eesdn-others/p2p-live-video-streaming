#include "PartialInfoNode.hpp"

#include <algorithm>

#include "MyRandom.hpp"

PartialInfoNode::PartialInfoNode(int _degree, Tree<PartialInfoNode>* _tree, PartialInfoNode::Type _type, int _parent) : 
	Node(_degree, _parent, 0),
	m_type(_type),
	m_tree(_tree)
{}

#ifdef UPDATE
void PartialInfoNode::update(){
    ;
}
#endif


void PartialInfoNode::addChild(unsigned _id) {
    auto& node = m_tree->getNode(_id);
	getChildren().push_back(_id);
    node.setParent(getOwnInd());
	changeSubtreeSize(node.getSubtreeSize());
}

void PartialInfoNode::removeFromChildren(NodeId _id) {
    m_tree->getNode(_id).setParent(-1);
	this->getChildren().remove(_id);
	changeSubtreeSize(-m_tree->getNode(_id).getSubtreeSize());
}

void PartialInfoNode::die() {
	auto& parent = m_tree->getNode(getParent());

	auto newFatherIdIte = getChildren().begin();

	//If some children
	if(newFatherIdIte != getChildren().end()) {
		auto& newFather = m_tree->getNode(*newFatherIdIte);
		if(getType() == BIG_SUBTREE) {
			newFather.setType(BIG_SUBTREE);
		}
		parent.replace(getOwnInd(), *newFatherIdIte);
		getChildren().pop_front();
		while(!getChildren().empty()) {
			auto siblingIte = getChildren().begin(); 
			newFather.adopt(getOwnInd(), siblingIte);
		}
	}
	else {
		parent.removeFromChildren(getOwnInd());
	}
	
	setParent(-1);
	setType(NEW_NODE);
}

void PartialInfoNode::replace(unsigned _old, unsigned _new) {
	m_tree->getNode(_old).setParent(-1);
	auto& newNode = m_tree->getNode(_new);
	auto ite = std::find(getChildren().begin(), getChildren().end(), _old);
	newNode.setParent(getOwnInd());
	*ite = _new;
	m_tree->getNode(_old).changeSubtreeSize(newNode.getSubtreeSize());
	changeSubtreeSize(newNode.getSubtreeSize()-m_tree->getNode(_old).getSubtreeSize());
}

bool PartialInfoNode::repair(){
	if((int) m_children.size() > m_degree) {
		auto extraChildIte = std::next(getChildren().begin(), m_degree);
		auto& extraChild = m_tree->getNode(*extraChildIte);
		if(extraChild.getType() == NEW_NODE) {
			// If its a {\em new} node, it is pushed under a {\em receiving node} chosen uniformly at random.
			auto newFatherIte = std::next(getChildren().begin(), MyRandom::getInstance().getIntUniform(0, m_degree-1));
			auto& newFather = m_tree->getNode(*newFatherIte);
			
			newFather.adopt(getOwnInd(), extraChildIte);
		}
		else if (extraChild.getType() == BIG_SUBTREE) {
			//If it's a {\em big subtree} node, a {\em receiving} node is selected uniformly at random and is pushed under the other {\em receiving} node.
			auto vec = MyRandom::getInstance().getShuffled(0, m_degree-1);
			auto newFatherIte = std::next(getChildren().begin(), vec[0]);
			auto newSonIte = std::next(getChildren().begin(), vec[1]);
			auto& newFather = m_tree->getNode(*newFatherIte);
			// auto& newSon = m_tree->getNode(*newSonIte);
			newFather.adopt(getOwnInd(), newSonIte);
			extraChild.setType(NORMAL);
		}
		return true;
	}
	return false;
}

bool PartialInfoNode::isReachable() const {
    if(getOwnInd() == 0) {
        return true;
    }
	auto& parent = m_tree->getNode(getParent());
    if(parent.isReachable()) {
        int i = 0;
        auto ite = parent.getChildren().begin();
        while(i < parent.m_degree) {
            if(*ite == getOwnInd()) {
                if(m_new == true) m_new = false;
                return true;
            }
            ++ite;
            ++i;
        }
    }
    return false;
}

void PartialInfoNode::adopt(NodeId _oldFatherId, std::list<NodeId>::iterator _iteChildInd) {
	auto& oldFather = m_tree->getNode(_oldFatherId);
	auto& child = m_tree->getNode(*_iteChildInd);
	if((int) m_children.size() < m_degree) {
		child.setType(NORMAL);
	}
	else if (child.getType() != NEW_NODE) {
		child.setType(BIG_SUBTREE);
	}
	child.setParent(getOwnInd());
	
	this->getChildren().splice(this->getChildren().end(), oldFather.getChildren(), _iteChildInd);
	
	changeSubtreeSize(child.getSubtreeSize());
	oldFather.changeSubtreeSize(-child.getSubtreeSize());
}

int PartialInfoNode::getSubtreeSize() const {
	int subtreeSize = 1;
	for(unsigned sonID : this->getChildren()) {
		PartialInfoNode& son = m_tree->getNode(sonID);
		subtreeSize += son.getSubtreeSize();
	}
	return subtreeSize;
}

std::string PartialInfoNode::getDotLabel() const {
	std::string strType;
	if(m_type == NORMAL) {
		strType = "NORMAL";
	}
	else if (m_type == NEW_NODE) {
		strType = "NEW_NODE";
	} 
	else if (m_type == BIG_SUBTREE) {
		strType = "BIG_SUBTREE";
	}
	return std::to_string(this->getOwnInd()) + "=>" + strType;
}

int PartialInfoNode::getBalance() const {
	int balance = 0;
	if(!m_children.empty()) {
		auto iteFirstChild = m_children.begin();
		auto iteSecondChild = ++m_children.begin();
		//If not only child and is a leaf
		if(iteSecondChild != m_children.end() && !m_tree->getNode(*iteFirstChild).getChildren().empty()) {
			balance += m_tree->getNode(*iteFirstChild).getSubtreeSize();
			for(; iteSecondChild != m_children.end(); ++iteSecondChild) {
				balance -= m_tree->getNode(*iteSecondChild).getSubtreeSize();
			}
		}
	}
	return balance;
}



void PartialInfoNode::changeSubtreeSize(int _delta) {
	m_subtreeSize += _delta;
	if(this->hasParent()) {
		m_tree->getNode(this->getParent()).changeSubtreeSize(_delta);
	}
}


int PartialInfoNode::getDepth() const {
    if(getOwnInd() == 0) {
        return 0;
    }
    auto& parent = m_tree->getNode(getParent());

    return 1+parent.getDepth();
}