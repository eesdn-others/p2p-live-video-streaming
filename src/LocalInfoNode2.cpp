#include "LocalInfoNode2.hpp"

#include <algorithm>
#include <iostream>
#include <cassert>
#include <limits>
#include <stdlib.h>

LocalInfoNode2::LocalInfoNode2(int _degree, Tree<LocalInfoNode2>* _tree, int _parent) :
Node(_degree, _parent, 0),
m_tree(_tree)
{
}


//update the distributions

void LocalInfoNode2::update(){
    this->degreesSubtree.clear();
    getDegreeSubtree(this->degreesSubtree);  
}


void LocalInfoNode2::updateReceivers() {
    int i = 0;
    for(auto it : m_children){
        if(i++ < m_degree){
            m_tree->getNode(it).setReachable(true);
        }
        else{
            m_tree->getNode(it).setReachable(false);
        }
    }
}

void LocalInfoNode2::addChild(unsigned _id) {
    //std::cout << "m_tree : " << m_tree << std::endl;
    //std::cout << getOwnInd() << ".addChild("<<_id<<')'<<std::endl;
    
    // get the node reference
    LocalInfoNode2& node = m_tree->getNode(_id);

    // add the id of the node in the list of sons
    m_children.insert(find_first_smaller(node.getDegree(),node.getSubtreeSize()), _id);

    // I set me as node parent
    node.setParent(getOwnInd());
    // change the subtree size
    this->changeSubtreeSize(node.m_subtreeSize);

    this->updateReceivers();
}

void LocalInfoNode2::die() {

    auto& parent = m_tree->getNode(getParent());

    if(m_children.size() == 0){
        parent.removeFromChildren(getOwnInd());
        return;
    }

    while (!m_children.empty()){
        parent.adopt(getOwnInd(), m_children.begin());
    }
    parent.removeFromChildren(getOwnInd());

    parent.updateReceivers();

    return;
}

std::list<NodeId>::iterator LocalInfoNode2::find_first_smaller(int degree, int subtreeSize) {
    auto ite = this->m_children.begin();
    while(ite != this->m_children.end()){
        if(m_tree->getNode(*ite).getDegree() < degree || (m_tree->getNode(*ite).getDegree() == degree && m_tree->getNode(*ite).getSubtreeSize() < subtreeSize)){
            break;
        }
        ++ite;
    } 
    return ite;
}


void LocalInfoNode2::adopt(NodeId _oldFatherId, std::list<NodeId>::iterator _iteChildInd) {
    auto& oldFather = m_tree->getNode(_oldFatherId);
    auto& child = m_tree->getNode(*_iteChildInd);
    
    child.setParent(getOwnInd());
    
    this->m_children.splice(find_first_smaller(child.getDegree(),child.getSubtreeSize()), oldFather.m_children, _iteChildInd);
    oldFather.changeSubtreeSize(-child.getSubtreeSize());
    this->changeSubtreeSize(child.getSubtreeSize());

    oldFather.updateReceivers();
    this->updateReceivers();
}


void LocalInfoNode2::replace(NodeId _old, NodeId _new) {
    // std::cout << _new << " replaces " << _old << " at " << getOwnInd() << std::endl;
    LocalInfoNode2& oldNode = m_tree->getNode(_old);
    LocalInfoNode2& newNode = m_tree->getNode(_new);
    
    oldNode.removeFromChildren(_new);

    auto ite = std::find(m_children.begin(), m_children.end(), _old);
    
    newNode.setParent(getOwnInd());

    oldNode.setParent(-1);
    *ite = _new;
    int newSS = newNode.m_subtreeSize;
    int oldSS = oldNode.m_subtreeSize;
    
    this->changeSubtreeSize(newSS-oldSS);
}

void LocalInfoNode2::removeFromChildren(NodeId _child) {
    // std::cout << _child << " leaves " << getOwnInd() << std::endl;
    LocalInfoNode2& child = m_tree->getNode(_child);
    m_children.remove(_child);
    this->changeSubtreeSize(-child.m_subtreeSize);
    child.setParent(-1);
}

/*
put in the vector all the nodes in the subtree rooted in it
*/
void LocalInfoNode2::getSubtree(std::vector<unsigned> &subtree){
    
    subtree.push_back(getOwnInd());

    for(auto it=m_children.begin();it!=m_children.end();it++){

        m_tree->getNode(*it).getSubtree(subtree);
    }
}

/*
put in the vector the degrees of all the nodes in the subtree rooted in it
*/
void LocalInfoNode2::getDegreeSubtree(std::vector<unsigned> &degreesSubtree){
    std::vector<unsigned> subtree;
    getSubtree(subtree);
    for(auto it=subtree.begin();it!=subtree.end();it++){
        degreesSubtree.push_back(m_tree->getNode(*it).getDegree());
    }
}

std::pair<unsigned, unsigned> LocalInfoNode2::estimatedHeight(){
    //ordering of the degrees in non increasing way
    std::sort(degreesSubtree.begin(),degreesSubtree.end(),std::greater<int>());

    //estimation of the height
    unsigned height=0,start=0,end=1,temp;
    while(end < degreesSubtree.size() && degreesSubtree[start]){

        height++;
        temp=end;
        for(unsigned i=start;i<temp;i++){
            end+=degreesSubtree[i];
        }
        start=temp;
    }
    return std::make_pair(height, degreesSubtree.size());
}

void LocalInfoNode2::addDegree(unsigned deg){
    this->degreesSubtree.push_back(deg);
 }


bool LocalInfoNode2::repair() {

    int size = m_children.size();
    
    //if all the children have degree 0

    if( getOwnInd() && size > m_degree && !m_tree->getNode(m_children.front()).getDegree()){

        auto it = std::next(m_children.begin(), m_degree);

        while(it != m_children.end()){
                        
            //node is killed and rejoin the system

            m_tree->getNode(0).adopt(getOwnInd(), it);
                        
            it = std::next(m_children.begin(), m_degree);

        }
    }



    //if overloaded...
    if( size > m_degree && m_degree && m_tree->getNode(m_children.front()).getDegree()) {

        int sum_bw = 0;

        unsigned size, height;
        std::vector<unsigned> ids,heights,sizes;

        //i find the node with the minimum estimated height
        for(auto it = m_children.begin(); it!=std::next(m_children.begin(),m_degree); it++){  

            if(m_tree->getNode(*it).getDegree()){

#ifndef UPDATE
        
    m_tree->getNode(*it).update();

#endif

                std::tie(height,size) = m_tree->getNode(*it).estimatedHeight();
                ids.push_back(*it);
                heights.push_back(height);
                sizes.push_back(size);
        }
    }
        while((int)m_children.size() > (int)m_degree){

            unsigned index_min_node = 0;

            for(auto& elem : ids){

                auto i = &elem - &ids[0];

                if(heights[i] <  heights[index_min_node] || (heights[i] ==  heights[index_min_node] && sizes[i] < sizes[index_min_node])){

                    index_min_node = i;
                }
            }


            unsigned idNode = ids[index_min_node];

            //add the degree of the node pushed in the subtree of the selected node
            auto pushedNode = std::next(m_children.begin(), m_degree);
            
            m_tree->getNode(idNode).addDegree(m_tree->getNode(*pushedNode).getDegree());

            m_tree->getNode(idNode).adopt(getOwnInd(),pushedNode);


            std::tie(height,size) = m_tree->getNode(idNode).estimatedHeight();

            sizes[index_min_node]=size;
            heights[index_min_node]=height;
        }
        return true;
    }
 return false;   
}



bool LocalInfoNode2::isReachable() const {

    return reachable;
}




int LocalInfoNode2::getBalance() const {
    int balance = 0;
    if(!m_children.empty()) {
        auto iteFirstChild = m_children.begin();
        auto iteSecondChild = ++m_children.begin();
        //If not only child and is a leaf
        if(iteSecondChild != m_children.end() && !m_tree->getNode(*iteFirstChild).m_children.empty()) {
            balance += m_tree->getNode(*iteFirstChild).m_subtreeSize;
            for(; iteSecondChild != m_children.end(); ++iteSecondChild) {
                balance -= m_tree->getNode(*iteSecondChild).m_subtreeSize;
            }
        }
    }
    return balance;
}

void LocalInfoNode2::changeSubtreeSize(int _delta) {
    // std::cout << "("<<getOwnInd()<<", "<<getParent()<<")" << ".changeSubtreeSize(" << _delta <<") : " << m_subtreeSize;
    m_subtreeSize += _delta;
    // std::cout << " => " << m_subtreeSize << "-->" << this << std::endl;
    if(this->hasParent()) {
        // std::cout << getOwnInd() << "^" << getParent() << std::endl;
        m_tree->getNode(this->getParent()).changeSubtreeSize(_delta);
    }
}


int LocalInfoNode2::getDepth() const {
    if(getOwnInd() == 0) {
        return 0;
    }
    auto& parent = m_tree->getNode(getParent());

    return 1+parent.getDepth();
}



