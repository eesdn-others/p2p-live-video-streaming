#ifndef NAMES_CPP
#define NAMES_CPP

#include "names.hpp"

#include "RandomNode.hpp"
#include "SmallestNode.hpp"
#include "NonInterruptNode.hpp"
// #include "DelayNode.hpp"
#include "PartialInfoNode.hpp"
#include "HeterogeneousNode.hpp"
#include "LocalInfoNode1.hpp"
#include "LocalInfoNode2.hpp"
#include "LocalInfoNode3.hpp"
#include "FullInfoNode.hpp"
#include "SmallestLFLNode.hpp"

template<>
inline std::string getName<RandomNode>() {
    return "rand";
}

template<>
inline std::string getName<SmallestNode>() {
    return "small";
}

template<>
inline std::string getName<NonInterruptNode>() {
    return "nointerr";
}

// template<>
// inline std::string getName<DelayNode>() {
//     return "delay";
// }

template<>
inline std::string getName<PartialInfoNode>() {
    return "partial";
}

template<>
inline std::string getName<HeterogeneousNode>() {
    return "heterogeneous";
}

template<>
inline std::string getName<LocalInfoNode1>() {
    return "local1";
}

template<>
inline std::string getName<LocalInfoNode2>() {
    return "local2";
}

template<>
inline std::string getName<LocalInfoNode3>() {
    return "local3";
}

template<>
inline std::string getName<FullInfoNode>() {
    return "full";
}

template<>
inline std::string getName<SmallestLFLNode>() {
    return "smallestLFL";
}

#endif