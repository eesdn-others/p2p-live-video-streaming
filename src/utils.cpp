#ifndef UTILS_CPP
#define UTILS_CPP

#include "utils.hpp"

#include <numeric>
#include <fstream>
#include <iterator>
#include <vector>
#include <tuple>


///Return the median of a sequence of numbers defined by the random
///access iterators begin and end.  The sequence must not be empty
///(median is undefined for an empty set).
///
///The numbers must be convertible to double.
template<class RandAccessIter>
double median(RandAccessIter begin, RandAccessIter end)
  throw(median_of_empty_list_exception){
  if(begin == end){ throw median_of_empty_list_exception(); }
  int size = end - begin;
  int middleIdx = size/2;
  RandAccessIter target = begin + middleIdx;
  std::nth_element(begin, target, end);

  if(size % 2 != 0){ //Odd number of elements
    return *target;
  }else{            //Even number of elements
    double a = *target;
    RandAccessIter targetNeighbor= target-1;
    std::nth_element(begin, targetNeighbor, end);
    return (a+*targetNeighbor)/2.0;
  }
}
 
template<class Tuple, int... Is>
void print(std::ostream& _out, Tuple& t, seq<Is...>)
{
  using swallow = int[];
  (void)swallow{0, (void(_out << " " << *std::get<Is>(t)), 0)...};
  (void)swallow{0, (void(++std::get<Is>(t)), 0)...};
}


template<typename T, typename... Args>
void toCSV(const std::string& _filename, const T& _tab, Args... args) {
  std::ofstream ofs (_filename, std::ofstream::out);
  auto tuple = make_tuple(_tab.begin(), args.begin()...);
  while(std::get<0>(tuple) != _tab.end()) {
    print(ofs, tuple, gen_seq<std::tuple_size<decltype(tuple)>::value>());
    ofs << std::endl;
  }
}

template<typename InputIterator>
double mean(InputIterator first, InputIterator last) {
    return std::accumulate(first, last, 0.0) / std::distance(first, last);
}

template<typename InputIterator1, typename InputIterator2>
double mean(InputIterator1 first, InputIterator1 last, InputIterator2 wFirst, InputIterator2 wLast) {
    std::vector<double> diff (std::distance(wFirst, wLast));
    std::adjacent_difference(wFirst, wLast, diff.begin());
    double totalWeight = std::accumulate(diff.begin(), diff.end(), 0.0);
    return std::inner_product(first, last, diff.begin(), 0.0) / totalWeight;
}

inline std::vector<double> frange(double _start, double _stop, int _nbSteps) {
    std::vector<double> range(_nbSteps);
    int nm1 = _nbSteps - 1;
    double nm1inv = 1.0 / nm1;
    for(int i = 0; i < _nbSteps; ++i) {
        range[i] = nm1inv * (_start*(nm1 - i) + _stop*i);
    }
    return range;
}

template<typename T>
void printClass(const T& _obj) {
  std::cout << _obj << std::endl;
}




#endif

