#include "FullInfoNode.hpp"
#include <algorithm>
#include <iostream>
#include <cassert>
#include <limits>
#include <stdlib.h>

/*
TODO
- remove from children all in die


- get the node of the last level of the subtree - 1 and put there nodes that do not find a place

*/


FullInfoNode::FullInfoNode(int _degree, Tree<FullInfoNode>* _tree, int _parent) :
Node(_degree, _parent, 0),
m_tree(_tree)
{
}


#ifdef UPDATE
void FullInfoNode::update(){
    ;
}
#endif


void FullInfoNode::addChild(NodeId _id) {
     //std::cout<<getOwnInd()<<" add child  "<<_id<<std::endl;

    //std::cout << "m_tree : " << m_tree << std::endl;
    //std::cout << getOwnInd() << ".addChild("<<_id<<')'<<std::endl;
    
    // get the node reference
    FullInfoNode& node = m_tree->getNode(_id);
    // add the id of the node in the list of sons
    m_children.push_back(_id);
    // I set me as node parent
    node.setParent(getOwnInd());
    // change the subtree size
    this->changeSubtreeSize(node.m_subtreeSize);
}

void FullInfoNode::die() {
    // parent is the reference to my father
    FullInfoNode& parent = m_tree->getNode(getParent());

    if(m_children.size() == 0){
        parent.removeFromChildren(getOwnInd());
        return;
    }
    int bandwidth_available_parent,children_to_adopt,b_avl,children_death_node;

    //this number could be negative if the node is overloaded, +1 because I still don't kill the node
    b_avl = parent.getDegree() - parent.getChildren().size() + 1;
    bandwidth_available_parent = 0 > b_avl? 0 : b_avl;
    children_death_node  = m_children.size();
    // maximum number of children that the grandfather can adopt
    children_to_adopt = bandwidth_available_parent < children_death_node? bandwidth_available_parent : children_death_node;
    this->orderChildren();

    // he adopts the nodes with maximum bandwidth
    for(int i=0; i<children_to_adopt; i++){
        parent.adopt(getOwnInd(),m_children.begin());

    }

    // if i've adopted all the children of the death node i can stop
    if(!m_children.size()){
        parent.removeFromChildren(getOwnInd());
        return;
    } 

    //else I get all the node 
    
    std::vector<unsigned> subtree = m_tree->getNode(0).getSubtreeOrderedByLevel();
    std::vector<unsigned> children_to_be_adopted;
    this->getSubtree(children_to_be_adopted);


    //in the subtree there are also the node to be adopted, so i remove them
    for(auto it = children_to_be_adopted.begin();it!=children_to_be_adopted.end();it++){
        subtree.erase(std::remove(subtree.begin(), subtree.end(), *it), subtree.end());
    }   

    //all the nodes in the subtree of the death node not adopted by the grandfather become orphans so I can process one by one

    parent.removeFromChildren(getOwnInd());
    
    children_to_be_adopted.erase(std::remove(children_to_be_adopted.begin(), children_to_be_adopted.end(), getOwnInd()), children_to_be_adopted.end());
    /*

    */
    for(auto parent = children_to_be_adopted.begin(); parent!= children_to_be_adopted.end(); parent++){
        auto childrens = m_tree->getNode(*parent).getChildren();
            for(auto child = childrens.begin(); child!= childrens.end();child++  ){
                m_tree->getNode(*parent).removeFromChildren(*child);
            }
        
    }

    //order node of the subtree by degree

    sort(children_to_be_adopted.begin(), children_to_be_adopted.end(),
            [&](const unsigned& a, const unsigned& b)
            {
                return m_tree->getNode(a).getDegree() > m_tree->getNode(b).getDegree();
            });


    //choose a position to push the node that does not receive the video
    unsigned j = 0;
    bool found ;
    for(auto id_novideo = children_to_be_adopted.begin(); id_novideo!= children_to_be_adopted.end(); id_novideo++){
        found = false;
        for(auto id_video = subtree.begin(); id_video != subtree.end() && !found; id_video++ ){

            //the node get adopted

            if(m_tree->getNode(*id_novideo).getDegree() <= m_tree->getNode(*id_video).getDegree()   &&  (int)m_tree->getNode(*id_video).getDegree() > (int)m_tree->getNode(*id_video).getChildren().size() ){

                found=true;
                m_tree->getNode(*id_video).addChild(*id_novideo);
                subtree.push_back(*id_novideo);


            }
            else if( m_tree->getNode(*id_novideo).getDegree() > m_tree->getNode(*id_video).getDegree() && *id_video){

                found=true;

                //the node push-replace a node that receive the video

                auto parent_selected_node = m_tree->getNode(*id_video).getParent();
                m_tree->getNode(parent_selected_node).addChild(*id_novideo);
                auto children_parent_selected_node = m_tree->getNode(parent_selected_node).m_children;


                m_tree->getNode(*id_novideo).adopt(m_tree->getNode(*id_video).getParent(), std::find(m_tree->getNode(m_tree->getNode(*id_video).getParent()).m_children.begin(), m_tree->getNode(m_tree->getNode(*id_video).getParent()).m_children.end(), *id_video));

                while(!m_tree->getNode(*id_video).getChildren().empty()) {
                    m_tree->getNode(*id_novideo).adopt(*id_video, m_tree->getNode(*id_video).getChildren().begin());
                }
                subtree.push_back(*id_novideo);
            }

        }

        if (! found){
            //take all the nodes in the before-last level
            std::vector<unsigned> node_before_last_level = parent.getNodeLevel(parent.getHeight()-1);

            auto node_to_push = m_tree->getNode(node_before_last_level[j++ % node_before_last_level.size()]).getParent();
            //added
            m_tree->getNode(node_to_push).addChild(*id_novideo);

        }

        if(found){
            //reorder by Level
            sort(subtree.begin(), subtree.end(),
                [&](const NodeId& a, const NodeId& b)
                {
                return m_tree->getNode(a).getLevel() < m_tree->getNode(b).getLevel();
                });
        }

    }
    //clear children of the death node
    this->m_children.clear();
    //subtree pull
    m_tree->getNode(0).pullNodes();

}

/*
swap the node with the node passed as argument, e.g. node a with degree 1 at level L, node b with degree 3 at level L+1-----> a.swap(b)
*/
void FullInfoNode::swap(NodeId _id){
    //std::cout<<getOwnInd()<<"<------->"<<_id<<std::endl;
    FullInfoNode& parent1 = m_tree->getNode(this->getParent());
    FullInfoNode& parent2 = m_tree->getNode(m_tree->getNode(_id).getParent());

    // it is possibile that the node to swap are father-child, so I have to manage differently these 2 cases

    std::list<NodeId> children1 = m_children;
    std::list<NodeId> children2 = m_tree->getNode(_id).m_children;
    // case 1: father-son swap
    if(m_tree->getNode(_id).getParent() == (int) getOwnInd()){

        parent1.removeFromChildren(getOwnInd());
        this->removeFromChildren(_id);
        parent1.addChild(_id);
        m_tree->getNode(_id).addChild(getOwnInd());
        for(auto it = children2.begin();it!=children2.end();it++){
            if(*it != getOwnInd()){
                m_tree->getNode(getOwnInd()).adopt(_id, std::find(m_tree->getNode(_id).m_children.begin(), m_tree->getNode(_id).m_children.end(), *it));
            }
        }
        for(auto it = children1.begin();it!=children1.end();it++){
            if(*it != _id){
                m_tree->getNode(_id).adopt(getOwnInd(), std::find(m_tree->getNode(getOwnInd()).m_children.begin(), m_tree->getNode(_id).m_children.end(), *it));
            }
        }
    }
    //case 2: all other cases
    else{
        parent1.removeFromChildren(getOwnInd());
        parent2.removeFromChildren(_id);
        parent1.addChild(_id);
        for(auto it = children1.begin();it!=children1.end();it++){
            if(*it != _id){
                m_tree->getNode(_id).adopt(getOwnInd(), std::find(m_tree->getNode(getOwnInd()).m_children.begin(), m_tree->getNode(_id).m_children.end(), *it));
            }
        }
        for(auto it = children2.begin();it!=children2.end();it++){
            if(*it != getOwnInd()){
                m_tree->getNode(getOwnInd()).adopt(_id, std::find(m_tree->getNode(_id).m_children.begin(), m_tree->getNode(_id).m_children.end(), *it));
            }
        }
        parent2.addChild(getOwnInd());
    }
}

bool FullInfoNode::swapNodes(){
    bool change_made = false;
    std::vector<unsigned> subtree;
    getSubtree(subtree);
    
    // i can't swap the root, so I remove node 0
    subtree.erase(remove(subtree.begin(), subtree.end(), 0), subtree.end());

    //for(auto it=subtree.begin();it!=subtree.end();it++){
    //    std::cout<<*it<<"\t";
    //}
    //std::cout<<"\n";
    //find the max of the next levels
    std::vector<unsigned> nodes_lower_levels;
    int max_bw_under, node_max_bw_under;
    unsigned node1;
    bool new_max=true;


    for(unsigned i = 0; i < subtree.size(); i++){
        node1 = subtree[i];

        //if I need the node with max bw of next levels..
        if(new_max){

            nodes_lower_levels = getNodeFromLevel(m_tree->getNode(node1).getLevel()+1);

            //if empty means that this is first node of the last level, so I exit
            if(!nodes_lower_levels.size()) return change_made;
            //when I consider the first node of the last level I exit
            //if(nodes_lower_levels.empty()) return change_made;

            max_bw_under = -1;
            for(auto it =nodes_lower_levels.begin(); it!= nodes_lower_levels.end();it++ ){
                if(m_tree->getNode(*it).getDegree() > max_bw_under){
                    max_bw_under = m_tree->getNode(*it).getDegree();
                    node_max_bw_under = *it;
                }
            }
        }

        if(m_tree->getNode(node1).getDegree() < max_bw_under  &&  node1 ){ //root can't be swapped
            change_made = true;
            m_tree->getNode(node1).swap(node_max_bw_under);
            new_max = true;
        }
        //I update the max if the next node is of a different level and the max is on the same level as the next node
        else if(i+1!= subtree.size() && m_tree->getNode(node1).getLevel() != m_tree->getNode(subtree[i+1]).getLevel() && m_tree->getNode(node_max_bw_under).getLevel() == m_tree->getNode(subtree[i+1]).getLevel()){
            new_max=true;
        }
        //else I keep the same max
        else{
            new_max=false;
        }
        
    }
    return change_made;
}


bool FullInfoNode::isUnderLoaded(){

    return this->m_degree > (int)this->m_children.size();
}


bool FullInfoNode::isOverLoaded(){

    return this->m_degree < (int)this->m_children.size();
}


/*

when called on a node, the node adopts the nodes with max degree of the next level, since i can call this method on 2+ children, there is
a skip that is the number of nodes of the next level already took
*/
void FullInfoNode::takeMaxNextLevel(unsigned root_subtree, unsigned skip){

    // if the node is in the last level of the subtree stop
    if(this->getLevel() == m_tree->getNode(root_subtree).getHeight() + m_tree->getNode(root_subtree).getDepth() || !m_degree) return;

    std::vector<unsigned> nodes_next_level = m_tree->getNode(root_subtree).getNodeLevel(this->getLevel()+1);

    int nodes_to_take = (int) (nodes_next_level.size() - skip) < this->m_degree ? (int)nodes_next_level.size() - skip : this->m_degree;

    if((int) nodes_next_level.size() - (int)skip < 0  || nodes_to_take <= 0) return;
    

    std::vector<unsigned> selected_nodes(&nodes_next_level[skip], &nodes_next_level[skip + nodes_to_take]);

    int sum = 0;

    for(auto it = selected_nodes.begin(); it!= selected_nodes.end();it++){
        m_tree->getNode(*it).takeMaxNextLevel(root_subtree, sum);  
        sum+= m_tree->getNode(*it).getDegree();
    }
    
    int n_children = (int)this->getChildren().size();
    for(auto it = selected_nodes.begin(); it!= selected_nodes.end();it++){
        if(m_tree->getNode(*it).getParent() != (int)getOwnInd()){
            if(n_children == m_degree){
                m_tree->getNode(*m_children.end()-1).swap(*it);
            }
            else{
                FullInfoNode& parent = m_tree->getNode(m_tree->getNode(*it).getParent());
                this->adopt(parent.getOwnInd(), std::find(parent.m_children.begin(), parent.m_children.end(), *it)); 
                this->orderChildren();
            }
            
        }

    }
}   
/*
when called on a node, the node, it adopts the node with max degree of level L+2, and call takeMaxNextLevel
*/
void FullInfoNode::pull(unsigned root_subtree){ 

    unsigned begin_level = this->getLevel() + 2, i = 0;
    unsigned available_bandwidth = m_degree - m_children.size();
    std::vector<unsigned> nodes_next_level = m_tree->getNode(root_subtree).getNodeLevel(begin_level);
    unsigned iter = available_bandwidth < nodes_next_level.size()? available_bandwidth : nodes_next_level.size();

    while(i < iter){

        unsigned selected_node = nodes_next_level[i++];

        m_tree->getNode(selected_node).takeMaxNextLevel(getOwnInd(), 0);

        FullInfoNode& parent = m_tree->getNode(m_tree->getNode(selected_node).getParent());
        this->adopt(parent.getOwnInd(), std::find(parent.m_children.begin(), parent.m_children.end(), selected_node));

    }
}

/*
check all the nodes from heigher to lower levels, when it found an underloaded node calls the pull method
*/

bool FullInfoNode::pullNodes(){
    bool change_made = false;
    std::vector<unsigned> subtree = getNodeFromLevel(0);
    unsigned height = this->getHeight();
    for(unsigned i = 0; i < subtree.size(); i++){
        unsigned node = subtree[i];
        if(m_tree->getNode(node).isUnderLoaded()  && (m_tree->getNode(node).getLevel() <= height - 2)){
            change_made = true;
            m_tree->getNode(node).pull(0);
        }
    }
    return change_made;
}




void FullInfoNode::adopt(NodeId _oldFatherInd, std::list<NodeId>::iterator _iteChildInd) {
    // std::cout << getOwnInd() << " adopt " << *_iteChildInd << " from " << _oldFatherInd << ", new SS :"
    // << m_tree->getNode(*_iteChildInd).m_subtreeSize << std::endl;
    
    m_children.splice(m_children.end(), m_tree->getNode(_oldFatherInd).m_children, _iteChildInd);
    m_tree->getNode(*_iteChildInd).setParent(getOwnInd());

    this->changeSubtreeSize(m_tree->getNode(*_iteChildInd).m_subtreeSize);
    m_tree->getNode(_oldFatherInd).changeSubtreeSize(-m_tree->getNode(*_iteChildInd).m_subtreeSize);
}

void FullInfoNode::replace(NodeId _old, NodeId _new) {
    // std::cout << _new << " replaces " << _old << " at " << getOwnInd() << std::endl;
    FullInfoNode& oldNode = m_tree->getNode(_old);
    FullInfoNode& newNode = m_tree->getNode(_new);
    
    oldNode.removeFromChildren(_new);

    auto ite = std::find(m_children.begin(), m_children.end(), _old);
    
    newNode.setParent(getOwnInd());

    oldNode.setParent(-1);
    *ite = _new;
    int newSS = newNode.m_subtreeSize;
    int oldSS = oldNode.m_subtreeSize;
    
    this->changeSubtreeSize(newSS-oldSS);
}

void FullInfoNode::removeFromChildren(NodeId _child) {
    // std::cout << _child << " leaves " << getOwnInd() << std::endl;
    FullInfoNode& child = m_tree->getNode(_child);
    m_children.remove(_child);
    this->changeSubtreeSize(-child.m_subtreeSize);
    child.setParent(-1);
}




/*
order children by degree
*/
void FullInfoNode::orderChildren(){
    m_children.sort([&](NodeId i, NodeId j){
        if (m_tree->getNode(i).getDegree() > m_tree->getNode(j).getDegree()){
            return true;
        }
        return false;
    });
}


/*
return the level of the node in the subtree
*/
unsigned FullInfoNode::getLevel(){
    if(!getOwnInd()) return 0;
    return 1 + m_tree->getNode(getParent()).getLevel();
}

/*
return the ids of all the node in its subtree ordered by level
*/



std::vector<unsigned> FullInfoNode::getSubtreeOrderedByLevel(){

    std::map<unsigned, unsigned> subtree;
    std::vector<unsigned> ordered_nodes;
    this->getSubtreeLevel(subtree);


    auto max = std::max_element(subtree.begin(), subtree.end(),
    [](const std::pair<unsigned, unsigned>& p1, const std::pair<unsigned, unsigned>& p2) {
        return p1.second < p2.second; });
    auto min = std::min_element(subtree.begin(), subtree.end(),
    [](const std::pair<unsigned, unsigned>& p1, const std::pair<unsigned, unsigned>& p2) {
        return p1.second < p2.second; });
    //std::cout<<min->second<<max->second<<std::endl;

    for(unsigned level = min->second; level<max->second+1; level++){
        for(auto it = subtree.begin();it != subtree.end();it++){
            if(it->second==level){
                ordered_nodes.push_back(it->first);
            }
        }
    }
    return ordered_nodes;
}

/*
return a map <id,level> for each node in the subtree
*/
void FullInfoNode::getSubtreeLevel(std::map<unsigned, unsigned> &subtree){
    
    subtree[getOwnInd()] = this->getLevel();

    for(auto it=m_children.begin();it!=m_children.end();it++){

        m_tree->getNode(*it).getSubtreeLevel(subtree);
    }
}



/*
return the subtree rooted in the node
*/
void FullInfoNode::getSubtree(std::vector<unsigned> &subtree){
    
    subtree.push_back(getOwnInd());

    for(auto it=m_children.begin();it!=m_children.end();it++){

        m_tree->getNode(*it).getSubtree(subtree);
    }
}

/*
return the height of the subtree
*/
unsigned FullInfoNode::getHeight(){
    if(!this->m_children.size()) return 0;
    unsigned height = 0;
    for(auto it = m_children.begin(); it!= m_children.end(); it++){
        if(m_tree->getNode(*it).getHeight() > height ){
            height = m_tree->getNode(*it).getHeight();
        }
    }
    return height+1;
}


/*
return the nodes from the level passed as argument ordered by degree(in the subtree on the called node)
*/
std::vector<unsigned> FullInfoNode::getNodeFromLevel(unsigned level){
    std::vector<unsigned> subtree,nodes_level;
    this->getSubtree(subtree);
    for(auto it = subtree.begin();it!= subtree.end();it++ ){
        if(m_tree->getNode(*it).getLevel()>=level){
            nodes_level.push_back(*it);
        }
    }

    sort(nodes_level.begin(), nodes_level.end(),
                [&](const unsigned& a, const unsigned& b)
                {
                    if(m_tree->getNode(a).getLevel() == m_tree->getNode(b).getLevel()){

                        return m_tree->getNode(a).getDegree() > m_tree->getNode(b).getDegree();
                    }
                    else return m_tree->getNode(a).getLevel() < m_tree->getNode(b).getLevel();
                });


    return nodes_level;
}
/*
return the nodes of the level passed as argument ordered by degree(in the subtree on the called node)
*/
std::vector<unsigned> FullInfoNode::getNodeLevel(unsigned level){
    std::vector<unsigned> subtree,nodes_level;
    this->getSubtree(subtree);
    for(auto it = subtree.begin();it!= subtree.end();it++ ){
        if(m_tree->getNode(*it).getLevel()==level){
            nodes_level.push_back(*it);
        }
    }

    sort(nodes_level.begin(), nodes_level.end(),
                [&](const unsigned& a, const unsigned& b)
                {
                    if(m_tree->getNode(a).getLevel() == m_tree->getNode(b).getLevel()){

                        return m_tree->getNode(a).getDegree() > m_tree->getNode(b).getDegree();
                    }
                    else return m_tree->getNode(a).getLevel() < m_tree->getNode(b).getLevel();
                });


    return nodes_level;
}



bool FullInfoNode::repair() {

    //if overloaded...
    //clock_t begin,end;
    //begin=clock();
    this->orderChildren();
    unsigned size = this->m_children.size();
    bool change_made=false;
    std::vector<NodeId> node_receive_video, node_dont_receive_video;
    std::list<NodeId> children = m_children;

    if((int)size > m_degree && m_degree) {
         //std::cout<<"*****************************BEFORE*************************************\n";
        change_made = true; 
        //std::cout<<getOwnInd() << "  is overloaded\n";
        //if overloaded...
        // std::cout<<m_tree->getNode(0)<<std::endl;
        // std::cout<<"video for:      ";

        // std::cout<<"\n";
        // vector of children that do not receive the video
         //std::cout<<"no video for:    ";
        for(auto it=std::next(children.begin(), m_degree);it!=children.end();it++){
          //   std::cout<<*it<<"\t";
            node_dont_receive_video.push_back(*it);
        }
         //std::cout<<"\n";

        // if there is no children with degree > 0 stop

        std::vector<NodeId> nodes_subtree_video, nodes_subtree_novideo;

        std::vector<unsigned> subtree;

        for(auto it=node_dont_receive_video.begin();it!=node_dont_receive_video.end();it++){

            subtree = m_tree->getNode(*it).getNodeFromLevel(0);
            nodes_subtree_novideo.insert(nodes_subtree_novideo.end(),subtree.begin(),subtree.end());
        }
        /*

        std::cout<<"subtree video:      ";
        for(auto it=nodes_subtree_video.begin();it!=nodes_subtree_video.end();it++){
            std::cout<<*it<<"\t";
        }

        std::cout<<"\n";

        // vector of children that do not receive the video

        std::cout<<"subtree no video:      ";
        for(auto it=nodes_subtree_novideo.begin();it!=nodes_subtree_novideo.end();it++){
            std::cout<<*it<<"\t";
        }

        std::cout<<"\n";
*/
        
        //all nodes in the subtree no video become orphans


        for(auto it=node_dont_receive_video.begin();it!=node_dont_receive_video.end();it++){
            this->removeFromChildren(*it);
        }
        for(auto parent = nodes_subtree_novideo.begin(); parent!= nodes_subtree_novideo.end(); parent++){
            auto childrens = m_tree->getNode(*parent).getChildren();
            for(auto child = childrens.begin(); child!= childrens.end();child++  ){
                m_tree->getNode(*parent).removeFromChildren(*child);
            }
        }


        //choose a position to push the node that does not receive the video
        unsigned j = 0;
        bool found ;

        nodes_subtree_video = m_tree->getNode(0).getSubtreeOrderedByLevel();

        for(auto id_novideo = nodes_subtree_novideo.begin(); id_novideo!= nodes_subtree_novideo.end(); id_novideo++){
            found = false;
             //std::cout<<"looking a place for  "<< *id_novideo<<"  \n";
            for(auto id_video = nodes_subtree_video.begin(); id_video != nodes_subtree_video.end() && !found; id_video++ ){
               
                if(m_tree->getNode(*id_novideo).getDegree() <= m_tree->getNode(*id_video).getDegree()   &&  (int)m_tree->getNode(*id_video).getDegree() > (int)m_tree->getNode(*id_video).getChildren().size() ){
                    /*
                    std::cout<<m_tree->getNode(*id_video)<<std::endl;
                    std::cout<<m_tree->getNode(*id_novideo)<<std::endl;
                    std::cout<<"if 1\n";
                    std::cout<<"node  "<< *id_video  <<"adopts node  "<< *id_novideo <<std::endl;
                    std::cout<<"node  "<< m_tree->getNode(*id_video).getDegree() << "children"<< m_tree->getNode(*id_video).getChildren().size()<<"adopts node  "<< m_tree->getNode(*id_novideo).getDegree()<<std::endl;
                    std::cout<<m_tree->getNode(*id_video)<<std::endl;
                    std::cout<<m_tree->getNode(*id_novideo)<<std::endl;

                    std::cout<<"\n\n\n";
                    */
                    found=true;
                    //std::cout<<"1\n";
                    m_tree->getNode(*id_video).addChild(*id_novideo);
                    //nodes_subtree_video.push_back(*id_novideo);
                    //std::cout<<"\n\n\n";
                    //std::cout<<"2\n";
                    
                }
                else if( m_tree->getNode(*id_novideo).getDegree() > m_tree->getNode(*id_video).getDegree() && *id_video){
                      //std::cout<<"else if 2\n";
                    found=true;

                    //push-replace

                    auto parent_selected_node = m_tree->getNode(*id_video).getParent();
                    m_tree->getNode(parent_selected_node).addChild(*id_novideo);
                    auto children_parent_selected_node = m_tree->getNode(parent_selected_node).m_children;


                    m_tree->getNode(*id_novideo).adopt(m_tree->getNode(*id_video).getParent(), std::find(m_tree->getNode(m_tree->getNode(*id_video).getParent()).m_children.begin(), m_tree->getNode(m_tree->getNode(*id_video).getParent()).m_children.end(), *id_video));

                    while(!m_tree->getNode(*id_video).getChildren().empty()) {
                        m_tree->getNode(*id_novideo).adopt(*id_video, m_tree->getNode(*id_video).getChildren().begin());
                    }
                    //nodes_subtree_video.push_back(*id_novideo);
                    //std::cout<<"node  "<< *id_novideo  <<"push_replace  "<< *id_video <<std::endl;
                }


            }

            if (! found){

                std::vector<unsigned> node_before_last_level = this->getNodeLevel(this->getHeight()-1);

                //cambiare: su uno a casa del punultimo livello

                auto node_to_push = m_tree->getNode(node_before_last_level[j++ % node_before_last_level.size()]).getParent();
                m_tree->getNode(node_to_push).addChild(*id_novideo);
                //push on a node in the max level - 1
                //get Node at level max L - 1
            }

            if(found){
                //reorder by Level
                sort(nodes_subtree_video.begin(), nodes_subtree_video.end(),
                    [&](const unsigned& a, const unsigned& b)
                    {
                    return m_tree->getNode(a).getLevel() < m_tree->getNode(b).getLevel();
                    });
            }

        }
        
    }


    //repair subtree to make it respect the 2 optimality conditions
    /*
    end=clock();
    m_tree->repair_time+=(double(end - begin) / CLOCKS_PER_SEC);
    begin=clock();
    */
    /*
    if(!getOwnInd()){
    end=clock();
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    std::cout<< "repair--->" <<elapsed_secs<<std::endl;
    begin=clock();

    }
    */
    bool swap = m_tree->getNode(0).swapNodes();
    /*
    
    end=clock();
    m_tree->swap_time+=(double(end - begin) / CLOCKS_PER_SEC);
    begin=clock();
 

    */
    bool pull = m_tree->getNode(0).pullNodes();
    /*
    end=clock();
    m_tree->pull_time+=(double(end - begin) / CLOCKS_PER_SEC);
    begin=clock();
    */
    /*
    if(!getOwnInd()){
    end=clock();
     double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    std::cout<< "pull--->" <<elapsed_secs<<std::endl;
    
}
*/ 
/*
    std::cout<< "*************************************************************************************"<<std::endl;
    std::cout<< "repair--->" <<m_tree->repair_time<<std::endl;
    std::cout<< "swap--->" <<m_tree->swap_time<<std::endl;
    std::cout<< "pull--->" <<m_tree->pull_time<<std::endl;
    std::cout<< "#####################################################################################"<<std::endl;
  */
    return change_made + swap + pull ;
}



bool FullInfoNode::isReachable() const {
    if(getOwnInd() == 0) {
        return true;
    }
    auto& parent = m_tree->getNode(getParent());
    if(parent.isReachable()) {
        parent.orderChildren();
        int i = 0;
        auto ite = parent.m_children.begin();
        while(i < parent.m_degree) {
            if(*ite == getOwnInd()) {
                if(m_new == true) m_new = false;
                return true;
            }
            ++ite;
            ++i;
        }
    }
    return false;
}




int FullInfoNode::getBalance() const {
    int balance = 0;
    if(!m_children.empty()) {
        auto iteFirstChild = m_children.begin();
        auto iteSecondChild = ++m_children.begin();
        //If not only child and is a leaf
        if(iteSecondChild != m_children.end() && !m_tree->getNode(*iteFirstChild).m_children.empty()) {
            balance += m_tree->getNode(*iteFirstChild).m_subtreeSize;
            for(; iteSecondChild != m_children.end(); ++iteSecondChild) {
                balance -= m_tree->getNode(*iteSecondChild).m_subtreeSize;
            }
        }
    }
    return balance;
}

void FullInfoNode::changeSubtreeSize(int _delta) {
    // std::cout << "("<<getOwnInd()<<", "<<getParent()<<")" << ".changeSubtreeSize(" << _delta <<") : " << m_subtreeSize;
    m_subtreeSize += _delta;
    // std::cout << " => " << m_subtreeSize << "-->" << this << std::endl;
    if(this->hasParent()) {
        // std::cout << getOwnInd() << "^" << getParent() << std::endl;
        m_tree->getNode(this->getParent()).changeSubtreeSize(_delta);
    }
}


int FullInfoNode::getDepth() const {
    if(getOwnInd() == 0) {
        return 0;
    }
    auto& parent = m_tree->getNode(getParent());

    return 1+parent.getDepth();
}



