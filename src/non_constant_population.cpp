#ifndef NON_CONSTANT_POPULATION_CPP
#define NON_CONSTANT_POPULATION_CPP

#include "non_constant_population.hpp"

#include <vector>
#include <list>
#include <algorithm>
#include <map>
#include <stack>
#include <fstream>
#include <iostream>

#include "nonConstantTree.hpp"
#include "NonConstantExperiment.hpp"
#include "utils.hpp"
#include "names.hpp"


/** 
Simulation for the interruption metrics
Save step by step the metrics and save them in a file with the name:
"nameOfTheProtocol"_"numberOfNodes"_"churnRate"_interrupt.plt
in the interrupt folder
in a csv format where the columns represent the following metrics:
1st: time t of the simulation
2nd: average interruption time over the nodes at time t
3rd: average number of interruption over the nodes at time t
4th: average interruption time over the non new nodes at time t
5th: average number of interruption over the non new nodes at time t
*/
template<class Node> 
#ifdef UPDATE
void interrupt(NonConstantTree<Node>& tree, int _maxTime, double _joinRate, double _churnRate, unsigned phase,int (*f)(void), double _updateTime) {
#else
void interrupt(NonConstantTree<Node>& tree, int _maxTime, double _joinRate, double _churnRate, unsigned phase,int (*f)(void)) {
#endif


#ifdef UPDATE
    std::string filename = "interruptNC/phase"+ std::to_string(phase) + "_" + getName<Node>() + "_"+ std::to_string(int(_updateTime))+"_interrupt.plt";
#else	
	std::string filename = "interruptNC/phase"+ std::to_string(phase) + "_" + getName<Node>() + "_interrupt.plt";
#endif
	
	std::cout << filename << std::endl;
	
	std::vector<unsigned> wasReachable;




	double timeWithoutVideoNotNew,timeWithoutVideo, interruptionsTime, interruptionsTimeNotNew;
    unsigned nbInterruption, nbInterruptionNotNew;
    std::list<unsigned> nodes_count;
    std::list<double> times, meanTime, meanNumber, meanTimeNotNew, meanNumberNotNew;
	
    auto nodes = tree.getValidNodes();

	for(auto it = nodes.begin(); it!=nodes.end(); it++){
		if(tree.getNode(*it).isReachable()){
			wasReachable.push_back(*it);
		}
	}


    double lastTime = 0;
	

    #ifdef UPDATE  
    NonConstantExperiment<Node> exp {&tree, _joinRate, _churnRate, 1.0, f, _updateTime}; 
    #else
    NonConstantExperiment<Node> exp {&tree, _joinRate, _churnRate, 1.0, f}; 
    #endif

	

	while(exp.getTime() < _maxTime) {
        nbInterruption = nbInterruptionNotNew = timeWithoutVideoNotNew = timeWithoutVideo = interruptionsTime = interruptionsTimeNotNew = 0;
		//std::cout << exp.getTime() << std::endl;
		double now = exp.getTime();

		exp.runUntil(1, _maxTime);

         //update of the reachable nodes - if the node was reachable but leaves (position is not valid) remove

        auto it = wasReachable.begin();

        while(it != wasReachable.end()) {
            if(!tree.isValid(*it)) {
                it = wasReachable.erase(it);
            }
            else ++it;
        }


		std::cout << tree.getNbNodes() << " => Join:" << exp.m_nbJoin << ", Leave:" << exp.m_nbLeave << ", Fix: " << exp.m_nbFix << ", Time: " << exp.getTime() << std::endl;

        nodes = tree.getValidNodes();
        // iterate over the valid nodes
        for(auto it = nodes.begin(); it!=nodes.end(); it++){
            bool isReachableNow = tree.getNode(*it).isReachable();
            //if the node was reachable and now it is not
            if(std::find(wasReachable.begin(), wasReachable.end(), *it) != wasReachable.end() && !isReachableNow) {
                wasReachable.erase(std::remove(wasReachable.begin(), wasReachable.end(), *it), wasReachable.end());
                //if node is new
                if(!tree.getNode(*it).isNew()) {
                    ++nbInterruptionNotNew;
                }
                ++nbInterruption;
            }
            //if wasn't reachable
            else if (std::find(wasReachable.begin(), wasReachable.end(), *it) == wasReachable.end()) {
                // if node is not new
                if(!tree.getNode(*it).isNew()) {
                    interruptionsTimeNotNew += (now - lastTime);
                    timeWithoutVideoNotNew += (now - lastTime);
                }
                if(isReachableNow){
                     //if wasn't reachable and not it is add
                    wasReachable.push_back(*it);
                }
                interruptionsTime += (now - lastTime);
                timeWithoutVideo += (now - lastTime);
            }       
        }
        lastTime = now;
		times.push_back(exp.getTime());
        nodes_count.push_back(tree.getValidNodes().size());
        meanTime.push_back(timeWithoutVideo);
		meanNumber.push_back(nbInterruption);
        meanTimeNotNew.push_back(timeWithoutVideoNotNew);
        meanNumberNotNew.push_back(nbInterruptionNotNew);

    }
    //for(auto it=meanTime.begin();it!=meanTime.end();it++)std::cout<<*it<<"\t";
	toCSV(filename, times, nodes_count, meanTime, meanNumber, meanTimeNotNew, meanNumberNotNew);
}



/** 
Simulation for the main metrics
Save step by step the metrics and save them in a file with the name:
"nameOfTheProtocol"_"numberOfNodes"_"churnRate".plt
in the overTime folder
in a csv format where the columns represent the following metrics:
1st: time t of the simulation
2nd: max height at time t
3rd: Number of nodes not receiving the video
4th: Number of son of the root
5th: Balance of the tree (ignore?)
6th: Absolute balance (ignore?)
7th: avg depth of the nodes
8th: avg depth of the nodes that receive the video

*/
template<class Node>
#ifdef UPDATE
void overTime(NonConstantTree<Node>& tree,  int _maxTime, double _joinRate, double _churnRate, unsigned phase, int (*f)(void), double _updateTime) {
#else
void overTime(NonConstantTree<Node>& tree,  int _maxTime, double _joinRate, double _churnRate, unsigned phase, int (*f)(void)) {
#endif

#ifdef UPDATE
    std::string filename = "overTimeNC/phase_"+ std::to_string(phase) + "_" + getName<Node>() +"_"+std::to_string(int(_updateTime))+ ".plt";
#else
    std::string filename = "overTimeNC/phase_"+ std::to_string(phase) + "_" + getName<Node>() + ".plt";
#endif
    std::cout << filename << std::endl;

    #ifdef UPDATE
    NonConstantExperiment<Node> exp {&tree, _joinRate, _churnRate, 1.0, f, _updateTime}; 
    #else
    NonConstantExperiment<Node> exp {&tree, _joinRate, _churnRate, 1.0, f}; 
    #endif
    // since the experiments can start on a tree not empty I add the leave event for the nodes already inside
    
    //exp.insertNodes(tree.getValidNodes());

    std::vector<int> results1, results2, results3, results6;

    std::vector<unsigned> nodes;

    std::vector<double> times, results4, results5,results7,results8;

    unsigned max_Degree = 20;//tree.getMaxDegree();//to fix


    // here for each bandwidth in each entry I store the avg depth of the nodes with that bandwidth
    std::vector<std::vector<double>> depth_in_function_bandwidth(max_Degree+1);

    // here for each bandwidth in each entry I store the ratio of the node with that bandwidth that receive the video
    std::vector<std::vector<double>> receivingvideo_in_function_bandwidth(max_Degree+1);



	while(exp.getTime() < _maxTime) {
		//std::cout << exp.getTime() << std::endl;
		exp.runUntil(1, _maxTime);
		
		std::cout << tree.getValidNodes().size() << " => Join:" << exp.m_nbJoin << ", Leave:" << exp.m_nbLeave << ", Fix: " << exp.m_nbFix << ", Time: " << exp.getTime() << std::endl;

 		nodes.push_back(tree.getNumberPeers());
        results1.push_back(tree.getMaxHeight());
		results2.push_back(tree.getNbUnreachable());
		results3.push_back(tree.getRoot().getChildren().size());
		results4.push_back(tree.getBalance());
		results5.push_back(tree.getAbsBalance());
        results6.push_back(tree.getNbUnreachableNotNew());
        results7.push_back(tree.getAvgDepth());
        results8.push_back(tree.getAvgDepthReachable());

        for(int i=0;i < (int) depth_in_function_bandwidth.size();i++){
        	depth_in_function_bandwidth[i].push_back(tree.getAvgDepthBandwidth(i));
        	receivingvideo_in_function_bandwidth[i].push_back(tree.getRatioReceivingBandwidth(i));
        }
        times.push_back(exp.getTime());
    }
   	toCSV(filename, times, nodes, results1,results2,results3,results4,results5,results6,results7, results8);
#ifdef UPDATE
    std::string filename_1="overTimeNC/phase_"+ std::to_string(phase) + "_AvgDepthBandwidth_" + getName<Node>()  +"_"+ std::to_string(int(_updateTime)) + ".plt";
    std::string filename_2="overTimeNC/phase_"+ std::to_string(phase) + "RatioReceivingBandwidth_phase_" + getName<Node>() +"_"+ std::to_string(int(_updateTime)) + ".plt";   
#else
    std::string filename_1="overTimeNC/phase_"+ std::to_string(phase) + "_AvgDepthBandwidth_" + getName<Node>()   + ".plt";
    std::string filename_2="overTimeNC/phase_"+ std::to_string(phase) + "RatioReceivingBandwidth_phase_" + getName<Node>()   + ".plt";
#endif
    //print to file
    std::ofstream file1,file2;
    file1.open (filename_1);
    file2.open (filename_2);
    for(int i=0;i<(int) depth_in_function_bandwidth[0].size();i++){
    	file1 << times[i];
    	file2 << times[i];
    	for(int j=0;j<(int) depth_in_function_bandwidth.size();j++){
    		file1 << "\t" << depth_in_function_bandwidth[j][i];
    		file2 << "\t" << receivingvideo_in_function_bandwidth[j][i];
    	}
    	file1 << "\n";
    	file2 << "\n";
    }
  	file1.close();
  	file2.close();


}



#endif