#ifndef NONCONSTANTEXPERIMENT_CPP
#define NONCONSTANTEXPERIMENT_CPP
#include "NonConstantExperiment.hpp"

#include <functional>
#include <cassert>

/**
 Create an NonConstantExperiment over a Tree<Node> with a churn rate of _churnRate and a repair
 *  rate of _fixRate
 It uses a Fake Queue to simulate a queue of events ocurring following a exponential distribution. See "FakeQueue"
 */
template<typename Node> 
#ifdef UPDATE
NonConstantExperiment<Node>::NonConstantExperiment(NonConstantTree<Node>* _tree, double _joinRate, double _indivLeaveRate, double _fixRate, int (*getFromDistribution)(void), double _updateTime) :
    m_nbLeave(0), // Count the number of churn that happened
    m_nbFix(0), // Count the number of "real" repairs that happended
    m_nbJoin(0),
    m_joinRate(_joinRate),
    m_indivLeaveRate(_indivLeaveRate),
    m_fixRate(_fixRate),
    m_updateTime(_updateTime),
    m_last_update(0),
    m_tree(_tree), 
    m_fakeQueue(),
    m_eventsToRemove()
#else
NonConstantExperiment<Node>::NonConstantExperiment(NonConstantTree<Node>* _tree, double _joinRate, double _indivLeaveRate, double _fixRate, int (*getFromDistribution)(void)) :
    m_nbLeave(0), // Count the number of churn that happened
    m_nbFix(0), // Count the number of "real" repairs that happended
    m_nbJoin(0),
    m_joinRate(_joinRate),
    m_indivLeaveRate(_indivLeaveRate),
    m_fixRate(_fixRate),
    m_tree(_tree), 
    m_fakeQueue(),
    m_eventsToRemove()
#endif


{
    //for nodes already in the system
    auto nodes = _tree->getValidNodes();
    for(auto it=nodes.begin();it!=nodes.end();it++){
        unsigned nodeId = *it;    
        // Fix event
        auto fixEventIte = m_fakeQueue.addEvent(m_fixRate);
        fixEventIte->m_func = [&, nodeId](){
            m_nbFix++;
            return this->m_tree->getNode(nodeId).repair();
        };
            
        // Leave event
        auto leaveEventIte = m_fakeQueue.addEvent(m_indivLeaveRate);
        leaveEventIte->m_func = [&, nodeId, leaveEventIte, fixEventIte]()->bool{
            m_nbLeave++;
            m_tree->getNode(nodeId).die();
            m_tree->removeNode(nodeId);
            m_eventsToRemove.push_back(leaveEventIte);
            m_eventsToRemove.push_back(fixEventIte);
            return true;
        };
    }

    // Root repair event
    m_fakeQueue.addEvent(m_fixRate, [&]()->bool{
        m_nbFix++;
        //std::cout<<"repairing root\n";
        m_tree->getRoot().repair();
        return m_tree->getRoot().repair();
    });

    m_fakeQueue.addEvent(m_joinRate, [&,getFromDistribution]()->bool{
        m_nbJoin++;
        addNode(getFromDistribution);
        return true;
    });
}

template<typename Node> 
void NonConstantExperiment<Node>::addNode(int (*f)(void)) {

    unsigned newBandwidth = f();

    int nodeId = m_tree->addNode(newBandwidth); 

    //std::cout <<"adding node with id "<< nodeId <<"and bandwidth  "<<  newBandwidth <<std::endl;

    m_tree->getRoot().addChild(nodeId);//

    assert(nodeId < (int) m_tree->getNodes().size());

    // Fix event
    auto fixEventIte = m_fakeQueue.addEvent(m_fixRate);
    fixEventIte->m_func = [&, nodeId](){
        m_nbFix++;
        //std::cout <<"repair on  "<< nodeId <<std::endl;
        return this->m_tree->getNode(nodeId).repair();
    };
        
    // Leave event
    auto leaveEventIte = m_fakeQueue.addEvent(m_indivLeaveRate);
    leaveEventIte->m_func = [&, nodeId, leaveEventIte, fixEventIte]()->bool{
        m_nbLeave++;
        //std::cout <<"die on  "<< nodeId <<std::endl;
        m_tree->getNode(nodeId).die();
        m_tree->removeNode(nodeId);
        m_eventsToRemove.push_back(leaveEventIte);
        m_eventsToRemove.push_back(fixEventIte);
        return true;
    };
}


template<typename Node>
NonConstantExperiment<Node>::~NonConstantExperiment()
{
}

/**
 Run until _nbSteps events occured
 */
template<typename Node>
void NonConstantExperiment<Node>::run(int _nbSteps) {
    for(int i = 0; i < _nbSteps; ++i) {
        this->nextEvent();
        this->cleanEvents();
    }
}

/**
 Run until an event change the tree
 */
template<typename Node>
void NonConstantExperiment<Node>::runUntil(int _nbEvents) {
    int count = 0;
    while(count < _nbEvents) {
        if(this->nextEvent()) {
            ++count;
            this->cleanEvents();
        }
    }
}

template<typename Node>
void NonConstantExperiment<Node>::runUntil(int _nbEvents, int _maxTime) {
    int count = 0;
    while(count < _nbEvents && getTime() < _maxTime ) {
        if(this->nextEvent()) {
            ++count;
            this->cleanEvents();
        }
    }
}

/**
 Get the next event via the FakeQueue
 */
template<typename Node>
bool NonConstantExperiment<Node>::nextEvent() {
    #ifdef UPDATE
    //std::cout<<"experiment.cpp\n";
    double now = getTime();
    if( now - m_last_update> m_updateTime){
        m_tree->update();
        m_last_update = now;
    }
    #endif
    const Event& nextEvent = m_fakeQueue.getNextEvent();
    return nextEvent.m_func();
}

template<typename Node>
void NonConstantExperiment<Node>::cleanEvents() {
    while(!m_eventsToRemove.empty()) {
        m_fakeQueue.removeEvent(m_eventsToRemove.front());
        m_eventsToRemove.pop_front();
    }
}

/*
template<typename Node>
void NonConstantExperiment<Node>::insertNodes(std::vector<unsigned> nodes){
    for(auto it=nodes.begin();it!=nodes.end();it++){
        unsigned nodeId = *it;    
        // Fix event
        auto fixEventIte = m_fakeQueue.addEvent(m_fixRate);
        fixEventIte->m_func = [&, nodeId](){
            m_nbFix++;
            return this->m_tree->getNode(nodeId).repair();
        };
            
        // Leave event
        auto leaveEventIte = m_fakeQueue.addEvent(m_indivLeaveRate);
        leaveEventIte->m_func = [&, nodeId, leaveEventIte, fixEventIte]()->bool{
            m_nbLeave++;
            m_tree->getNode(nodeId).die();
            m_tree->removeNode(nodeId);
            m_eventsToRemove.push_back(leaveEventIte);
            m_eventsToRemove.push_back(fixEventIte);
            return true;
        };
    }
}
*/

#endif // EXPERIMENT_CPP