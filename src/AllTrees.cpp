#include "AllTrees.hpp"

#include <algorithm>
#include <chrono>
#include <map>
#include <queue>
#include <set>
#include <utility>

#include "Tree.hpp"
#include "SmallestNode.hpp"

void allTrees(int _nbNodes) {
    auto start = std::chrono::high_resolution_clock::now();
    Tree<SmallestNode> tree = Tree<SmallestNode>::balancedTree(2, _nbNodes);
    int i = 0;
    std::map<std::pair<uint64_t, uint64_t>, int> transitionChurnRate;
    std::map<std::pair<uint64_t, uint64_t>, int> transitionRepairRate;

    std::map<uint64_t, int> processed;

    std::list<Tree<SmallestNode>> trees;
    std::list<uint64_t> treesHash;

    int index = 0;
    trees.push_back(tree);
    processed[tree.getHash()] = index;
	trees.back().createDotFile("tree"+std::to_string(index));
    ++index;
	
    auto ite = trees.begin();
    while(ite != trees.end()) {
        auto& currentTree = *ite;
        uint64_t currHash = currentTree.getHash();
        for(Tree<SmallestNode>& t : currentTree.getChurnTransitionTrees()) {
            ++transitionChurnRate[std::make_pair(currHash, t.getHash())];

            if(processed.find(t.getHash()) == processed.end()) {
                trees.push_back(t);
                processed[t.getHash()] = index;
                ++index;
            }
        }

		// std::cout << __FILE__ << ':'<<__LINE__<<std::endl;
        for(const auto& t : currentTree.getRepairTransitionTrees()) {
            ++transitionRepairRate[std::make_pair(currHash, t.getHash())];

            if(processed.find(t.getHash()) == processed.end()) {
                trees.push_back(t);
                processed[t.getHash()] = index;
                ++index;
            }
        }
		// std::cout << __FILE__ << ':'<<__LINE__<<std::endl;
        ++ite;
        ++i;
    }  
    std::cout << "# of trees : " << i << std::endl;
	// std::cout << __FILE__ << ':'<<__LINE__<<std::endl;
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> time_span = end-start;
    std::cout << "Total time : " << time_span.count() << std::endl;
	
	auto nnzChurn = transitionChurnRate.size(), 
		nnzRepair = transitionRepairRate.size();
	auto rows = trees.size(), columns = rows;
	
	std::priority_queue<TriplInt, std::vector<TriplInt>, CompareTriplInt> repairMatrix, 
		churnMatrix;
	for(const auto& pair : transitionRepairRate) {
		repairMatrix.emplace(std::make_tuple(processed[pair.first.first]+1, 
								processed[pair.first.second]+1, 
								pair.second));
	}
	for(const auto& pair : transitionChurnRate) {
		churnMatrix.emplace(std::make_tuple(processed[pair.first.first]+1, 
								processed[pair.first.second]+1, 
								pair.second));
	}
	
	std::ofstream ofs (std::to_string(_nbNodes)+"_MarkovMatrix.mat", std::ofstream::out);
	
	ofs << "# name: model" << std::endl	
		<< "# type: scalar struct" << std::endl
		<< "# ndims: 2" << std::endl
		<< " 1 1" << std::endl
		<< "# length 3" << std::endl
		<< "# name: repair" << std::endl
		<< "# type: sparse matrix"<<std::endl
		<< "# nnz: " << nnzRepair << std::endl
		<< "# rows: " << rows << std::endl
		<< "# columns: " << columns << std::endl;
	while(!repairMatrix.empty()) {
		auto& tuple = repairMatrix.top();
		ofs << std::get<0>(tuple) << " "
			<< std::get<1>(tuple) << " "
			<< std::get<2>(tuple) << std::endl;
		repairMatrix.pop();
	}
	ofs << std::endl << std::endl << std::endl;
	
	ofs << "# name: churn" << std::endl
		<< "# type: sparse matrix"<<std::endl
		<< "# nnz: " << nnzChurn << std::endl
		<< "# rows: " << rows << std::endl
		<< "# columns: " << columns << std::endl;
		
	while(!churnMatrix.empty()) {
		auto tuple = churnMatrix.top();
		ofs << std::get<0>(tuple) << " "
			<< std::get<1>(tuple) << " "
			<< std::get<2>(tuple) << std::endl;
		churnMatrix.pop();
	}
	ofs << std::endl << std::endl << std::endl;
	
	ofs << "# name: tree" << std::endl
		<< "# type: matrix" << std::endl
		<< "# rows: 2" << std::endl
		<< "# columns:" << trees.size() << std::endl;
	
	ofs << " ";
	for(const auto& tree : trees) {
		ofs << tree.getMaxHeight() << " ";
	}
	ofs << std::endl ;
	
	ofs << " ";
	for(const auto& tree : trees) {
		ofs << tree.getNbUnreachable() << " ";
	}
	ofs << std::endl;
	
	ofs << std::endl << std::endl << std::endl;
	ofs.close();
}
