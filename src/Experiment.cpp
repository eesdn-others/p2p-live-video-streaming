#ifndef EXPERIMENT_CPP
#define EXPERIMENT_CPP
#include "Experiment.hpp"

#include <functional>

/**
 Create an Experiment over a Tree<Node> with a churn rate of _churnRate and a repair
 *  rate of _fixRate
 It uses a Fake Queue to simulate a queue of events ocurring following a exponential distribution. See "FakeQueue"
 */


template<typename Node> 

#ifdef UPDATE
Experiment<Node>::Experiment(double _churnRate, double _fixRate, Tree<Node>* _tree, double _updateTime) :
    m_nbChurn(0), // Count the number of churn that happened
    m_nbFix(0), // Count the number of "real" repairs that happended
    m_churnRate(_churnRate), 
    m_fixRate(_fixRate), 
    m_updateTime(_updateTime),
    m_last_update(0),
    m_tree(_tree),
    m_fakeQueue()
{
#else
Experiment<Node>::Experiment(double _churnRate, double _fixRate, Tree<Node>* _tree) :
    m_nbChurn(0), // Count the number of churn that happened
    m_nbFix(0), // Count the number of "real" repairs that happended
    m_churnRate(_churnRate), 
    m_fixRate(_fixRate), 
    m_tree(_tree), 
    m_fakeQueue()
{
#endif  

    
    // Add repair event for each node with rate m_fixRate
    for(Node& node : m_tree->getNodes()) {
        m_fakeQueue.addEvent(Event(m_fixRate, [&](){
            ++this->m_nbFix;
            bool res = node.repair();
            return res;
        }));
    }
    // Add churn rate for each node with rate m_churnRate
    double indChurnRate = m_churnRate / (m_tree->getNodes().size() - 1);
    std::cout << "# of nodes: " << m_tree->getNodes().size() << std::endl;
    
    Node &root = m_tree->getRoot();
    for(unsigned i = 1; i < m_tree->getNodes().size(); ++i) {
        Node& node = m_tree->getNodes()[i]; 
        m_fakeQueue.addEvent(Event(indChurnRate,[&](){
            // std::cout << node.getOwnInd() << " leaves the system." << std::endl;
            ++this->m_nbChurn;
            node.die();
            node.setNew(true);
            root.addChild(node.getOwnInd());
            return true;
        }));
    }
}

/**
 Run until _nbSteps events occured
 */
template<typename Node>
void Experiment<Node>::run(int _nbSteps) {
    for(int i = 0; i < _nbSteps; ++i) {
        this->nextEvent();
    }
}

/**
 Run until an event change the tree
 */
template<typename Node>
void Experiment<Node>::runUntil(int _nbEvents) {
    int count = 0;
    while(count < _nbEvents) {
        if(this->nextEvent()) {
            std::cout << m_fakeQueue << std::endl;
            ++count;
        }
    }
}

template<typename Node>
void Experiment<Node>::runUntil(int _nbEvents, int _maxTime) {
    int count = 0;
    while(count < _nbEvents && getTime() < _maxTime ) {
        if(this->nextEvent()) {
            ++count;
        }
    }
}



/**
 Get the next event via the FakeQueue
 */
template<typename Node>
bool Experiment<Node>::nextEvent() {
    const Event& nextEvent = m_fakeQueue.getNextEvent();

    #ifdef UPDATE
    //std::cout<<"experiment.cpp\n";
    double now = getTime();
    if( now - m_last_update> m_updateTime){
        m_tree->update();
        m_last_update = now;
    }
    #endif

    return nextEvent.m_func();
}

#endif // EXPERIMENT_CPP