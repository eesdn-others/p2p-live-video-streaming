#include "FakeQueue.hpp"

#include <iostream>
#include <algorithm>

/**
 Create a FakeQueue for Poisson process
 Contains the list of all possible events that can occurs with the differents rates.
 Sum of Poisson process of rate alpha & beta is a Poisson process of rate alpha+beta
 */
FakeQueue::FakeQueue() : 
    m_time(0), 
	m_events(), 
	m_totalRate(0)
{}

/**
 Add the event into the queue
 Increase the total rate of the queue
 */
void FakeQueue::addEvent(Event& _event) {
    m_totalRate += _event.m_rate;
    m_events.push_front(_event);
}

void FakeQueue::addEvent(Event&& _event) {
    m_totalRate += _event.m_rate;
    m_events.push_front(_event);
}

std::list<Event>::iterator FakeQueue::addEvent(double _rate, std::function<bool()> _func) {
    m_totalRate += _rate;
    m_events.emplace_front(_rate, _func);
    return m_events.begin();
}

/**
 See inverse i don't know what anymore method
 */
const Event& FakeQueue::getNextEvent() {
    double delta = -log(1.0 - MyRandom::getInstance().getRealUniform(0.0, 1.0)) / m_totalRate;
    m_time += delta;
    double value = MyRandom::getInstance().getRealUniform(0.0 , m_totalRate);
    double total = 0;
    for(Event& event : m_events) {
        total += event.m_rate;
        if(total >= value) {
            return event;
        }
    }
    return m_events.back();
}

// void FakeQueue::removeEvent(const Event& _event) {
//     auto iteEvent = std::find(m_events.begin(), m_events.end(), _event);
    
//     std::iter_swap(iteEvent, m_events.rbegin());
//     m_totalRate -= _event.m_rate;
//     m_events.pop_back();
// }

void FakeQueue::removeEvent(std::list<Event>::const_iterator _ite) {
    m_totalRate -= _ite->m_rate;
    m_events.erase(_ite);
}

// bool operator==(const Event& _ev1, const Event& _ev2) {
//     return _ev1.m_rate == _ev2.m_rate && 
//         _ev1.m_func.target<bool()>() == _ev2.m_func.target<bool()>();
// }

std::ostream& operator<<(std::ostream& _out, const FakeQueue& _fq) {
    _out << "{ ";
    for(auto& event: _fq.m_events) {
        _out << event << ',';
    }
    return _out << "}";
}

std::ostream& operator<<(std::ostream& _out, const Event& _ev) {
    return _out << _ev.m_rate << '\t' << _ev.m_func.target<bool()>();
}