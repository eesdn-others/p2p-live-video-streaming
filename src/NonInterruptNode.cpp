#include "NonInterruptNode.hpp"

#include <algorithm>
#include <iostream>
#include "MyRandom.hpp"

NonInterruptNode::NonInterruptNode(int _degree, Tree<NonInterruptNode>* _tree, int _parent) : 
	Node(_degree, _parent, 0),
	m_tree(_tree)
{
}

#ifdef UPDATE
void NonInterruptNode::update(){
    ;
}
#endif


void NonInterruptNode::addChild(unsigned _id) {
    auto& node = m_tree->getNode(_id);
	getChildren().push_back(_id);
    node.setParent(getOwnInd());
	changeSubtreeSize(node.getSubtreeSize());
}

void NonInterruptNode::die() {
    auto newFatherIndIte = getChildren().begin();
	NonInterruptNode& parent = m_tree->getNode(getParent());
    if(newFatherIndIte != getChildren().end()) {
		parent.replace(getOwnInd(), *newFatherIndIte);
		getChildren().pop_front();
		
		NonInterruptNode& newFather = m_tree->getNode(*newFatherIndIte);
        while(!getChildren().empty()) {
			auto ite = getChildren().begin();
			newFather.adopt(getOwnInd(), ite);
		} 
    } else {
		parent.removeFromChildren(getOwnInd());
	}
	setParent(-1);
}

void NonInterruptNode::removeFromChildren(unsigned _child) {
    m_tree->getNode(_child).setParent(-1);
    getChildren().remove(_child);
	changeSubtreeSize(-m_tree->getNode(_child).getSubtreeSize());
}

bool NonInterruptNode::repair() {
	int size = getChildren().size();
    if((int) size > m_degree) {
        int pi = MyRandom::getInstance().getIntUniform(m_degree, size-1);
        int fi = MyRandom::getInstance().getIntUniform(0, m_degree-1);
		auto pushedNode = std::next(getChildren().begin(), pi);
        auto father = std::next(getChildren().begin(), fi);

        m_tree->getNode(*father).adopt(getOwnInd(), pushedNode);
		return true;
    }
	return false;
}

void NonInterruptNode::adopt(NodeId _oldFatherInd, std::list<NodeId>::iterator _iteChildInd) {
	auto& child = m_tree->getNode(*_iteChildInd), 
		oldFather = m_tree->getNode(_oldFatherInd);
	child.setParent(getOwnInd());
	getChildren().splice(getChildren().end(), oldFather.getChildren(), _iteChildInd);
	changeSubtreeSize(child.getSubtreeSize());
	oldFather.changeSubtreeSize(-child.getSubtreeSize());
}
 
bool NonInterruptNode::isReachable() const {
    if(getOwnInd() == 0) {
        return true;
    }
	NonInterruptNode& parent = m_tree->getNode(getParent());
    if(parent.isReachable()) {
        int i = 0;
        auto ite = parent.getChildren().begin();
        while(i < parent.m_degree) {
            if(*ite == getOwnInd()) {
                if(m_new == true) m_new = false;
                return true;
            }
            ++ite;
            ++i;
        }
    }
    return false;
}

void NonInterruptNode::replace(unsigned _old, unsigned _new) {
	m_tree->getNode(_old).setParent(-1);
	auto& newNode = m_tree->getNode(_new);
	auto ite = std::find(getChildren().begin(), getChildren().end(), _old);
	newNode.setParent(getOwnInd());
	*ite = _new;
	m_tree->getNode(_old).changeSubtreeSize(newNode.getSubtreeSize());
	changeSubtreeSize(newNode.getSubtreeSize()-m_tree->getNode(_old).getSubtreeSize());
}

int NonInterruptNode::getSubtreeSize() const {
	int subtreeSize = 1;
	for(unsigned sonID : this->getChildren()) {
		auto& son = m_tree->getNode(sonID);
		subtreeSize += son.getSubtreeSize();
	}
	return subtreeSize;
}

int NonInterruptNode::getBalance() const {
	int balance = 0;
	if(!m_children.empty()) {
		auto iteFirstChild = m_children.begin();
		auto iteSecondChild = ++m_children.begin();
		//If not only child and is a leaf
		if(iteSecondChild != m_children.end() && !m_tree->getNode(*iteFirstChild).getChildren().empty()) {
			balance += m_tree->getNode(*iteFirstChild).getSubtreeSize();
			for(; iteSecondChild != m_children.end(); ++iteSecondChild) {
				balance -= m_tree->getNode(*iteSecondChild).getSubtreeSize();
			}
		}
	}
	return balance;
}



void NonInterruptNode::changeSubtreeSize(int _delta) {
	m_subtreeSize += _delta;
	if(this->hasParent()) {
		m_tree->getNode(this->getParent()).changeSubtreeSize(_delta);
	}
}


int NonInterruptNode::getDepth() const {
    if(getOwnInd() == 0) {
        return 0;
    }
    auto& parent = m_tree->getNode(getParent());

    return 1+parent.getDepth();
}
