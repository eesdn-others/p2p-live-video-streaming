#include "RandomNode.hpp"

#include <algorithm>
#include <iostream>

#include "MyRandom.hpp"

RandomNode::RandomNode(int _degree, Tree<RandomNode>* _tree, int _parent) :
    Node(_degree, _parent, 0),
	m_tree(_tree)
{
}

#ifdef UPDATE
void RandomNode::update(){
	;
}
#endif


void RandomNode::addChild(unsigned _id) {
	 //std::cout << "m_tree : " << m_tree << std::endl;
//	 std::cout << getOwnInd() << ".addChild("<<_id<<')'<<std::endl;
    RandomNode& node = m_tree->getNode(_id);
	m_children.push_back(_id);
    node.setParent(getOwnInd());
	this->changeSubtreeSize(node.m_subtreeSize);
}

void RandomNode::die() {
	// std::cout << "this die : " << this << std::endl;
	// std::cout << getOwnInd() << " dies" << std::endl;
	// std::cout << getOwnInd() << " children : (";
	// for(auto nodeId : m_children) {
		// std::cout << nodeId << " ";
	// }
	// std::cout <<')'<< std::endl;
	RandomNode& parent = m_tree->getNode(getParent());
	
    auto newFatherIndIte = m_children.begin();
	
    if(newFatherIndIte != m_children.end()) {
		RandomNode& newFather = m_tree->getNode(*newFatherIndIte);
		// std::cout << "new father is " << *newFatherIndIte << std::endl;
		parent.replace(getOwnInd(), *newFatherIndIte);
		// std::cout << *newFatherIndIte<<".getParent() : " << newFather.getParent() << " " << &newFather <<std::endl;
		
		// std::cout << getOwnInd() << " children : " << std::endl;
		// for(auto nodeId : m_children) {
			// std::cout << nodeId << " ";
		// }
		// std::cout << std::endl;
		
        while(!m_children.empty()) {
			newFather.adopt(getOwnInd(), m_children.begin());
		}
		
		// std::cout << getOwnInd() << " children : " << std::endl;
		// for(auto nodeId : m_children) {
			// std::cout << nodeId << " ";
		// }
		// std::cout << std::endl;
    } else {
		parent.removeFromChildren(getOwnInd());
	}
}



void RandomNode::adopt(NodeId _oldFatherInd, std::list<NodeId>::iterator _iteChildInd) {
	// std::cout << getOwnInd() << " adopt " << *_iteChildInd << " from " << _oldFatherInd << ", new SS :" 
		// << m_tree->getNode(*_iteChildInd).m_subtreeSize << std::endl;
		
	m_children.splice(m_children.end(), m_tree->getNode(_oldFatherInd).m_children, _iteChildInd);
	m_tree->getNode(*_iteChildInd).setParent(getOwnInd());
	this->changeSubtreeSize(m_tree->getNode(*_iteChildInd).m_subtreeSize);
	m_tree->getNode(_oldFatherInd).changeSubtreeSize(-m_tree->getNode(*_iteChildInd).m_subtreeSize);
}

void RandomNode::replace(NodeId _old, NodeId _new) {
	// std::cout << _new << " replaces " << _old << " at " << getOwnInd() << std::endl;
	RandomNode& oldNode = m_tree->getNode(_old);
	RandomNode&	newNode = m_tree->getNode(_new);
	
	oldNode.removeFromChildren(_new);
	auto ite = std::find(m_children.begin(), m_children.end(), _old);
	
	newNode.setParent(getOwnInd());
	oldNode.setParent(-1);
	*ite = _new;
	int newSS = newNode.m_subtreeSize;
	int oldSS = oldNode.m_subtreeSize;
	
	this->changeSubtreeSize(newSS-oldSS);
}

void RandomNode::removeFromChildren(NodeId _child) {
	// std::cout << _child << " leaves " << getOwnInd() << std::endl;
	RandomNode& child = m_tree->getNode(_child);
    m_children.remove(_child);
	this->changeSubtreeSize(-child.m_subtreeSize);
    child.setParent(-1);
}

bool RandomNode::repair() {
    int size = m_children.size();
    if(size > m_degree && m_degree) {
		auto shuffle = MyRandom::getInstance().getShuffled(0, size-1);
        int ind_father = shuffle[0];
        int ind_son = shuffle[1];

        auto ite = m_children.begin();
        std::advance(ite, ind_father);

        RandomNode& father = m_tree->getNode(*ite);

        ite = m_children.begin();
        std::advance(ite, ind_son);

        NodeId sonId = *ite;

        removeFromChildren(sonId);
        father.addChild(sonId);
		return true;
    }
	return false;
}

bool RandomNode::isReachable() const {
    if(getOwnInd() == 0) {
        return true;
    }
	RandomNode& parent = m_tree->getNode(getParent());
    if(parent.isReachable()) {
        int i = 0;
        auto ite = parent.m_children.begin();
        while(i < parent.m_degree) {
            if(*ite == getOwnInd()) {
                if(m_new == true) m_new = false;
                return true;
            }
            ++ite;
            ++i;
        }
    }
    return false;
}

int RandomNode::getBalance() const {
	int balance = 0;
	if(!m_children.empty()) {
		auto iteFirstChild = m_children.begin();
		auto iteSecondChild = ++m_children.begin();
		//If not only child and is a leaf
		if(iteSecondChild != m_children.end() && !m_tree->getNode(*iteFirstChild).m_children.empty()) {
			balance += m_tree->getNode(*iteFirstChild).m_subtreeSize;
			for(; iteSecondChild != m_children.end(); ++iteSecondChild) {
				balance -= m_tree->getNode(*iteSecondChild).m_subtreeSize;
			}
		}
	}
	return balance;
}

void RandomNode::changeSubtreeSize(int _delta) {
	// std::cout << "("<<getOwnInd()<<", "<<getParent()<<")" << ".changeSubtreeSize(" << _delta <<") : " << m_subtreeSize;
	m_subtreeSize += _delta;
	// std::cout << " => " << m_subtreeSize << "-->" << this << std::endl;
	if(this->hasParent()) {
		// std::cout << getOwnInd() << "^" << getParent() << std::endl;
		m_tree->getNode(this->getParent()).changeSubtreeSize(_delta);
	}
}

int RandomNode::getDepth() const {
    if(getOwnInd() == 0) {
        return 0;
    }
    auto& parent = m_tree->getNode(getParent());

    return 1+parent.getDepth();
}
