#include "Node.hpp"

/**
Abstract class for the Protocols
A node has an id, a list of children, a size and a maximum degree
*/
Node::Node(int _degree, int _parent, unsigned _ownInd) : 
	m_degree(_degree),
	m_children(),
	m_subtreeSize(1), 
	m_parent(_parent),
	m_ownInd(_ownInd)
{
}

Node::~Node()
{
}

/**
For uses with Graphviz
*/
std::string Node::getDotLabel() const {
	return std::to_string(this->getOwnInd()) + "=>" + std::to_string(this->getSubtreeSize()) + ", d:" + std::to_string(this->getDegree());
}

/**
Debug print
*/
std::ostream& operator<<(std::ostream& _out, Node& _n) {
	_out << "m_ownInd : " << _n.m_ownInd << ", m_parent : " << _n.m_parent << ", m_subtreeSize :" <<_n.m_subtreeSize << ", children : (";
	for(auto& node : _n.m_children) {
		_out << node << ", ";
	}
	return _out << ')';
}


